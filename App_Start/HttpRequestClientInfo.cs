﻿//
//  HttpRequestClientInfo.cs
//
//  Author:
//       Roy Merkel <merkel-roy@comcast.net>
//
//  Copyright (c) 2018 Roy Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net.Http;
using System.Web;
using Newtonsoft.Json;

namespace GameOfLife.App_Start
{
    public static class HttpRequestClientInfo
    {
        private const string HttpContext = "MS_HttpContext";
        private const string RemoteEndpointMessage =
            "System.ServiceModel.Channels.RemoteEndpointMessageProperty";
        private const string OwinContext = "MS_OwinContext";

        public static string GetClientIpAddress(this HttpRequestMessage request)
        {
            // Web-hosting. Needs reference to System.Web.dll
            if (request.Properties.ContainsKey(HttpContext))
            {
                dynamic ctx = request.Properties[HttpContext];
                if (ctx != null)
                {
                    return ctx.Request.UserHostAddress;
                }
            }

            // Self-hosting. Needs reference to System.ServiceModel.dll. 
            if (request.Properties.ContainsKey(RemoteEndpointMessage))
            {
                dynamic remoteEndpoint = request.Properties[RemoteEndpointMessage];
                if (remoteEndpoint != null)
                {
                    return remoteEndpoint.Address;
                }
            }

            // Self-hosting using Owin. Needs reference to Microsoft.Owin.dll. 
            if (request.Properties.ContainsKey(OwinContext))
            {
                dynamic owinContext = request.Properties[OwinContext];
                if (owinContext != null)
                {
                    return owinContext.Request.RemoteIpAddress;
                }
            }

            return null;
        }

        public static string GetUserAgent(this HttpRequestMessage request)
        {
            string userAgent = null;

            IEnumerable<string> values = null;

            if (request.Headers.TryGetValues("User-Agent", out values))
            {
                if (values != null)
                {
                    IEnumerator<string> iter = values.GetEnumerator();

                    while (iter.MoveNext())
                    {
                        userAgent = iter.Current;
                    }
                }
            }

            return userAgent;
        }

        public static string GetReferrer(this HttpRequestMessage request)
        {
            string referrer = null;

            IEnumerable<string> values = null;

            if (request.Headers.TryGetValues("Referer", out values))
            {
                if (values != null)
                {
                    IEnumerator<string> iter = values.GetEnumerator();

                    while (iter.MoveNext())
                    {
                        referrer = iter.Current;
                    }
                }
            }

            return referrer;
        }

        public static Dictionary<string, dynamic> GetParams(this HttpRequestMessage request)
        {
            string body = request.Content.ReadAsStringAsync().Result;

            Dictionary<string, dynamic> parms = null;

            try
            {
                parms = JsonConvert.DeserializeObject<Dictionary<string, dynamic>>(body);
            }
            catch (Exception)
            {
                parms = null;
            }

            if (parms == null)
            {
                try
                {
                    bool tmp = Uri.IsWellFormedUriString("tmp.html?" + body, UriKind.Relative);

                    if (tmp)
                    {
                        bool valid = false;
                        NameValueCollection nvc = HttpUtility.ParseQueryString(body);

                        foreach (string key in nvc.Keys)
                        {
                            if (!key.Equals(""))
                            {
                                valid = true;
                            }
                            else
                            {
                                valid = false;
                                break;
                            }
                        }

                        if (valid)
                        {
                            parms = new Dictionary<string, dynamic>();

                            foreach (string key in nvc.Keys)
                            {
                                string[] values = nvc.GetValues(key);

                                if (values.Length == 0)
                                {
                                    String nullStr = null;
                                    parms.Add(key, nullStr);
                                }
                                else if (values.Length == 1)
                                {
                                    parms.Add(key, values[0]);
                                }
                                else
                                {
                                    parms.Add(key, values);
                                }
                            }
                        }
                    }
                }
                catch (Exception)
                {
                    parms = null;
                }
            }

            if (parms == null)
            {
                // TODO: other body types other then json or urlencoded?

                parms = new Dictionary<string, dynamic>();
            }

            IEnumerable<KeyValuePair<string, string>> reqparms = null;
            IEnumerator<KeyValuePair<string, string>> reqparmsiter = null;

            try
            {
                reqparms = request.GetQueryNameValuePairs();
                reqparmsiter = reqparms.GetEnumerator();
            }
            catch (Exception)
            {
                reqparms = null;
                reqparmsiter = null;
            }

            if (reqparmsiter != null)
            {
                while (reqparmsiter.MoveNext())
                {
                    KeyValuePair<string, string> parm = reqparmsiter.Current;

                    if (!parms.ContainsKey(parm.Key))
                    {
                        parms.Add(parm.Key, parm.Value);
                    }
                }
            }

            return parms;
        }
    }
}
