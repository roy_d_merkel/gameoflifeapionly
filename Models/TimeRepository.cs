﻿//
//  TimeRepository.cs
//
//  Author:
//       Joe Merkel <merkeljoe9@gmail.com>
//
//  Copyright (c) 2018 Joe Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using GameOfLife.Entities;
using T4R.Data;

namespace GameOfLife.Models
{
    public class TimeRepository
    {
        private GameStateRepository gameStateRepository = null;
        public TimeRepository()
        {
            gameStateRepository = new GameStateRepository();
        }

        public TimeData Get(DBConnection conn, int studentId)
        {
            TimeData result = null;

            bool launched;
            DateTime? now;
            DateTime? nextDateTime;
            TimeSpan? timeToNextTick;

            timeToNextTick = gameStateRepository.GetNextTick(conn, studentId, out launched, out now, out nextDateTime);

            result = new TimeData(launched, ((now == null || !now.HasValue) ? null : now.Value.ToString("yyyy/MM/dd HH:mm:ss")), ((nextDateTime == null || !nextDateTime.HasValue) ? null : nextDateTime.Value.ToString("yyyy/MM/dd HH:mm:ss")), timeToNextTick);

            return result;
        }
    }
}
