﻿//
//  CarsRepository.cs
//
//  Author:
//       Joe Merkel <merkeljoe9@gmail.com>
//
//  Copyright (c) 2018 Joe Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using GameOfLife.Entities;
using T4R.Data;

namespace GameOfLife.Models
{
    public class CarsRepository
    {
        public List<Car> Get(DBConnection conn, int studentId)
        {
            List<Car> result = null;

            string query;
            List<Dictionary<string, object>> rs = null;
            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"SELECT c.*, l.monthly_amount
FROM cars c
LEFT JOIN loans l ON l.category = CONCAT('Car Loan ', c.car_id) and l.removed = 0
WHERE c.user_id = ? and c.removed = 0";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId));

                    break;
                case "postgresql":
                    query =
@"SELECT c.*, l.monthly_amount
FROM cars c
LEFT JOIN loans l ON l.category = CONCAT('Car Loan ', c.car_id) and l.removed = 0
WHERE c.user_id = :1 and c.removed = 0";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId));

                    break;
                case "sqlserver":
                    query =
@"SELECT c.*, l.monthly_amount
FROM cars c
LEFT JOIN loans l ON l.category = CONCAT('Car Loan ', c.car_id) and l.removed = 0
WHERE c.user_id = @1 and c.removed = 0";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId));

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }

            foreach (Dictionary<string, object> row in rs)
            {
                object carId;
                object userId;
                object name;
                object new_used;
                object price;
                object loanDownpayment;
                object loanAmount;
                object loanDuration;
                object monthlyAmount;

                row.TryGetValue("car_id", out carId);
                row.TryGetValue("user_id", out userId);
                row.TryGetValue("name", out name);
                row.TryGetValue("new_used", out new_used);
                row.TryGetValue("price", out price);
                row.TryGetValue("loan_downpayment", out loanDownpayment);
                row.TryGetValue("loan_amount", out loanAmount);
                row.TryGetValue("loan_duration", out loanDuration);
                row.TryGetValue("monthly_amount", out monthlyAmount);

                if (monthlyAmount != null && !(monthlyAmount.GetType().IsInstanceOfType(DBNull.Value)))
                {
                    monthlyAmount = Convert.ToInt32(monthlyAmount);
                }

                if(result == null)
                {
                    result = new List<Car>();
                }

                Car car = new Car((int)carId, (int)userId, (string)name, (string)new_used, (int)price, loanDownpayment as int?, loanAmount as int?, loanDuration as int?, monthlyAmount as int?);
                result.Add(car);
            }

            return result;
        }

        public bool Buy(DBConnection conn, int studentId, string name, string newUsed, int price)
        {
            bool result = false;

            string query;
            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"INSERT INTO cars (user_id, name, new_used, price, loan_downpayment, loan_amount, loan_duration)
VALUES (?, ?, ?, ?, null, null, null);";
                    result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId), new ParamType(typeof(string), name), new ParamType(typeof(string), newUsed), new ParamType(typeof(int), price)) > 0);

                    break;
                case "postgresql":
                    query =
@"INSERT INTO cars (user_id, name, new_used, price, loan_downpayment, loan_amount, loan_duration)
VALUES (:1, :2, :3, :4, null, null, null);";
                    result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId), new ParamType(typeof(string), name), new ParamType(typeof(string), newUsed), new ParamType(typeof(int), price)) > 0);

                    break;
                case "sqlserver":
                    query =
@"INSERT INTO cars (user_id, name, new_used, price, loan_downpayment, loan_amount, loan_duration)
VALUES (@1, @2, @3, @4, null, null, null);";
                    result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId), new ParamType(typeof(string), name), new ParamType(typeof(string), newUsed), new ParamType(typeof(int), price)) > 0);

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }

            return result;
        }

        public bool Loan(DBConnection conn, int studentId, string name, string newUsed, int price, int loan_downpayment, int loan_amount, int loan_duration)
        {
            bool result = false;
            List<Dictionary<string, object>> rs = null;

            string query;
            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"INSERT INTO cars (user_id, name, new_used, price, loan_downpayment, loan_amount, loan_duration)
VALUES (?, ?, ?, ?, ?, ?, ?);
SELECT LAST_INSERT_ID() AS car_id;";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId), new ParamType(typeof(string), name), new ParamType(typeof(string), newUsed), new ParamType(typeof(int), price), new ParamType(typeof(int), loan_downpayment), new ParamType(typeof(int), loan_amount), new ParamType(typeof(int), loan_duration));

                    break;
                case "postgresql":
                    query =
@"INSERT INTO cars (user_id, name, new_used, price, loan_downpayment, loan_amount, loan_duration)
VALUES (:1, :2, :3, :4, :5, :6, :7)
RETURNING car_id;";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId), new ParamType(typeof(string), name), new ParamType(typeof(string), newUsed), new ParamType(typeof(int), price), new ParamType(typeof(int), loan_downpayment), new ParamType(typeof(int), loan_amount), new ParamType(typeof(int), loan_duration));

                    break;
                case "sqlserver":
                    query =
@"INSERT INTO cars (user_id, name, new_used, price, loan_downpayment, loan_amount, loan_duration)
VALUES (@1, @2, @3, @4, @5, @6, @7);
OUTPUT INSERTED.ID AS car_id;";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId), new ParamType(typeof(string), name), new ParamType(typeof(string), newUsed), new ParamType(typeof(int), price), new ParamType(typeof(int), loan_downpayment), new ParamType(typeof(int), loan_amount), new ParamType(typeof(int), loan_duration));

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }

            int carId = -1;
            result = false;
            foreach (Dictionary<string, object> row in rs)
            {
                object car_id;

                row.TryGetValue("car_id", out car_id);
                carId = Convert.ToInt32(car_id);

                result = true;
            }

            if(result)
            {
                LoanRepository loanRepository = new LoanRepository();
                result = loanRepository.AddCarLoan(conn, studentId, carId, loan_downpayment, loan_amount, loan_duration);
            }

            return result;
        }
    }
}
