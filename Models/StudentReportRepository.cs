﻿//
//  StudentReportRepository.cs
//
//  Author:
//       Joe Merkel <merkeljoe9@gmail.com>
//
//  Copyright (c) 2018 Joe Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using GameOfLife.Entities;
using T4R.Data;

namespace GameOfLife.Models
{
    public class StudentReportRepository
    {
        public List<StudentReport> Get(DBConnection conn, int studentId)
        {
            List<StudentReport> result = new List<StudentReport>();
            List<Dictionary<string, object>> rs = null;

            string query;

            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"SELECT i.income_expenses_log_id, i.month, i.cash_on_hand, i.monthly_income, i.monthly_expenses, i.checking, i.savings, e.*
FROM income_expenses_log i
JOIN game_state s
LEFT JOIN expenses e ON i.user_id = e.user_id AND e.charged_on = i.month
WHERE i.user_id = ?
ORDER BY i.month, i.income_expenses_log_id, COALESCE(e.charged_on, 0), COALESCE(e.category, '');";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId));

                    break;
                case "postgresql":
                    query =
@"SELECT i.income_expenses_log_id, i.month, i.cash_on_hand, i.monthly_income, i.monthly_expenses, i.checking, i.savings, e.*
FROM income_expenses_log i
JOIN game_state s
LEFT JOIN expenses e ON i.user_id = e.user_id AND e.charged_on = i.month
WHERE i.user_id = :1
ORDER BY i.month, i.income_expenses_log_id, COALESCE(e.charged_on, 0), COALESCE(e.category, '');";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId));

                    break;
                case "sqlserver":
                    query =
@"SELECT i.income_expenses_log_id, i.month, i.cash_on_hand, i.monthly_income, i.monthly_expenses, i.checking, i.savings, e.*
FROM income_expenses_log i
JOIN game_state s
LEFT JOIN expenses e ON i.user_id = e.user_id AND e.charged_on = i.month
WHERE i.user_id = @1
ORDER BY i.month, i.income_expenses_log_id, COALESCE(e.charged_on, 0), COALESCE(e.category, '');";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId));

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }

            Dictionary<int, StudentReport> months = new Dictionary<int, StudentReport>();
            foreach (Dictionary<string, object> row in rs)
            {
                object month;
                object cash_on_hand;
                object monthly_income;
                object monthly_expenses;
                object checking;
                object savings;

                object id;
                object student_id;
                object charged_on;
                object category;
                object amount;
                object description;

                row.TryGetValue("month", out month);
                row.TryGetValue("cash_on_hand", out cash_on_hand);
                row.TryGetValue("monthly_income", out monthly_income);
                row.TryGetValue("monthly_expenses", out monthly_expenses);
                row.TryGetValue("checking", out checking);
                row.TryGetValue("savings", out savings);

                if(month == null || month.GetType().IsInstanceOfType(DBNull.Value))
                {
                    continue;
                }

                StudentReport report = null;
                if(months.ContainsKey(Convert.ToInt32(month)))
                {
                    report = months[Convert.ToInt32(month)];
                }
                else
                {
                    report = new StudentReport(studentId, Convert.ToInt32(month), Convert.ToInt32(cash_on_hand), Convert.ToInt32(monthly_income), Convert.ToInt32(monthly_expenses), Convert.ToInt32(checking), Convert.ToInt32(savings), new List<Expense>());
                    months[Convert.ToInt32(month)] = report;
                }

                row.TryGetValue("expense_id", out id);
                row.TryGetValue("user_id", out student_id);
                row.TryGetValue("charged_on", out charged_on);
                row.TryGetValue("category", out category);
                row.TryGetValue("amount", out amount);
                row.TryGetValue("description", out description);

                List<Expense> es = report.expenses;
                Expense e = new Expense((int)id, (int?)student_id, (int?)charged_on, (string)category, (string)amount, (string)description);
                es.Add(e);
            }

            foreach(int month in months.Keys)
            {
                result.Add(months[month]);
            }

            return result;
        }
    }
}
