﻿//
//  ExpenseRepository.cs
//
//  Author:
//       Joe Merkel <merkeljoe9@gmail.com>
//
//  Copyright (c) 2018 Joe Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using GameOfLife.Entities;
using T4R.Data;

namespace GameOfLife.Models
{
    public class ExpenseRepository
    {
		public List<Expense> expenses(DBConnection conn, int studentId)
        {
            List<Expense> result = new List<Expense>();
            List<Dictionary<string, object>> rs = null;

            string query;

            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"SELECT *
FROM expenses e
WHERE user_id = ?;";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId));

                    break;
                case "postgresql":
                    query =
@"SELECT *
FROM expenses e
WHERE user_id = :1;";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId));

                    break;
                case "sqlserver":
                    query =
@"SELECT *
FROM expenses e
WHERE user_id = @1;";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId));

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }

            foreach (Dictionary<string, object> row in rs)
            {
                object id;
                object student_id;
                object charged_on;
                object category;
                object amount;
                object description;

                row.TryGetValue("expense_id", out id);
                row.TryGetValue("user_id", out student_id);
                row.TryGetValue("charged_on", out charged_on);
                row.TryGetValue("category", out category);
                row.TryGetValue("amount", out amount);
                row.TryGetValue("description", out description);

                Expense e = new Expense((int)id, (int?)student_id, (int?)charged_on, (string)category, (string)amount, (string)description);
                result.Add(e);
            }

            return result;
        }

		public List<Expense> expenses(DBConnection conn, int studentId, int month)
        {
            List<Expense> result = new List<Expense>();
            List<Dictionary<string, object>> rs = null;

            string query;

            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"SELECT *
FROM expenses e
WHERE user_id = ? AND charged_on = ?;";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId), new ParamType(typeof(int), month));

                    break;
                case "postgresql":
                    query =
@"SELECT *
FROM expenses e
WHERE user_id = :1 AND charged_on = :2;";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId), new ParamType(typeof(int), month));

                    break;
                case "sqlserver":
                    query =
@"SELECT *
FROM expenses e
WHERE user_id = @1 AND charged_on = @2;";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId), new ParamType(typeof(int), month));

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }

            foreach (Dictionary<string, object> row in rs)
            {
                object id;
                object student_id;
                object charged_on;
                object category;
                object amount;
                object description;

                row.TryGetValue("expense_id", out id);
                row.TryGetValue("user_id", out student_id);
                row.TryGetValue("charged_on", out charged_on);
                row.TryGetValue("category", out category);
                row.TryGetValue("amount", out amount);
                row.TryGetValue("description", out description);

                Expense e = new Expense((int)id, (int?)student_id, (int?)charged_on, (string)category, (string)amount, (string)description);
                result.Add(e);
            }

            return result;
        }

		public List<Expense> expenses(DBConnection conn, int studentId, int start, int end)
        {
            List<Expense> result = new List<Expense>();
            List<Dictionary<string, object>> rs = null;

            string query;

            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"SELECT *
FROM expenses e
WHERE user_id = ? AND charged_on BETWEEN ? AND ?;";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId), new ParamType(typeof(int), start), new ParamType(typeof(int), end));

                    break;
                case "postgresql":
                    query =
@"SELECT *
FROM expenses e
WHERE user_id = :1 AND charged_on BETWEEN :2 AND :3;";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId), new ParamType(typeof(int), start), new ParamType(typeof(int), end));

                    break;
                case "sqlserver":
                    query =
@"SELECT *
FROM expenses e
WHERE user_id = @1 AND charged_on BETWEEN @2 AND @3;";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId), new ParamType(typeof(int), start), new ParamType(typeof(int), end));

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }

            foreach (Dictionary<string, object> row in rs)
            {
                object id;
                object student_id;
                object charged_on;
                object category;
                object amount;
                object description;

                row.TryGetValue("expense_id", out id);
                row.TryGetValue("user_id", out student_id);
                row.TryGetValue("charged_on", out charged_on);
                row.TryGetValue("category", out category);
                row.TryGetValue("amount", out amount);
                row.TryGetValue("description", out description);

                Expense e = new Expense((int)id, (int?)student_id, (int?)charged_on, (string)category, (string)amount, (string)description);
                result.Add(e);
            }

            return result;
        }

		public void addExpense(DBConnection conn, int? studentId, DateTime? chargedOn, string category, string amount, string description)
        {         
            string query;

            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"INSERT INTO expenses (user_id, charged_on, category, amount, description)
VALUES (?, ?, ?, ?, ?);";
                    conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId), new ParamType(typeof(DateTime), chargedOn), new ParamType(typeof(string), category), new ParamType(typeof(string), amount), new ParamType(typeof(string), description));

                    break;
                case "postgresql":
                    query =
@"INSERT INTO expenses (user_id, charged_on, category, amount, description)
VALUES (:1, :2, :3, :4, :5);";
                    conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId), new ParamType(typeof(DateTime), chargedOn), new ParamType(typeof(string), category), new ParamType(typeof(string), amount), new ParamType(typeof(string), description));

                    break;
                case "sqlserver":
                    query =
@"INSERT INTO expenses (user_id, charged_on, category, amount, description)
VALUES (@1, @2, @3, @4, @5);";
                    conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId), new ParamType(typeof(DateTime), chargedOn), new ParamType(typeof(string), category), new ParamType(typeof(string), amount), new ParamType(typeof(string), description));

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }
        }

        public bool addEventExpense(DBConnection conn, int? studentId, bool chargeNow, string category, string amount, string description)
        {
            string query;
            int updated;

            if(chargeNow)
            {
                switch (conn.DriverClass)
                {
                    case "mysql":
                        query =
@"INSERT INTO expenses (user_id, charged_on, category, amount, description)
VALUES (?, (SELECT current_game_month_number FROM game_of_life.game_state where class_id=1), ?, ?, ?);";
                        updated = conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId), new ParamType(typeof(string), category), new ParamType(typeof(string), amount), new ParamType(typeof(string), description));

                        break;
                    case "postgresql":
                        query =
@"INSERT INTO expenses (user_id, charged_on, category, amount, description)
VALUES (:1, (SELECT current_game_month_number FROM game_of_life.game_state where class_id=1), :2, :3, :4);";
                        updated = conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId), new ParamType(typeof(string), category), new ParamType(typeof(string), amount), new ParamType(typeof(string), description));

                        break;
                    case "sqlserver":
                        query =
@"INSERT INTO expenses (user_id, charged_on, category, amount, description)
VALUES (@1, (SELECT current_game_month_number FROM game_of_life.game_state where class_id=1), @2, @3, @4);";
                        updated = conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId), new ParamType(typeof(string), category), new ParamType(typeof(string), amount), new ParamType(typeof(string), description));

                        break;
                    default:
                        throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
                }

                return updated > 0;
            }
            else
            {
                switch (conn.DriverClass)
                {
                    case "mysql":
                        query =
@"INSERT INTO bank (user_id, cash_on_hand, checking, savings)
SELECT u.user_id, 0, 0, 0
FROM users u
left join bank b ON b.user_id = u.user_id
WHERE u.removed = 0 AND u.user_id = ? AND b.user_id IS NULL;";
                        conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId));

                        break;
                    case "postgresql":
                        query =
@"INSERT INTO bank (user_id, cash_on_hand, checking, savings)
SELECT u.user_id, 0, 0, 0
FROM users u
left join bank b ON b.user_id = u.user_id
WHERE u.removed = 0 AND u.user_id = :1 AND b.user_id IS NULL;";
                        conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId));

                        break;
                    case "sqlserver":
                        query =
@"INSERT INTO bank (user_id, cash_on_hand, checking, savings)
SELECT u.user_id, 0, 0, 0
FROM users u
left join bank b ON b.user_id = u.user_id
WHERE u.removed = 0 AND u.user_id = @1 AND b.user_id IS NULL;";
                        conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId));

                        break;
                    default:
                        throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
                }

                switch (conn.DriverClass)
                {
                    case "mysql":
                        query =
@"INSERT INTO expenses (user_id, charged_on, category, amount, description)
VALUES (?, (SELECT current_game_month_number FROM game_of_life.game_state where class_id=1)+1, ?, ?, ?);";
                        updated = conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId), new ParamType(typeof(string), category), new ParamType(typeof(string), amount), new ParamType(typeof(string), description));

                        break;
                    case "postgresql":
                        query =
@"INSERT INTO expenses (user_id, charged_on, category, amount, description)
VALUES (:1, (SELECT current_game_month_number FROM game_of_life.game_state where class_id=1)+1, :2, :3, :4);";
                        updated = conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId), new ParamType(typeof(string), category), new ParamType(typeof(string), amount), new ParamType(typeof(string), description));

                        break;
                    case "sqlserver":
                        query =
@"INSERT INTO expenses (user_id, charged_on, category, amount, description)
VALUES (@1, (SELECT current_game_month_number FROM game_of_life.game_state where class_id=1)+1, @2, @3, @4);";
                        updated = conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId), new ParamType(typeof(string), category), new ParamType(typeof(string), amount), new ParamType(typeof(string), description));

                        break;
                    default:
                        throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
                }

                switch (conn.DriverClass)
                {
                    case "mysql":
                        query =
@"UPDATE bank
SET cash_on_hand = cash_on_hand - ?
WHERE user_id = ?;";
                        updated += conn.ExecuteUpdate(query, new ParamType(typeof(string), amount), new ParamType(typeof(int), studentId));

                        break;
                    case "postgresql":
                        query =
@"UPDATE bank
SET cash_on_hand = cash_on_hand - :1
WHERE user_id = :2;";
                        updated += conn.ExecuteUpdate(query, new ParamType(typeof(string), amount), new ParamType(typeof(int), studentId));

                        break;
                    case "sqlserver":
                        query =
@"UPDATE bank
SET cash_on_hand = cash_on_hand - @1
WHERE user_id = @2;";
                        updated += conn.ExecuteUpdate(query, new ParamType(typeof(string), amount), new ParamType(typeof(int), studentId));

                        break;
                    default:
                        throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
                }

                return updated > 1;
            }
        }
    }
}
