﻿//
//  LoanRepository.cs
//
//  Author:
//       Joe Merkel <merkeljoe9@gmail.com>
//
//  Copyright (c) 2018 Joe Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using GameOfLife.Entities;
using T4R.Data;

namespace GameOfLife.Models
{
    public class LoanRepository
    {
        public List<Loan> loans(int studentId)
        {
            List<Loan> result = new List<Loan>();
            DBConnection conn = new DBConnection("GameOfLife");
            List<Dictionary<string, object>> rs = null;

            string query;

            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"SELECT *
FROM loans e
WHERE user_id = ? and removed = 0;";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId));

                    break;
                case "postgresql":
                    query =
@"SELECT *
FROM loans e
WHERE user_id = :1 and removed = 0;";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId));

                    break;
                case "sqlserver":
                    query =
@"SELECT *
FROM loans e
WHERE user_id = @1 and removed = 0;";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId));

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }

            foreach (Dictionary<string, object> row in rs)
            {
                object id;
                object student_id;
                object active_start;
                object active_end;
                object category;
                object total_amount;
                object monthly_amount;

                row.TryGetValue("loan_id", out id);
                row.TryGetValue("user_id", out student_id);
                row.TryGetValue("active_start", out active_start);
                row.TryGetValue("active_end", out active_end);
                row.TryGetValue("category", out category);
                row.TryGetValue("total_amount", out total_amount);
                row.TryGetValue("monthly_amount", out monthly_amount);

                Loan e = new Loan((int)id, (int?)student_id, (DateTime?)active_start, (DateTime?)active_end, (string)category, (string)total_amount, (string)monthly_amount);
                result.Add(e);
            }

            conn.Close();

            return result;
        }

        public List<Loan> loans(int studentId, DateTime month)
        {
            List<Loan> result = new List<Loan>();
            DBConnection conn = new DBConnection("GameOfLife");
            List<Dictionary<string, object>> rs = null;

            string query;

            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"SELECT *
FROM loans e
WHERE user_id = ? AND ? BETWEEN active_start AND active_end and removed = 0;";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId), new ParamType(typeof(DateTime), month));

                    break;
                case "postgresql":
                    query =
@"SELECT *
FROM loans e
WHERE user_id = :1 AND :2 BETWEEN active_start AND active_end and removed = 0;";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId), new ParamType(typeof(DateTime), month));

                    break;
                case "sqlserver":
                    query =
@"SELECT *
FROM loans e
WHERE user_id = @1 AND @2 BETWEEN active_start AND active_end and removed = 0;";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId), new ParamType(typeof(DateTime), month));

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }

            foreach (Dictionary<string, object> row in rs)
            {
                object id;
                object student_id;
                object active_start;
                object active_end;
                object category;
                object total_amount;
                object monthly_amount;

                row.TryGetValue("loan_id", out id);
                row.TryGetValue("user_id", out student_id);
                row.TryGetValue("active_start", out active_start);
                row.TryGetValue("active_end", out active_end);
                row.TryGetValue("category", out category);
                row.TryGetValue("total_amount", out total_amount);
                row.TryGetValue("monthly_amount", out monthly_amount);

                Loan e = new Loan((int)id, (int?)student_id, (DateTime?)active_start, (DateTime?)active_end, (string)category, (string)total_amount, (string)monthly_amount);
                result.Add(e);
            }

            conn.Close();

            return result;
        }

        public List<Loan> loans(int studentId, DateTime start, DateTime end)
        {
            List<Loan> result = new List<Loan>();
            DBConnection conn = new DBConnection("GameOfLife");
            List<Dictionary<string, object>> rs = null;

            string query;

            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"SELECT *
FROM loans e
WHERE user_id = ? AND active_start <= ? AND active_end >= ? and removed = 0;";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId), new ParamType(typeof(DateTime), end), new ParamType(typeof(DateTime), start));

                    break;
                case "postgresql":
                    query =
@"SELECT *
FROM loans e
WHERE user_id = :1 AND active_start <= :2 AND active_end >= :3 and removed = 0;";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId), new ParamType(typeof(DateTime), end), new ParamType(typeof(DateTime), start));

                    break;
                case "sqlserver":
                    query =
@"SELECT *
FROM loans e
WHERE user_id = @1 AND active_start <= @2 AND active_end >= @3 and removed = 0;";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId), new ParamType(typeof(DateTime), end), new ParamType(typeof(DateTime), start));

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }

            foreach (Dictionary<string, object> row in rs)
            {
                object id;
                object student_id;
                object active_start;
                object active_end;
                object category;
                object total_amount;
                object monthly_amount;

                row.TryGetValue("loan_id", out id);
                row.TryGetValue("user_id", out student_id);
                row.TryGetValue("active_start", out active_start);
                row.TryGetValue("active_end", out active_end);
                row.TryGetValue("category", out category);
                row.TryGetValue("total_amount", out total_amount);
                row.TryGetValue("monthly_amount", out monthly_amount);

                Loan e = new Loan((int)id, (int?)student_id, (DateTime?)active_start, (DateTime?)active_end, (string)category, (string)total_amount, (string)monthly_amount);
                result.Add(e);
            }

            conn.Close();

            return result;
        }

        public bool AddCollegeLoan(DBConnection conn, int studentId, int monthlyAmount, int totalAmount)
        {
            bool result = false;

            string query;

            // Disable any existing college loans.
            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"UPDATE loans
SET removed = 1
WHERE user_id = ? AND category = ?;";
                    result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId), new ParamType(typeof(string), "College Loan"))) > 0;

                    break;
                case "postgresql":
                    query =
@"UPDATE loans
SET removed = 1
WHERE user_id = :1 AND category = :2;";
                    result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId), new ParamType(typeof(string), "College Loan"))) > 0;

                    break;
                case "sqlserver":
                    query =
@"UPDATE loans
SET removed = 1
WHERE user_id = @1 AND category = @2;";
                    result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId), new ParamType(typeof(string), "College Loan"))) > 0;

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }
            result = true; // 0 rows might have been updated.

            // Create the updated loan information.
            if(result)
            {
                int months = ((monthlyAmount > 0) ? ((totalAmount + (monthlyAmount - 1)) / monthlyAmount) : totalAmount);
                switch (conn.DriverClass)
                {
                    case "mysql":
                        query =
@"INSERT INTO loans (user_id, active_start, active_end, category, total_amount, monthly_amount)
VALUES (?, (SELECT MAX(current_game_time) FROM game_state), (SELECT MAX(current_game_time) FROM game_state) + interval ? month, ?, ?, ?)";
                        result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId), new ParamType(typeof(int), months), new ParamType(typeof(string), "College Loan"), new ParamType(typeof(int), totalAmount), new ParamType(typeof(int), monthlyAmount))) > 0;

                        break;
                    case "postgresql":
                        query =
@"INSERT INTO loans (user_id, active_start, active_end, category, total_amount, monthly_amount)
VALUES (:1, (SELECT MAX(current_game_time) FROM game_state), (SELECT MAX(current_game_time) FROM game_state) + concat(:2::text, ' months')::interval, :3, :4, :5)";
                        result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId), new ParamType(typeof(int), months), new ParamType(typeof(string), "College Loan"), new ParamType(typeof(int), totalAmount), new ParamType(typeof(int), monthlyAmount))) > 0;

                        break;
                    case "sqlserver":
                        query =
@"INSERT INTO loans (user_id, active_start, active_end, category, total_amount, monthly_amount)
VALUES (@1, (SELECT MAX(current_game_time) FROM game_state), DATEADD(@2, 'month', (SELECT MAX(current_game_time) FROM game_state)), @3, @4, @5)";
                        result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId), new ParamType(typeof(int), months), new ParamType(typeof(string), "College Loan"), new ParamType(typeof(int), totalAmount), new ParamType(typeof(int), monthlyAmount))) > 0;

                        break;
                    default:
                        throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
                }
            }

            return result;
        }

        public bool AddCarLoan(DBConnection conn, int studentId, int carId, int downPayment, int totalAmount, int months)
        {
            bool result = false;

            string query;

            // Find the interest for Car Loans.
            List<Dictionary<string, object>> rs = null;
            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"SELECT *
FROM class_configuration
WHERE class_id = 1";
                    rs = conn.ExecuteQuery(query);

                    break;
                case "postgresql":
                    query =
@"SELECT *
FROM class_configuration
WHERE class_id = 1";
                    rs = conn.ExecuteQuery(query);

                    break;
                case "sqlserver":
                    query =
@"SELECT *
FROM class_configuration
WHERE class_id = 1";
                    rs = conn.ExecuteQuery(query);

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }

            double autoLoanInterest = 0;
            foreach (Dictionary<string, object> row in rs)
            {
                object auto_loan_interest;

                row.TryGetValue("auto_loan_interest", out auto_loan_interest);

                autoLoanInterest = Convert.ToDouble(auto_loan_interest);
            }

            // Figure out the actual loan amount.
            int realAmount = totalAmount - downPayment;
            if(realAmount < 0)
            {
                realAmount = 0;
            }
            if(realAmount == 0)
            {
                return true;
            }
            realAmount = (int)Math.Ceiling((double)realAmount * (1.0 + autoLoanInterest));

            if(months <= 0)
            {
                months = 1;
            }
            int monthlyAmount = (realAmount + months - 1) / months;

            // Disable any existing college loans.
            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"UPDATE loans
SET removed = 1
WHERE user_id = ? AND category = ?;";
                    result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId), new ParamType(typeof(string), "Car Loan " + carId))) > 0;

                    break;
                case "postgresql":
                    query =
@"UPDATE loans
SET removed = 1
WHERE user_id = :1 AND category = :2;";
                    result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId), new ParamType(typeof(string), "Car Loan " + carId))) > 0;

                    break;
                case "sqlserver":
                    query =
@"UPDATE loans
SET removed = 1
WHERE user_id = @1 AND category = @2;";
                    result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId), new ParamType(typeof(string), "Car Loan " + carId))) > 0;

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }
            result = true; // 0 rows might have been updated.

            // Create the updated loan information.
            if (result)
            {
                switch (conn.DriverClass)
                {
                    case "mysql":
                        query =
@"INSERT INTO loans (user_id, active_start, active_end, category, total_amount, monthly_amount)
VALUES (?, (SELECT MAX(current_game_time) FROM game_state), (SELECT MAX(current_game_time) FROM game_state) + interval ? month, ?, ?, ?)";
                        result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId), new ParamType(typeof(int), months), new ParamType(typeof(string), "Car Loan " + carId), new ParamType(typeof(int), realAmount), new ParamType(typeof(int), monthlyAmount))) > 0;

                        break;
                    case "postgresql":
                        query =
@"INSERT INTO loans (user_id, active_start, active_end, category, total_amount, monthly_amount)
VALUES (:1, (SELECT MAX(current_game_time) FROM game_state), (SELECT MAX(current_game_time) FROM game_state) + concat(:2::text, ' months')::interval, :3, :4, :5)";
                        result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId), new ParamType(typeof(int), months), new ParamType(typeof(string), "Car Loan " + carId), new ParamType(typeof(int), realAmount), new ParamType(typeof(int), monthlyAmount))) > 0;

                        break;
                    case "sqlserver":
                        query =
@"INSERT INTO loans (user_id, active_start, active_end, category, total_amount, monthly_amount)
VALUES (@1, (SELECT MAX(current_game_time) FROM game_state), DATEADD(@2, 'month', (SELECT MAX(current_game_time) FROM game_state)), @3, @4, @5)";
                        result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId), new ParamType(typeof(int), months), new ParamType(typeof(string), "Car Loan " + carId), new ParamType(typeof(int), realAmount), new ParamType(typeof(int), monthlyAmount))) > 0;

                        break;
                    default:
                        throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
                }
            }

            return result;
        }

        public bool AddHomeLoan(DBConnection conn, int studentId, int carId, int downPayment, int totalAmount, int years)
        {
            bool result = false;

            string query;

            // Find the interest for Car Loans.
            List<Dictionary<string, object>> rs = null;
            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"SELECT *
FROM class_configuration
WHERE class_id = 1";
                    rs = conn.ExecuteQuery(query);

                    break;
                case "postgresql":
                    query =
@"SELECT *
FROM class_configuration
WHERE class_id = 1";
                    rs = conn.ExecuteQuery(query);

                    break;
                case "sqlserver":
                    query =
@"SELECT *
FROM class_configuration
WHERE class_id = 1";
                    rs = conn.ExecuteQuery(query);

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }

            double homeLoanInterest = 0.0;
            foreach (Dictionary<string, object> row in rs)
            {
                object mortage_interest;

                row.TryGetValue("mortgage_interest", out mortage_interest);

                homeLoanInterest = Convert.ToDouble(mortage_interest);
            }

            // Figure out the actual loan amount.
            int realAmount = totalAmount - downPayment;
            if (realAmount < 0)
            {
                realAmount = 0;
            }
            if (realAmount == 0)
            {
                return true;
            }
            realAmount = (int)Math.Ceiling((double)realAmount * (1.0 + homeLoanInterest));

            if (years <= 0)
            {
                years = 1;
            }
            int monthlyAmount = (realAmount + years - 1) / (years * 12);

            // Disable any existing college loans.
            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"UPDATE loans
SET removed = 1
WHERE user_id = ? AND category = ?;";
                    result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId), new ParamType(typeof(string), "Mortage " + carId))) > 0;

                    break;
                case "postgresql":
                    query =
@"UPDATE loans
SET removed = 1
WHERE user_id = :1 AND category = :2;";
                    result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId), new ParamType(typeof(string), "Mortage " + carId))) > 0;

                    break;
                case "sqlserver":
                    query =
@"UPDATE loans
SET removed = 1
WHERE user_id = @1 AND category = @2;";
                    result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId), new ParamType(typeof(string), "Mortage " + carId))) > 0;

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }
            result = true; // 0 rows might have been updated.

            // Create the updated loan information.
            if (result)
            {
                switch (conn.DriverClass)
                {
                    case "mysql":
                        query =
@"INSERT INTO loans (user_id, active_start, active_end, category, total_amount, monthly_amount)
VALUES (?, (SELECT MAX(current_game_time) FROM game_state), (SELECT MAX(current_game_time) FROM game_state) + interval ? month, ?, ?, ?)";
                        result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId), new ParamType(typeof(int), years*12), new ParamType(typeof(string), "Mortage " + carId), new ParamType(typeof(int), realAmount), new ParamType(typeof(int), monthlyAmount))) > 0;

                        break;
                    case "postgresql":
                        query =
@"INSERT INTO loans (user_id, active_start, active_end, category, total_amount, monthly_amount)
VALUES (:1, (SELECT MAX(current_game_time) FROM game_state), (SELECT MAX(current_game_time) FROM game_state) + concat(:2::text, ' months')::interval, :3, :4, :5)";
                        result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId), new ParamType(typeof(int), years*12), new ParamType(typeof(string), "Mortage " + carId), new ParamType(typeof(int), realAmount), new ParamType(typeof(int), monthlyAmount))) > 0;

                        break;
                    case "sqlserver":
                        query =
@"INSERT INTO loans (user_id, active_start, active_end, category, total_amount, monthly_amount)
VALUES (@1, (SELECT MAX(current_game_time) FROM game_state), DATEADD(@2, 'month', (SELECT MAX(current_game_time) FROM game_state)), @3, @4, @5)";
                        result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId), new ParamType(typeof(int), years*12), new ParamType(typeof(string), "Mortage " + carId), new ParamType(typeof(int), realAmount), new ParamType(typeof(int), monthlyAmount))) > 0;

                        break;
                    default:
                        throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
                }
            }

            return result;
        }

        public bool AddHomeRental(DBConnection conn, int studentId, int carId, int price)
        {
            bool result = false;

            string query;

            // Disable any existing college loans.
            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"UPDATE loans
SET removed = 1
WHERE user_id = ? AND category = ?;";
                    result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId), new ParamType(typeof(string), "Rental " + carId))) > 0;

                    break;
                case "postgresql":
                    query =
@"UPDATE loans
SET removed = 1
WHERE user_id = :1 AND category = :2;";
                    result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId), new ParamType(typeof(string), "Rental " + carId))) > 0;

                    break;
                case "sqlserver":
                    query =
@"UPDATE loans
SET removed = 1
WHERE user_id = @1 AND category = @2;";
                    result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId), new ParamType(typeof(string), "Rental " + carId))) > 0;

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }
            result = true; // 0 rows might have been updated.

            // Create the updated loan information.
            if (result)
            {
                switch (conn.DriverClass)
                {
                    case "mysql":
                        query =
@"INSERT INTO loans (user_id, active_start, active_end, category, total_amount, monthly_amount)
VALUES (?, (SELECT MAX(current_game_time) FROM game_state), (SELECT MAX(current_game_time) FROM game_state) + interval 200 year, ?, 0, ?)";
                        result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId), new ParamType(typeof(string), "Rental " + carId), new ParamType(typeof(int), price))) > 0;

                        break;
                    case "postgresql":
                        query =
@"INSERT INTO loans (user_id, active_start, active_end, category, total_amount, monthly_amount)
VALUES (:1, (SELECT MAX(current_game_time) FROM game_state), (SELECT MAX(current_game_time) FROM game_state) + concat('200', ' years')::interval, :2, 0, :3)";
                        result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId), new ParamType(typeof(string), "Rental " + carId), new ParamType(typeof(int), price))) > 0;

                        break;
                    case "sqlserver":
                        query =
@"INSERT INTO loans (user_id, active_start, active_end, category, total_amount, monthly_amount)
VALUES (@1, (SELECT MAX(current_game_time) FROM game_state), DATEADD(200, 'year', (SELECT MAX(current_game_time) FROM game_state)), @2, 0, @3)";
                        result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId), new ParamType(typeof(string), "Rental " + carId), new ParamType(typeof(int), price))) > 0;

                        break;
                    default:
                        throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
                }
            }

            return result;
        }
    }
}
