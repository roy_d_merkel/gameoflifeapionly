﻿//
//  BankRepository.cs
//
//  Author:
//       Joe Merkel <merkeljoe9@gmail.com>
//
//  Copyright (c) 2018 Joe Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using GameOfLife.Entities;
using T4R.Data;

namespace GameOfLife.Models
{
    public class BankRepository
    {
        public Bank Get(DBConnection conn, int studentId)
        {
            Bank result = null;

            string query;
            List<Dictionary<string, object>> rs = null;

            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"INSERT INTO bank (user_id, cash_on_hand, checking, savings)
SELECT u.user_id, 0, 0, 0
FROM users u
left join bank b ON b.user_id = u.user_id
WHERE u.removed = 0 AND u.user_id = ? AND b.user_id IS NULL;";
                    conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId));

                    break;
                case "postgresql":
                    query =
@"INSERT INTO bank (user_id, cash_on_hand, checking, savings)
SELECT u.user_id, 0, 0, 0
FROM users u
left join bank b ON b.user_id = u.user_id
WHERE u.removed = 0 AND u.user_id = :1 AND b.user_id IS NULL;";
                    conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId));

                    break;
                case "sqlserver":
                    query =
@"INSERT INTO bank (user_id, cash_on_hand, checking, savings)
SELECT u.user_id, 0, 0, 0
FROM users u
left join bank b ON b.user_id = u.user_id
WHERE u.removed = 0 AND u.user_id = @1 AND b.user_id IS NULL;";
                    conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId));

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }

            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"SELECT *
FROM bank
WHERE user_id = ?";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId));

                    break;
                case "postgresql":
                    query =
@"SELECT *
FROM bank
WHERE user_id = :1";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId));

                    break;
                case "sqlserver":
                    query =
@"SELECT *
FROM bank
WHERE user_id = @1";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId));

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }

            foreach (Dictionary<string, object> row in rs)
            {
                object user_id;
                object cash_on_hand;
                object checking;
                object savings;

                row.TryGetValue("user_id", out user_id);
                row.TryGetValue("cash_on_hand", out cash_on_hand);
                row.TryGetValue("checking", out checking);
                row.TryGetValue("savings", out savings);

                result = new Bank((int)user_id, (int)cash_on_hand, (int)checking, (int)savings);
                break;
            }

            return result;
        }

        public bool Set(DBConnection conn, int studentId, int cash_on_hand)
        {
            bool result = false;

            string query;


            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"INSERT INTO bank (user_id, cash_on_hand, checking, savings)
SELECT u.user_id, 0, 0, 0
FROM users u
left join bank b ON b.user_id = u.user_id
WHERE u.removed = 0 AND u.user_id = ? AND b.user_id IS NULL;";
                    conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId));

                    break;
                case "postgresql":
                    query =
@"INSERT INTO bank (user_id, cash_on_hand, checking, savings)
SELECT u.user_id, 0, 0, 0
FROM users u
left join bank b ON b.user_id = u.user_id
WHERE u.removed = 0 AND u.user_id = :1 AND b.user_id IS NULL;";
                    conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId));

                    break;
                case "sqlserver":
                    query =
@"INSERT INTO bank (user_id, cash_on_hand, checking, savings)
SELECT u.user_id, 0, 0, 0
FROM users u
left join bank b ON b.user_id = u.user_id
WHERE u.removed = 0 AND u.user_id = @1 AND b.user_id IS NULL;";
                    conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId));

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }

            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"UPDATE bank
SET cash_on_hand = ?
WHERE user_id = ?";
                    result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), cash_on_hand), new ParamType(typeof(int), studentId)) > 0);

                    break;
                case "postgresql":
                    query =
@"UPDATE bank
SET cash_on_hand = :1
WHERE user_id = :2";
                    result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), cash_on_hand), new ParamType(typeof(int), studentId)) > 0);

                    break;
                case "sqlserver":
                    query =
@"UPDATE bank
SET cash_on_hand = @1
WHERE user_id = @2";
                    result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), cash_on_hand), new ParamType(typeof(int), studentId)) > 0);

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }

            return result;
        }

        public bool DepositChecking(DBConnection conn, int studentId, int amount)
        {
            bool result = false;

            string query;

            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"INSERT INTO bank (user_id, cash_on_hand, checking, savings)
SELECT u.user_id, 0, 0, 0
FROM users u
left join bank b ON b.user_id = u.user_id
WHERE u.removed = 0 AND u.user_id = ? AND b.user_id IS NULL;";
                    conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId));

                    break;
                case "postgresql":
                    query =
@"INSERT INTO bank (user_id, cash_on_hand, checking, savings)
SELECT u.user_id, 0, 0, 0
FROM users u
left join bank b ON b.user_id = u.user_id
WHERE u.removed = 0 AND u.user_id = :1 AND b.user_id IS NULL;";
                    conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId));

                    break;
                case "sqlserver":
                    query =
@"INSERT INTO bank (user_id, cash_on_hand, checking, savings)
SELECT u.user_id, 0, 0, 0
FROM users u
left join bank b ON b.user_id = u.user_id
WHERE u.removed = 0 AND u.user_id = @1 AND b.user_id IS NULL;";
                    conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId));

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }

            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"UPDATE bank
SET cash_on_hand = cash_on_hand - ?, checking = checking + ?, opened_bank_checking=1
WHERE user_id = ?";
                    result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), amount), new ParamType(typeof(int), amount), new ParamType(typeof(int), studentId)) > 0);

                    break;
                case "postgresql":
                    query =
@"UPDATE bank
SET cash_on_hand = cash_on_hand - :1, checking = checking + :1, opened_bank_checking=1 
WHERE user_id = :2";
                    result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), amount), new ParamType(typeof(int), studentId)) > 0);

                    break;
                case "sqlserver":
                    query =
@"UPDATE bank
SET cash_on_hand = cash_on_hand - @1, checking = checking + @1, opened_bank_checking=1
WHERE user_id = @2";
                    result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), amount), new ParamType(typeof(int), studentId)) > 0);

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }

            return result;
        }

        public bool CheckingToSavings(DBConnection conn, int studentId, int amount)
        {
            bool result = false;

            string query;

            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"INSERT INTO bank (user_id, cash_on_hand, checking, savings)
SELECT u.user_id, 0, 0, 0
FROM users u
left join bank b ON b.user_id = u.user_id
WHERE u.removed = 0 AND u.user_id = ? AND b.user_id IS NULL;";
                    conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId));

                    break;
                case "postgresql":
                    query =
@"INSERT INTO bank (user_id, cash_on_hand, checking, savings)
SELECT u.user_id, 0, 0, 0
FROM users u
left join bank b ON b.user_id = u.user_id
WHERE u.removed = 0 AND u.user_id = :1 AND b.user_id IS NULL;";
                    conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId));

                    break;
                case "sqlserver":
                    query =
@"INSERT INTO bank (user_id, cash_on_hand, checking, savings)
SELECT u.user_id, 0, 0, 0
FROM users u
left join bank b ON b.user_id = u.user_id
WHERE u.removed = 0 AND u.user_id = @1 AND b.user_id IS NULL;";
                    conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId));

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }

            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"UPDATE bank
SET checking = checking - ?, savings = savings + ?, opened_bank_savings=1
WHERE user_id = ?";
                    result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), amount), new ParamType(typeof(int), amount), new ParamType(typeof(int), studentId)) > 0);

                    break;
                case "postgresql":
                    query =
@"UPDATE bank
SET checking = checking - :1, savings = savings + :1, opened_bank_savings=1
WHERE user_id = :2";
                    result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), amount), new ParamType(typeof(int), studentId)) > 0);

                    break;
                case "sqlserver":
                    query =
@"UPDATE bank
SET checking = checking - @1, savings = savings + @1, opened_bank_savings=1
WHERE user_id = @2";
                    result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), amount), new ParamType(typeof(int), studentId)) > 0);

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }

            return result;
        }

        public bool WithdrawChecking(DBConnection conn, int studentId, int amount)
        {
            bool result = false;

            string query;

            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"INSERT INTO bank (user_id, cash_on_hand, checking, savings)
SELECT u.user_id, 0, 0, 0
FROM users u
left join bank b ON b.user_id = u.user_id
WHERE u.removed = 0 AND u.user_id = ? AND b.user_id IS NULL;";
                    conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId));

                    break;
                case "postgresql":
                    query =
@"INSERT INTO bank (user_id, cash_on_hand, checking, savings)
SELECT u.user_id, 0, 0, 0
FROM users u
left join bank b ON b.user_id = u.user_id
WHERE u.removed = 0 AND u.user_id = :1 AND b.user_id IS NULL;";
                    conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId));

                    break;
                case "sqlserver":
                    query =
@"INSERT INTO bank (user_id, cash_on_hand, checking, savings)
SELECT u.user_id, 0, 0, 0
FROM users u
left join bank b ON b.user_id = u.user_id
WHERE u.removed = 0 AND u.user_id = @1 AND b.user_id IS NULL;";
                    conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId));

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }

            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"UPDATE bank
SET cash_on_hand = cash_on_hand + ?, checking = checking - ?
WHERE user_id = ?";
                    result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), amount), new ParamType(typeof(int), amount), new ParamType(typeof(int), studentId)) > 0);

                    break;
                case "postgresql":
                    query =
@"UPDATE bank
SET cash_on_hand = cash_on_hand + :1, checking = checking - :1
WHERE user_id = :2";
                    result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), amount), new ParamType(typeof(int), studentId)) > 0);

                    break;
                case "sqlserver":
                    query =
@"UPDATE bank
SET cash_on_hand = cash_on_hand + @1, checking = checking - @1
WHERE user_id = @2";
                    result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), amount), new ParamType(typeof(int), studentId)) > 0);

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }

            return result;
        }

        public bool DepositSavings(DBConnection conn, int studentId, int amount)
        {
            bool result = false;

            string query;

            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"INSERT INTO bank (user_id, cash_on_hand, checking, savings)
SELECT u.user_id, 0, 0, 0
FROM users u
left join bank b ON b.user_id = u.user_id
WHERE u.removed = 0 AND u.user_id = ? AND b.user_id IS NULL;";
                    conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId));

                    break;
                case "postgresql":
                    query =
@"INSERT INTO bank (user_id, cash_on_hand, checking, savings)
SELECT u.user_id, 0, 0, 0
FROM users u
left join bank b ON b.user_id = u.user_id
WHERE u.removed = 0 AND u.user_id = :1 AND b.user_id IS NULL;";
                    conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId));

                    break;
                case "sqlserver":
                    query =
@"INSERT INTO bank (user_id, cash_on_hand, checking, savings)
SELECT u.user_id, 0, 0, 0
FROM users u
left join bank b ON b.user_id = u.user_id
WHERE u.removed = 0 AND u.user_id = @1 AND b.user_id IS NULL;";
                    conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId));

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }

            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"UPDATE bank
SET cash_on_hand = cash_on_hand - ?, savings = savings + ?, opened_bank_savings=1
WHERE user_id = ?";
                    result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), amount), new ParamType(typeof(int), amount), new ParamType(typeof(int), studentId)) > 0);

                    break;
                case "postgresql":
                    query =
@"UPDATE bank
SET cash_on_hand = cash_on_hand - :1, savings = savings + :1, opened_bank_savings=1
WHERE user_id = :2";
                    result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), amount), new ParamType(typeof(int), studentId)) > 0);

                    break;
                case "sqlserver":
                    query =
@"UPDATE bank
SET cash_on_hand = cash_on_hand - @1, savings = savings + @1, opened_bank_savings=1
WHERE user_id = @2";
                    result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), amount), new ParamType(typeof(int), studentId)) > 0);

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }

            return result;
        }

        public bool SavingsToChecking(DBConnection conn, int studentId, int amount)
        {
            bool result = false;

            string query;

            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"INSERT INTO bank (user_id, cash_on_hand, checking, savings)
SELECT u.user_id, 0, 0, 0
FROM users u
left join bank b ON b.user_id = u.user_id
WHERE u.removed = 0 AND u.user_id = ? AND b.user_id IS NULL;";
                    conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId));

                    break;
                case "postgresql":
                    query =
@"INSERT INTO bank (user_id, cash_on_hand, checking, savings)
SELECT u.user_id, 0, 0, 0
FROM users u
left join bank b ON b.user_id = u.user_id
WHERE u.removed = 0 AND u.user_id = :1 AND b.user_id IS NULL;";
                    conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId));

                    break;
                case "sqlserver":
                    query =
@"INSERT INTO bank (user_id, cash_on_hand, checking, savings)
SELECT u.user_id, 0, 0, 0
FROM users u
left join bank b ON b.user_id = u.user_id
WHERE u.removed = 0 AND u.user_id = @1 AND b.user_id IS NULL;";
                    conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId));

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }

            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"UPDATE bank
SET savings = savings - ?, checking = checking + ?, opened_bank_checking=1
WHERE user_id = ?";
                    result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), amount), new ParamType(typeof(int), amount), new ParamType(typeof(int), studentId)) > 0);

                    break;
                case "postgresql":
                    query =
@"UPDATE bank
SET savings = savings - :1, checking = checking + :1, opened_bank_checking=1
WHERE user_id = :2";
                    result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), amount), new ParamType(typeof(int), studentId)) > 0);

                    break;
                case "sqlserver":
                    query =
@"UPDATE bank
SET savings = savings - @1, checking = checking + @1, opened_bank_checking=1
WHERE user_id = @2";
                    result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), amount), new ParamType(typeof(int), studentId)) > 0);

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }

            return result;
        }

        public bool WithdrawSavings(DBConnection conn, int studentId, int amount)
        {
            bool result = false;

            string query;

            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"INSERT INTO bank (user_id, cash_on_hand, checking, savings)
SELECT u.user_id, 0, 0, 0
FROM users u
left join bank b ON b.user_id = u.user_id
WHERE u.removed = 0 AND u.user_id = ? AND b.user_id IS NULL;";
                    conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId));

                    break;
                case "postgresql":
                    query =
@"INSERT INTO bank (user_id, cash_on_hand, checking, savings)
SELECT u.user_id, 0, 0, 0
FROM users u
left join bank b ON b.user_id = u.user_id
WHERE u.removed = 0 AND u.user_id = :1 AND b.user_id IS NULL;";
                    conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId));

                    break;
                case "sqlserver":
                    query =
@"INSERT INTO bank (user_id, cash_on_hand, checking, savings)
SELECT u.user_id, 0, 0, 0
FROM users u
left join bank b ON b.user_id = u.user_id
WHERE u.removed = 0 AND u.user_id = @1 AND b.user_id IS NULL;";
                    conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId));

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }

            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"UPDATE bank
SET cash_on_hand = cash_on_hand + ?, savings = savings - ?
WHERE user_id = ?";
                    result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), amount), new ParamType(typeof(int), amount), new ParamType(typeof(int), studentId)) > 0);

                    break;
                case "postgresql":
                    query =
@"UPDATE bank
SET cash_on_hand = cash_on_hand + :1, savings = savings - :1
WHERE user_id = :2";
                    result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), amount), new ParamType(typeof(int), studentId)) > 0);

                    break;
                case "sqlserver":
                    query =
@"UPDATE bank
SET cash_on_hand = cash_on_hand + @1, savings = savings - @1
WHERE user_id = @2";
                    result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), amount), new ParamType(typeof(int), studentId)) > 0);

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }

            return result;
        }
    }
}
