﻿//
//  JobsRepository.cs
//
//  Author:
//       Joe Merkel <merkeljoe9@gmail.com>
//
//  Copyright (c) 2018 Joe Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using GameOfLife.Entities;
using T4R.Data;

namespace GameOfLife.Models
{
    public class JobsRepository
    {
        public bool AddEditCollegeJobs(DBConnection conn, int studentId, string program_name, int salary)
        {
            bool result = false;

            string query;

            // See if the loan already exists.
            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"UPDATE job
SET removed = 1
WHERE user_id = ? AND from_college > 0;";
                    result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId))) > 0;

                    break;
                case "postgresql":
                    query =
@"UPDATE job
SET removed = 1
WHERE user_id = :1 AND from_college > 0;";
                    result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId))) > 0;

                    break;
                case "sqlserver":
                    query =
@"UPDATE job
SET removed = 1
WHERE user_id = @1 AND from_college > 0;";
                    result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId))) > 0;

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }
            result = true; // 0 rows might have been updated.

            // Create the updated loan information.
            if (result)
            {
                switch (conn.DriverClass)
                {
                    case "mysql":
                        query =
@"INSERT INTO job (user_id, from_college, title, type, salary, monthly_take_home)
VALUES (?, 1, ?, ?, ?, ?);";
                        result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId), new ParamType(typeof(string), program_name), new ParamType(typeof(string), "Full Time"), new ParamType(typeof(int), salary), new ParamType(typeof(int), salary/12))) > 0;

                        break;
                    case "postgresql":
                        query =
@"INSERT INTO job (user_id, from_college, title, type, salary, monthly_take_home)
VALUES (:1, 1, :2, :3, :4, :5);";
                        result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId), new ParamType(typeof(string), program_name), new ParamType(typeof(string), "Full Time"), new ParamType(typeof(int), salary), new ParamType(typeof(int), salary / 12))) > 0;

                        break;
                    case "sqlserver":
                        query =
@"INSERT INTO job (user_id, from_college, title, type, salary, monthly_take_home)
VALUES (@1, 1, @2, @3, @4, @5);";
                        result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId), new ParamType(typeof(string), program_name), new ParamType(typeof(string), "Full Time"), new ParamType(typeof(int), salary), new ParamType(typeof(int), salary / 12))) > 0;

                        break;
                    default:
                        throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
                }
            }

            return result;
        }

		public double GetIncomeTax(DBConnection conn, int userId)
        {
            double result = 0;
            string query;
            List<Dictionary<string, object>> rs = null;
            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"SELECT cc.income_tax
FROM user_class uc
INNER JOIN class_configuration cc ON uc.class_id = cc.class_id
WHERE uc.user_id = ?";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), userId));

                    break;
                case "postgresql":
                    query =
@"SELECT cc.income_tax
FROM user_class uc
INNER JOIN class_configuration cc ON uc.class_id = cc.class_id
WHERE uc.user_id = :1";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), userId));

                    break;
                case "sqlserver":
                    query =
@"SELECT cc.income_tax
FROM user_class uc
INNER JOIN class_configuration cc ON uc.class_id = cc.class_id
WHERE uc.user_id = @1";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), userId));

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }

            foreach (Dictionary<string, object> row in rs)
            {
				object income_tax;

				row.TryGetValue("income_tax", out income_tax);

				result = Convert.ToDouble(income_tax);
                break;
            }

            return result;
        }

        public bool AddOrEditJobs(DBConnection conn, int studentId, int jobId, Job job)
        {
            bool result = false;

            string query;

			// See if the loan already exists.
			if (jobId > 0)
			{
				switch (conn.DriverClass)
				{
					case "mysql":
						query =
@"UPDATE job
SET removed = 1
WHERE user_id = ? AND job_id = ?;";
						conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId),
										   new ParamType(typeof(int), jobId));

						break;
					case "postgresql":
						query =
@"UPDATE job
SET removed = 1
WHERE user_id = :1 AND job_id = :2;";
						conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId),
										   new ParamType(typeof(int), jobId));

						break;
					case "sqlserver":
						query =
@"UPDATE job
SET removed = 1
WHERE user_id = @1 AND job_id = @2;";
						conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId),
										   new ParamType(typeof(int), jobId));
						break;
					default:
						throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
				}
			}
            result = true; // 0 rows might have been updated.

            // Create the updated loan information.
            if (result)
            {
                switch (conn.DriverClass)
                {
                    case "mysql":
                        query =
@"INSERT INTO job (user_id, from_college, title, type, salary, monthly_take_home, approved)
VALUES (?, ?, ?, ?, ?, ?, ?);";
						result = (conn.ExecuteUpdate(query,
                                                     new ParamType(typeof(int), studentId),
                                                     new ParamType(typeof(bool), job.from_college),
                                                     new ParamType(typeof(string), job.title),
                                                     new ParamType(typeof(string), job.type),
                                                     new ParamType(typeof(int), job.salary),
                                                     new ParamType(typeof(int), job.salary / 12),
                                                     new ParamType(typeof(bool), job.approved)
                                                    )) > 0;
						
                        break;
                    case "postgresql":
                        query =
@"INSERT INTO job (user_id, from_college, title, type, salary, monthly_take_home, approved)
VALUES (:1, :2, :3, :4, :5, :6, :7);";
						result = (conn.ExecuteUpdate(query,
                                                     new ParamType(typeof(int), studentId),
                                                     new ParamType(typeof(bool), job.from_college),
                                                     new ParamType(typeof(string), job.title),
                                                     new ParamType(typeof(string), job.type),
                                                     new ParamType(typeof(int), job.salary),
                                                     new ParamType(typeof(int), job.salary / 12),
                                                     new ParamType(typeof(bool), job.approved)
                                                    )) > 0;
						
                        break;
                    case "sqlserver":
                        query =
@"INSERT INTO job (user_id, from_college, title, type, salary, monthly_take_home, approved)
VALUES (@1, @2, @3, @4, @5, @6, @7);";
                        result = (conn.ExecuteUpdate(query, 
						                             new ParamType(typeof(int), studentId),
						                             new ParamType(typeof(bool), job.from_college),
						                             new ParamType(typeof(string), job.title),
						                             new ParamType(typeof(string), job.type),
						                             new ParamType(typeof(int), job.salary),
						                             new ParamType(typeof(int), job.salary / 12),
                                                     new ParamType(typeof(bool), job.approved)
                                                    ))> 0;

                        break;
                    default:
                        throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
                }
            }

            return result;
        }

		public bool Delete(DBConnection conn, int studentId, int jobId)
        {
            bool result = false;

            string query;

            // See if the loan already exists.
            if (jobId > 0)
            {
                switch (conn.DriverClass)
                {
                    case "mysql":
                        query =
@"UPDATE job
SET removed = 1
WHERE user_id = ? AND job_id = ?;";
                        result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId),
						                             new ParamType(typeof(int), jobId))) > 0;

                        break;
                    case "postgresql":
                        query =
@"UPDATE job
SET removed = 1
WHERE user_id = :1 AND job_id = :2;";
						result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId),
						                             new ParamType(typeof(int), jobId))) > 0;

                        break;
                    case "sqlserver":
                        query =
@"UPDATE job
SET removed = 1
WHERE user_id = @1 AND job_id = @2;";
						result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId),
						                             new ParamType(typeof(int), jobId))) > 0;
                        break;
                    default:
                        throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
                }
            }

            return result;
        }

		public Job[] Get(DBConnection conn, int userId, bool includeUnapproved = true)
        {
			double incomeTax = GetIncomeTax(conn, userId);
            List<Job> jobs = new List<Job>();
            string query;
            List<Dictionary<string, object>> rs = null;
            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"SELECT *
FROM job
WHERE user_id = ? AND NOT removed AND (? OR approved)";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), userId), new ParamType(typeof(bool), includeUnapproved));

                    break;
                case "postgresql":
                    query =
@"SELECT *
FROM job
WHERE user_id = :1 AND NOT removed AND (:2 OR approved)";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), userId), new ParamType(typeof(bool), includeUnapproved));

                    break;
                case "sqlserver":
                    query =
                        @"SELECT *
FROM job
WHERE user_id = @1 AND NOT removed AND (@2 OR approved)";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), userId), new ParamType(typeof(bool), includeUnapproved));

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }

            foreach (Dictionary<string, object> row in rs)
            {
                object job_id;
                object user_id;
                object from_college;
                object title;
                object type;
                object salary;
                object monthly_take_home;
                object approved;
                object recorded_on;
                object removed;

                row.TryGetValue("job_id", out job_id);
                row.TryGetValue("user_id", out user_id);
                row.TryGetValue("from_college", out from_college);
                row.TryGetValue("title", out title);
                row.TryGetValue("type", out type);
                row.TryGetValue("salary", out salary);
                row.TryGetValue("monthly_take_home", out monthly_take_home);
                row.TryGetValue("approved", out approved);
                row.TryGetValue("recorded_on", out recorded_on);
                row.TryGetValue("removed", out removed);
                
				double mon = Convert.ToInt32(monthly_take_home);
                
				mon = mon - incomeTax * mon;
                
				int monc = Convert.ToInt32(Math.Ceiling(mon));

				Job job = new Job(Convert.ToInt32(job_id), Convert.ToInt32(user_id), Convert.ToBoolean(from_college), Convert.ToString(title), Convert.ToString(type), Convert.ToInt32(salary), monc, Convert.ToBoolean(approved), (DateTime?)recorded_on, Convert.ToBoolean(removed));
                jobs.Add(job);
            }

            return jobs.ToArray();
        }

        public bool Approve(DBConnection conn, int studentId, int jobId)
        {
            bool result = false;

            string query;

            // See if the loan already exists.
            if (jobId > 0)
            {
                switch (conn.DriverClass)
                {
                    case "mysql":
                        query =
@"UPDATE job
SET approved = 1
WHERE user_id = ? AND job_id = ?;";
                        result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId),
                                                     new ParamType(typeof(int), jobId))) > 0;

                        break;
                    case "postgresql":
                        query =
@"UPDATE job
SET approved = 1
WHERE user_id = :1 AND job_id = :2;";
                        result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId),
                                                     new ParamType(typeof(int), jobId))) > 0;

                        break;
                    case "sqlserver":
                        query =
@"UPDATE job
SET approved = 1
WHERE user_id = @1 AND job_id = @2;";
                        result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId),
                                                     new ParamType(typeof(int), jobId))) > 0;
                        break;
                    default:
                        throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
                }
            }

            return result;
        }

        public bool Disapprove(DBConnection conn, int studentId, int jobId)
        {
            bool result = false;

            string query;

            // See if the loan already exists.
            if (jobId > 0)
            {
                switch (conn.DriverClass)
                {
                    case "mysql":
                        query =
@"UPDATE job
SET approved = 0
WHERE user_id = ? AND job_id = ?;";
                        result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId),
                                                     new ParamType(typeof(int), jobId))) > 0;

                        break;
                    case "postgresql":
                        query =
@"UPDATE job
SET approved = 0
WHERE user_id = :1 AND job_id = :2;";
                        result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId),
                                                     new ParamType(typeof(int), jobId))) > 0;

                        break;
                    case "sqlserver":
                        query =
@"UPDATE job
SET approved = 0
WHERE user_id = @1 AND job_id = @2;";
                        result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId),
                                                     new ParamType(typeof(int), jobId))) > 0;
                        break;
                    default:
                        throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
                }
            }

            return result;
        }
    }
}
