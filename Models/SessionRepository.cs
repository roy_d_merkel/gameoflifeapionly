﻿//
//  LoginRepository.cs
//
//  Author:
//       Roy Merkel <merkel-roy@comcast.net>
//
//  Copyright (c) 2018 Roy Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.SessionState;
using GameOfLife.Entities;
using T4R.Data;

namespace GameOfLife.Models
{
    public class SessionRepository
    {
        UserRepository userRepository = null;

        public SessionRepository()
        {
            userRepository = new UserRepository();
        }

		public string CreateSessionToken(DBConnection conn)
        {
            List<Dictionary<string, object>> rs = null;

            string query;
            bool exists;
            Random random = new Random();
            string digits = null;
            int numDigits = 64;

            if (numDigits % 2 == 1)
            {
                numDigits++;
            }

            do
            {
                int i = 0;
                digits = "";

                exists = false;

                for (i = 0; i < numDigits / 2 * 2; i += 2)
                {
                    digits += random.Next(0, 0x100).ToString("X2");
                }

                query =
    @"SELECT IF(COUNT(s.session_id) > 0, 1, 0) AS exi
FROM sessions s
WHERE s.php_session_id = ?";
                rs = conn.ExecuteQuery(query, new ParamType(typeof(string), digits));

                foreach (Dictionary<string, object> row in rs)
                {
                    object exi;

                    row.TryGetValue("exi", out exi);

                    exists = (Convert.ToInt32(exi) != 0);
                }
            } while (exists);
                     
            return digits;
        }

        public void insertSession(DBConnection conn, int userId, string ipAddress, string browserInfo, DateTime startTime, out string token)
        {
            int res = -1;
            string query;

            token = CreateSessionToken(conn);

            query =
@"INSERT INTO sessions(user_id, ip_address, browser_info, start_time, php_session_id)
VALUES (?, ?, ?, ?, ?)";
            res = conn.ExecuteInsert(query, 
                                     new ParamType(typeof(int), userId), 
                                     new ParamType(typeof(string), ipAddress), 
                                     new ParamType(typeof(string), browserInfo), 
                                     new ParamType(typeof(DateTime), startTime), 
                                     new ParamType(typeof(string), token));
        }

		public string DoLogin(DBConnection conn, HttpSessionState session, string userName, string password, string ipAddress, string browserInfo, DateTime attemptedOn)
        {
            IAuthUser user = null;

            return DoLogin(conn, session, userName, password, ipAddress, browserInfo, attemptedOn, out user);
        }

        public string DoLogin(DBConnection conn, HttpSessionState session, string userName, string password, string ipAddress, string browserInfo, DateTime attemptedOn, out IAuthUser user)
        {
            user = userRepository.Get(conn, userName, password);

            if (user == null || user.user_id <= 0)
            {
                return null;
            }
            else
            {
                string token = null;

                insertSession(conn, user.user_id, ipAddress, browserInfo, attemptedOn, out token);

                return token;
            }
        }

		public Session GetSession(DBConnection conn, string token)
        {         
            string query;

            query =
@"SELECT s.*
FROM sessions s
WHERE s.php_session_id = ?;";
            
            List<Dictionary<string, object>> rs = conn.ExecuteQuery(query, new ParamType(typeof(string), token));
            Session s = null;

            foreach (Dictionary<string, object> row in rs)
            {
                object session_id;
                object user_id;
                object ip_address;
                object browser_info;
                object start_time;
                object end_time;
                object heart_beat;
                object php_session_id;

                row.TryGetValue("session_id", out session_id);
                row.TryGetValue("user_id", out user_id);
                row.TryGetValue("ip_address", out ip_address);
                row.TryGetValue("browser_info", out browser_info);
                row.TryGetValue("start_time", out start_time);
                row.TryGetValue("end_time", out end_time);
                row.TryGetValue("heart_beat", out heart_beat);
                row.TryGetValue("php_session_id", out php_session_id);
                s = new Session(Convert.ToInt32(session_id), Convert.ToInt32(user_id), Convert.ToString(ip_address), Convert.ToString(browser_info), (DateTime?)start_time, (DateTime?)end_time, (DateTime?)heart_beat, Convert.ToString(php_session_id));
            }
                     
            return s;
        }

		public bool IsFirstLogin(DBConnection conn, int userId)
		{
			bool ret = true;

            string query;

            query =
@"SELECT COUNT(s.session_id) AS cnt
FROM sessions s
WHERE s.user_id = ?;";

			List<Dictionary<string, object>> rs = conn.ExecuteQuery(query, new ParamType(typeof(int), userId));

            foreach (Dictionary<string, object> row in rs)
            {
				object cnt;

				row.TryGetValue("cnt", out cnt);

				if (Convert.ToInt32(cnt) > 1)
				{
					ret = false;
				}
            }

            return ret;
		}
    }
}
