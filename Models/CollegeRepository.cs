﻿//
//  CollegeRepository.cs
//
//  Author:
//       Roy Merkel <merkel-roy@comcast.net>
//
//  Copyright (c) 2018 Roy Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using GameOfLife.Entities;
using T4R.Data;

namespace GameOfLife.Models
{
    public class CollegeRepository
    {
        public CollegeRepository()
        {
        }

		public bool Exists(DBConnection conn, int userId)
        {
			bool exists = false;
            string query =
@"SELECT IF(COUNT(c.user_id) > 0, 1, 0) AS exi
FROM college c
WHERE c.user_id = ?;";

            List<Dictionary<string, object>> rs = conn.ExecuteQuery(query, new ParamType(typeof(int), userId));
            College c = null;

            foreach (Dictionary<string, object> row in rs)
            {
				object exi;

				row.TryGetValue("exi", out exi);

				try
				{
					int exist = Convert.ToInt32(exi);

					exists = (exist != 0);
				}
				catch (Exception ex)
				{
					exists = false;
				}
            }

            return exists;
        }

		public bool Set(DBConnection conn, int userId, College college)
		{
			/*if (Exists(conn, userId))
			{
				string query =
@"UPDATE college
SET college_name = ?, 
    school_degree = ?, 
    program_name = ?, 
    school_debt = ?,
    school_loan_payment = ?,
    salary = ?
WHERE user_id = ?;";
				return conn.ExecuteUpdate(query, new ParamType[] { new ParamType( typeof(string), college.college_name),
					new ParamType( typeof(string), college.school_degree),
					new ParamType( typeof(string), college.program_name),
					new ParamType( typeof(decimal), college.school_debt),
					new ParamType( typeof(decimal), college.school_loan_payment),
					new ParamType( typeof(decimal), college.salary),
					new ParamType( typeof(int), college.user_id) }) >= 0;
			}
			else
			{*/
				string query =
@"INSERT INTO college (user_id, college_name, school_degree, program_name, school_debt, school_loan_payment, salary)
VALUES
(?, ?, ?, ?, ?, ?, ?)";
				return conn.ExecuteUpdate(query, new ParamType[] { new ParamType( typeof(int), college.user_id),
					new ParamType( typeof(string), college.college_name),
                    new ParamType( typeof(string), college.school_degree),
                    new ParamType( typeof(string), college.program_name),
                    new ParamType( typeof(decimal), college.school_debt),
                    new ParamType( typeof(decimal), college.school_loan_payment),
                    new ParamType( typeof(decimal), college.salary) }) >= 0;
			/*}*/
		}

        public College Get(DBConnection conn, int userId)
        {
            College result = null;
            string query;
            List<Dictionary<string, object>> rs = null;
            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"SELECT *
FROM college
WHERE user_id = ?
ORDER BY college_id DESC
LIMIT 1";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), userId));

                    break;
                case "postgresql":
                    query =
@"SELECT *
FROM college
WHERE user_id = :1
ORDER BY college_id DESC
LIMIT 1";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), userId));

                    break;
                case "sqlserver":
                    query =
@"SELECT *
FROM college
WHERE user_id = @1
ORDER BY college_id DESC
LIMIT 1";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), userId));

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }

            foreach (Dictionary<string, object> row in rs)
            {
                object uId;
                object collegeName;
                object schoolDegree;
                object programName;
                object schoolDebt;
                object schoolLoanPayment;
                object salary;
                
                row.TryGetValue("user_id", out uId);
                row.TryGetValue("college_name", out collegeName);
                row.TryGetValue("school_degree", out schoolDegree);
                row.TryGetValue("program_name", out programName);
                row.TryGetValue("school_debt", out schoolDebt);
                row.TryGetValue("school_loan_payment", out schoolLoanPayment);
                row.TryGetValue("salary", out salary);

                result = new College((int)uId, (string)collegeName, (string)schoolDegree, (string)programName, (decimal)schoolDebt, (decimal)schoolLoanPayment, (decimal)salary);
                break;
            }

            return result;
        }
    }
}
