﻿//
//  HomesRepository.cs
//
//  Author:
//       Joe Merkel <merkeljoe9@gmail.com>
//
//  Copyright (c) 2018 Joe Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using GameOfLife.Entities;
using T4R.Data;

namespace GameOfLife.Models
{
    public class HomesRepository
    {
        public List<Home> Get(DBConnection conn, int studentId)
        {
            List<Home> result = null;

            string query;
            List<Dictionary<string, object>> rs = null;
            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"SELECT h.*, l.monthly_amount
FROM homes h
LEFT JOIN loans l ON l.category = CONCAT('Mortage ', h.home_id) OR l.category = CONCAT('Rental ', h.home_id)
WHERE h.user_id = ? AND h.removed = 0";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId));

                    break;
                case "postgresql":
                    query =
@"SELECT h.*, l.monthly_amount
FROM homes h
LEFT JOIN loans l ON l.category = CONCAT('Mortage ', h.home_id) OR l.category = CONCAT('Rental ', h.home_id)
WHERE h.user_id = :1 AND h.removed = 0";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId));

                    break;
                case "sqlserver":
                    query =
@"SELECT h.*, l.monthly_amount
FROM homes h
LEFT JOIN loans l ON l.category = CONCAT('Mortage ', h.home_id) OR l.category = CONCAT('Rental ', h.home_id)
WHERE h.user_id = @1 AND h.removed = 0";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId));

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }

            foreach (Dictionary<string, object> row in rs)
            {
                object homeId;
                object userId;
                object address;
                object is_rental;
                object price;
                object loanDownpayment;
                object loanAmount;
                object loanDuration;
                object monthlyAmount;

                row.TryGetValue("home_id", out homeId);
                row.TryGetValue("user_id", out userId);
                row.TryGetValue("address", out address);
                row.TryGetValue("is_rental", out is_rental);
                row.TryGetValue("price", out price);
                row.TryGetValue("loan_downpayment", out loanDownpayment);
                row.TryGetValue("loan_amount", out loanAmount);
                row.TryGetValue("loan_duration", out loanDuration);
                row.TryGetValue("monthly_amount", out monthlyAmount);

                if (monthlyAmount != null && !(monthlyAmount.GetType().IsInstanceOfType(DBNull.Value)))
                {
                    monthlyAmount = Convert.ToInt32(monthlyAmount);
                }

                if (result == null)
                {
                    result = new List<Home>();
                }

                Home home = new Home((int)homeId, (int)userId, (string)address, Convert.ToBoolean(is_rental), (int)price, loanDownpayment as int?, loanAmount as int?, loanDuration as int?, monthlyAmount as int?);
                result.Add(home);
            }

            return result;
        }

        public bool Buy(DBConnection conn, int studentId, string address, int price)
        {
            bool result = false;

            string query;
            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"INSERT INTO homes (user_id, address, is_rental, price, loan_downpayment, loan_amount, loan_duration)
VALUES (?, ?, 0, ?, null, null, null);";
                    result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId), new ParamType(typeof(string), address), new ParamType(typeof(int), price)) > 0);

                    break;
                case "postgresql":
                    query =
@"INSERT INTO homes (user_id, address, is_rental, price, loan_downpayment, loan_amount, loan_duration)
VALUES (:1, :2, 0, :3, null, null, null);";
                    result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId), new ParamType(typeof(string), address), new ParamType(typeof(int), price)) > 0);

                    break;
                case "sqlserver":
                    query =
@"INSERT INTO homes (user_id, address, is_rental, price, loan_downpayment, loan_amount, loan_duration)
VALUES (@1, @2, 0, @3, null, null, null);";
                    result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId), new ParamType(typeof(string), address), new ParamType(typeof(int), price)) > 0);

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }

            return result;
        }

        public bool Loan(DBConnection conn, int studentId, string address, int price, int loan_downpayment, int loan_amount, int loan_duration)
        {
            bool result = false;
            List<Dictionary<string, object>> rs = null;

            string query;
            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"INSERT INTO homes (user_id, address, is_rental, price, loan_downpayment, loan_amount, loan_duration)
VALUES (?, ?, 0, ?, ?, ?, ?);
SELECT LAST_INSERT_ID() AS home_id;";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId), new ParamType(typeof(string), address), new ParamType(typeof(int), price), new ParamType(typeof(int), loan_downpayment), new ParamType(typeof(int), loan_amount), new ParamType(typeof(int), loan_duration));

                    break;
                case "postgresql":
                    query =
@"INSERT INTO homes (user_id, address, is_rental, price, loan_downpayment, loan_amount, loan_duration)
VALUES (:1, :2, 0, :3, :4, :5, :6)
RETURNING home_id;";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId), new ParamType(typeof(string), address), new ParamType(typeof(int), price), new ParamType(typeof(int), loan_downpayment), new ParamType(typeof(int), loan_amount), new ParamType(typeof(int), loan_duration));

                    break;
                case "sqlserver":
                    query =
@"INSERT INTO homes (user_id, address, is_rental, price, loan_downpayment, loan_amount, loan_duration)
VALUES (@1, @2, 0, @3, @4, @5, @6);
OUTPUT INSERTED.ID AS home_id;";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId), new ParamType(typeof(string), address), new ParamType(typeof(int), price), new ParamType(typeof(int), loan_downpayment), new ParamType(typeof(int), loan_amount), new ParamType(typeof(int), loan_duration));

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }

            int homeId = -1;
            result = false;
            foreach (Dictionary<string, object> row in rs)
            {
                object home_id;

                row.TryGetValue("home_id", out home_id);
                homeId = Convert.ToInt32(home_id);

                result = true;
            }

            if (result)
            {
                LoanRepository loanRepository = new LoanRepository();
                result = loanRepository.AddHomeLoan(conn, studentId, homeId, loan_downpayment, loan_amount, loan_duration);
            }

            return result;
        }

        public bool Rent(DBConnection conn, int studentId, string address, int price)
        {
            bool result = false;
            List<Dictionary<string, object>> rs = null;

            string query;
            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"INSERT INTO homes (user_id, address, is_rental, price, loan_downpayment, loan_amount, loan_duration)
VALUES (?, ?, 1, ?, null, null, null);
SELECT LAST_INSERT_ID() AS home_id;";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId), new ParamType(typeof(string), address), new ParamType(typeof(int), price));

                    break;
                case "postgresql":
                    query =
@"INSERT INTO homes (user_id, address, is_rental, price, loan_downpayment, loan_amount, loan_duration)
VALUES (:1, :2, 1, :3, null, null, null)
RETURNING home_id;";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId), new ParamType(typeof(string), address), new ParamType(typeof(int), price));

                    break;
                case "sqlserver":
                    query =
@"INSERT INTO homes (user_id, address, is_rental, price, loan_downpayment, loan_amount, loan_duration)
VALUES (@1, @2, 1, @3, null, null, null);
OUTPUT INSERTED.ID AS home_id;";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId), new ParamType(typeof(string), address), new ParamType(typeof(int), price));

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }

            int homeId = -1;
            result = false;
            foreach (Dictionary<string, object> row in rs)
            {
                object home_id;

                row.TryGetValue("home_id", out home_id);
                homeId = Convert.ToInt32(home_id);

                result = true;
            }

            if (result)
            {
                LoanRepository loanRepository = new LoanRepository();
                result = loanRepository.AddHomeRental(conn, studentId, homeId, price);
            }

            return result;
        }
    }
}
