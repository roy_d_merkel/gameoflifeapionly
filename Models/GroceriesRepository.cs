﻿//
//  ExpenseRepository.cs
//
//  Author:
//       Joe Merkel <merkeljoe9@gmail.com>
//
//  Copyright (c) 2018 Joe Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using GameOfLife.Entities;
using T4R.Data;

namespace GameOfLife.Models
{
    public class GroceriesRepository
    {
        public Groceries Get(DBConnection conn, int studentId)
        {
            Groceries result = null;
            int cashOnHand = -1;
            List<Grocery> groceries = null;
            List<Dictionary<string, object>> rs = null;

            string query;

            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"SELECT i.*
FROM  grocery i 
WHERE i.user_id = ?;";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId), new ParamType(typeof(int), studentId));

                    break;
                case "postgresql":
                    query =
@"SELECT i.*
FROM  grocery i 
WHERE i.user_id = :1;";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId));

                    break;
                case "sqlserver":
                    query =
@"SELECT i.*
FROM  grocery i 
WHERE i.user_id = @1;";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId));

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }

            foreach (Dictionary<string, object> row in rs)
            {
                object id;
                object student_id;
                object amount;
                object name;
                object price;

                row.TryGetValue("grocery_id", out id);
                row.TryGetValue("user_id", out student_id);
                row.TryGetValue("amount", out amount);
                row.TryGetValue("name", out name);
                row.TryGetValue("price", out price);

                if (groceries == null)
                {
                    groceries = new List<Grocery>();
                }

                if (id != null)
                {
                    Grocery grocery = new Grocery((int)id, (int)student_id, (int)amount, (string)name, (int)price);
                    groceries.Add(grocery);
                }
            }

            BankRepository bank = new BankRepository();
            cashOnHand = (bank.Get(conn, studentId) ?? new Bank(studentId, 0, 0, 0)).cash_on_hand;

            if (groceries != null)
            {
                result = new Groceries(studentId, cashOnHand, groceries.ToArray());
            }
            else
            {
                result = new Groceries(studentId, cashOnHand, new Grocery[0]);
            }

            return result;
        }

        public bool Add(DBConnection conn, int studentId, Grocery grocery)
        {
            bool result = false;

            string query;

            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"INSERT INTO grocery (user_id, amount, name, price)
VALUES (?, ?, ?, ?);";
                    result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId), new ParamType(typeof(int), grocery.amount), new ParamType(typeof(string), grocery.name), new ParamType(typeof(int), grocery.price)) > 0);

                    break;
                case "postgresql":
                    query =
@"INSERT INTO grocery (user_id, amount, name, price)
VALUES (:1, :2, :3, :4);";
                    result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId), new ParamType(typeof(int), grocery.amount), new ParamType(typeof(string), grocery.name), new ParamType(typeof(int), grocery.price)) > 0);

                    break;
                case "sqlserver":
                    query =
@"INSERT INTO grocery (user_id, amount, name, price)
VALUES (@1, @2, @3, @4);";
                    result = (conn.ExecuteUpdate(query, new ParamType(typeof(int), studentId), new ParamType(typeof(int), grocery.amount), new ParamType(typeof(string), grocery.name), new ParamType(typeof(int), grocery.price)) > 0);

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }

            return result;
        }







    }
}
