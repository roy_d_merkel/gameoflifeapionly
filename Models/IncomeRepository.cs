﻿//
//  IncomeRepository.cs
//
//  Author:
//       Joe Merkel <merkeljoe9@gmail.com>
//
//  Copyright (c) 2018 Joe Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using GameOfLife.Entities;
using T4R.Data;

namespace GameOfLife.Models
{
	public class IncomeRepository
    {
		private JobsRepository jobsRepository;

		public IncomeRepository()
		{
			jobsRepository = new JobsRepository();
		}

		public Income Get(DBConnection conn, int userId)
        {
			Job[] jobs = jobsRepository.Get(conn, userId, false);
			int monthlyIncome = 0;
			int salary = 0;

			foreach (Job j in jobs)
			{
				monthlyIncome += j.monthly_take_home;
				salary += j.salary;
			}

			Income results = new Income(userId, monthlyIncome, salary, jobs);

			return results;
        }
    }
}
