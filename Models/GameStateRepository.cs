﻿//
//  GameStateRepository.cs
//
//  Author:
//       Roy Merkel <merkel-roy@comcast.net>
//
//  Copyright (c) 2018 Roy Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using GameOfLife.Entities;
using T4R.Data;

namespace GameOfLife.Models
{
	public class GameStateRepository
    {
		public GameStateRepository()
        {
        }
        
		public TimeSpan? GetNextTick(DBConnection conn, int userId, out bool launched, out DateTime? now, out DateTime? nextTickTime)
        {
			TimeSpan? result = null;
            string query;
            List<Dictionary<string, object>> rs = null;
            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"SELECT NOW() AS now, DATE_ADD(DATE_ADD(DATE_ADD(gs.last_game_time_event, INTERVAL hour(cc.month_interval) HOUR), INTERVAL minute(cc.month_interval) MINUTE), INTERVAL second(cc.month_interval) SECOND) AS next_tick, gs.launched
FROM user_class uc
INNER JOIN game_state gs ON uc.class_id = gs.class_id
INNER JOIN class_configuration cc ON uc.class_id = cc.class_id
WHERE uc.user_id = ?";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), userId));

                    break;
                case "postgresql":
                    query =
@"SELECT NOW() AS now, (gs.last_game_time_event + cc.month_interval) AS next_tick, gs.launched
FROM user_class uc
INNER JOIN game_state gs ON uc.class_id = gs.class_id
INNER JOIN class_configuration cc ON uc.class_id = cc.class_id
WHERE uc.user_id = :1";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), userId));

                    break;
                case "sqlserver":
                    query =
@"SELECT GETDATE() AS now, DATEADD(SECOND, DATEPART(SECOND, cc.month_interval), DATEADD(MINUTE, DATEPART(MINUTE, cc.month_interval), DATEADD(HOUR, DATEPART(HOUR, cc.month_interval), gs.last_game_time_event))) AS next_tick, gs.launched
FROM user_class uc
INNER JOIN game_state gs ON uc.class_id = gs.class_id
INNER JOIN class_configuration cc ON uc.class_id = cc.class_id
WHERE uc.user_id = @1";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), userId));

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }

			launched = false;
			now = null;
			nextTickTime = null;

            foreach (Dictionary<string, object> row in rs)
            {
				object noW;
				object next_tick;
				object launcheD;

				row.TryGetValue("now", out noW);
				row.TryGetValue("next_tick", out next_tick);
				row.TryGetValue("launched", out launcheD);

				launched = Convert.ToBoolean(launcheD);
				now = noW as DateTime?;

				if (!launched)
				{
					result = null;
					nextTickTime = null;
				}
				else
				{               
					nextTickTime = next_tick as DateTime?;

					if (nextTickTime != null && now != null)
					{
						result = (nextTickTime - now);
					}
					else
					{
						result = null;
					}
				}

                break;
            }

            return result;
        }

		public GameState Get(DBConnection conn)
        {
			GameState result = null;
			string query;
            List<Dictionary<string, object>> rs = null;
            query =
@"SELECT g.*
FROM game_state g";
            rs = conn.ExecuteQuery(query);

            foreach (Dictionary<string, object> row in rs)
            {
                object game_state_id;
                object class_id;
                object current_game_month_number;
                object current_game_time;
                object last_game_time_event;

				row.TryGetValue("game_state_id", out game_state_id);
				row.TryGetValue("class_id", out class_id);
				row.TryGetValue("current_game_month_number", out current_game_month_number);
				row.TryGetValue("current_game_time", out current_game_time);
				row.TryGetValue("last_game_time_event", out last_game_time_event);

				Type type = current_game_time.GetType();
				type = last_game_time_event.GetType();
				result = new GameState(Convert.ToInt32(game_state_id), 
				                       Convert.ToInt32(class_id), 
				                       Convert.ToInt32(current_game_month_number), 
				                       current_game_time as DateTime?, 
				                       last_game_time_event as DateTime?);
                break;
            }

            return result;
        }

		public bool StartSession(DBConnection conn)
        {
            string query;
			int result = -1;
			switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"UPDATE game_state
SET launched = 1, last_game_time_event = NOW()";
					result = conn.ExecuteUpdate(query);

                    break;
                case "postgresql":
                    query =
@"UPDATE game_state
SET launched = true, last_game_time_event = NOW()";
					result = conn.ExecuteUpdate(query);

                    break;
                case "sqlserver":
                    query =
@"UPDATE game_state
SET launched = 1, last_game_time_event = GETDATE()";
					result = conn.ExecuteUpdate(query);

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }

            return result >= 0;
        }

		public bool StopSession(DBConnection conn)
        {
            string query;
            int result = -1;
            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"UPDATE game_state
SET launched = 0, stopping = 1, last_game_time_event = NOW()";
                    result = conn.ExecuteUpdate(query);

                    break;
                case "postgresql":
                    query =
@"UPDATE game_state
SET launched = false, stopping = true, last_game_time_event = NOW()";
                    result = conn.ExecuteUpdate(query);

                    break;
                case "sqlserver":
                    query =
@"UPDATE game_state
SET launched = 0, stopping = 1, last_game_time_event = GETDATE()";
                    result = conn.ExecuteUpdate(query);

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }

            return result >= 0;
        }
    }
}
