﻿//
//  StudentsRepository.cs
//
//  Author:
//       Joe Merkel <merkeljoe9@gmail.com>
//
//  Copyright (c) 2018 Joe Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using GameOfLife.Entities;
using T4R.Data;

namespace GameOfLife.Models
{
    public class StudentsRepository
    {
        private JobsRepository jobsRepository;
        private ItemsRepository itemsRepository;

        public StudentsRepository()
        {
            jobsRepository = new JobsRepository();
            itemsRepository = new ItemsRepository();
        }

        public List<Student> Get(DBConnection conn, bool includeArchived = false)
        {
            List<Student> result = new List<Student>();
            List<Dictionary<string, object>> rs = null;

            string query;

            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"SELECT *
FROM users e
WHERE user_type = 'Student' AND NOT removed AND (? OR NOT archived);";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(bool), includeArchived));

                    break;
                case "postgresql":
                    query =
@"SELECT *
FROM users e
WHERE user_type = 'Student' AND NOT removed AND (:1 OR NOT archived);";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(bool), includeArchived));

                    break;
                case "sqlserver":
                    query =
@"SELECT *
FROM users e
WHERE user_type = 'Student' AND NOT removed AND (@1 OR NOT archived);";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(bool), includeArchived));

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }

            foreach (Dictionary<string, object> row in rs)
            {
                object student_id;
                object username;
                object password;
                object gender;
                object grade;
                object school_year;

                row.TryGetValue("user_id", out student_id);
                row.TryGetValue("username", out username);
                row.TryGetValue("plaintextpassword", out password);
                row.TryGetValue("gender", out gender);
                row.TryGetValue("grade_school", out grade);
                row.TryGetValue("school_year", out school_year);

                Job[] jobs = jobsRepository.Get(conn, (int)student_id, true);
                Items items = itemsRepository.Get(conn, (int)student_id);
                Student s = new Student((int)student_id,(string)username, (string)password, (string)gender, (string)grade, (string)school_year, jobs, items);
                result.Add(s);
            }

            return result;
        }
    }
}
