﻿//
//  UserRepository.cs
//
//  Author:
//       Roy Merkel <merkel-roy@comcast.net>
//
//  Copyright (c) 2018 Roy Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using GameOfLife.Entities;
using T4R.Data;

namespace GameOfLife.Models
{
    public class UserRepository
    {
        public UserRepository()
        {
        }

		public bool userIdExists(DBConnection conn, string user_name, out int userId)
        {
            bool exists = false;
         
            userId = -1;

            string query =
@"SELECT MAX(se.user_id) AS user_id
FROM (
    SELECT -1 AS user_id
    UNION
    SELECT u.user_id
    FROM users u
    WHERE u.username = ?

) AS se;";
            List<Dictionary<string, object>> rs = conn.ExecuteQuery(query, new ParamType(typeof(string), user_name));

            foreach (Dictionary<string, object> row in rs)
            {
                object user_id;

                row.TryGetValue("user_id", out user_id);

                if (Convert.ToInt32(user_id) > 0)
                {
                    userId = Convert.ToInt32(user_id);

                    exists = true;

                    break;
                }
            }
                     
            return exists;
        }

		public bool userIdPasswordExists(DBConnection conn, int userId, string password)
        {
            bool exists = false;
         
            string query =
@"SELECT MAX(se.user_id) AS user_id
FROM (
    SELECT -1 AS user_id
    UNION
    SELECT u.user_id
    FROM users u
    WHERE u.user_id = ? AND u.password = ?
) AS se;";
            List<Dictionary<string, object>> rs = conn.ExecuteQuery(query, new ParamType(typeof(int), userId), new ParamType(typeof(string), password));

            foreach (Dictionary<string, object> row in rs)
            {
                object user_id;

                row.TryGetValue("user_id", out user_id);

                if (Convert.ToInt32(user_id) > 0)
                {
                    exists = true;

                    break;
                }
            }
                     
            return exists;
        }

		public User GetUser(DBConnection conn, int userId)
        {
            string query =
@"SELECT u.*
FROM users u
WHERE u.user_id = ?;";
                
            List<Dictionary<string, object>> rs = conn.ExecuteQuery(query, new ParamType(typeof(int), userId));
            User u = null;

            foreach (Dictionary<string, object> row in rs)
            {
                object user_id;
                object user_type;
                object username;
                object password;
                object plaintextpassword;
                object gender;
                object grade_school;
                object school;
                object removed;
                object removed_date;
                object enrolled_date;

                row.TryGetValue("user_id", out user_id);
                row.TryGetValue("user_type", out user_type);
                row.TryGetValue("username", out username);
                row.TryGetValue("password", out password);
                row.TryGetValue("plaintextpassword", out plaintextpassword);
                row.TryGetValue("gender", out gender);
                row.TryGetValue("grade_school", out grade_school);
                row.TryGetValue("school", out school);
                row.TryGetValue("removed", out removed);
                row.TryGetValue("removed_date", out removed_date);
                row.TryGetValue("enrolled_date", out enrolled_date);

                u = new User(Convert.ToInt32(user_id), Convert.ToString(user_type), Convert.ToString(username), Convert.ToString(password), Convert.ToString(plaintextpassword), Convert.ToString(gender), Convert.ToString(grade_school), Convert.ToString(school), Convert.ToBoolean(removed), (DateTime?)removed_date, (DateTime?)enrolled_date);
            }
                     
            return u;
        }

		public IAuthUser Get(DBConnection conn, string user_name, string password)
        {
            int userId = -1;

            if (!userIdExists(conn, user_name, out userId))
            {
                return null;
            }
            else if (!userIdPasswordExists(conn, userId, password))
            {
                return null;
            }
            else if (userId > 0)
            {
                return GetUser(conn, userId);
            }
            else
            {
                return null;
            }
        }
    }
}
