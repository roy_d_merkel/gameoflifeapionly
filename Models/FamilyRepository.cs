﻿//
//  FamilyRepository.cs
//
//  Author:
//       Roy Merkel <merkel-roy@comcast.net>
//
//  Copyright (c) 2018 Roy Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using GameOfLife.Entities;
using T4R.Data;

namespace GameOfLife.Models
{
	public class FamilyRepository
    {
		public FamilyRepository()
        {
        }

		public Family Get(DBConnection conn, int userId)
        {
            Family result = null;
            string query;
            List<Dictionary<string, object>> rs = null;
            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"SELECT f.*
FROM family f
INNER JOIN (
    SELECT f.user_id, MAX(f.recorded_on) AS recorded_on
    FROM family f
    GROUP BY f.user_id
) fi ON fi.user_id = f.user_id AND ((fi.recorded_on IS NULL AND f.recorded_on IS NULL) OR fi.recorded_on = f.recorded_on)
WHERE f.user_id = ?";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), userId));

                    break;
                case "postgresql":
                    query =
@"SELECT f.*
FROM family f
INNER JOIN (
    SELECT f.user_id, MAX(f.recorded_on) AS recorded_on
    FROM family f
    GROUP BY f.user_id
) fi ON fi.user_id = f.user_id AND ((fi.recorded_on IS NULL AND f.recorded_on IS NULL) OR fi.recorded_on = f.recorded_on)
WHERE f.user_id = :1";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), userId));

                    break;
                case "sqlserver":
                    query =
@"SELECT f.*
FROM family f
INNER JOIN (
    SELECT f.user_id, MAX(f.recorded_on) AS recorded_on
    FROM family f
    GROUP BY f.user_id
) fi ON fi.user_id = f.user_id AND ((fi.recorded_on IS NULL AND f.recorded_on IS NULL) OR fi.recorded_on = f.recorded_on)
WHERE f.user_id = @1";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), userId));

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }
            
            foreach (Dictionary<string, object> row in rs)
            {
				object family_id;
				object user_id;
				object actual_number_children;
				object child_support_costs;
				object wants_to_start_family;
				object wants_children;
				object children_applied_for;
				object recorded_on;

				row.TryGetValue("family_id", out family_id);
				row.TryGetValue("user_id", out user_id);
				row.TryGetValue("actual_number_children", out actual_number_children);
				row.TryGetValue("child_support_costs", out child_support_costs);
				row.TryGetValue("wants_to_start_family", out wants_to_start_family);
				row.TryGetValue("wants_children", out wants_children);
				row.TryGetValue("children_applied_for", out children_applied_for);
				row.TryGetValue("recorded_on", out recorded_on);

				result = new Family(Convert.ToInt32(family_id), Convert.ToInt32(user_id), Convert.ToInt32(actual_number_children), Convert.ToInt32(child_support_costs), Convert.ToString(wants_to_start_family), Convert.ToString(wants_children), Convert.ToInt32(children_applied_for), (DateTime?)recorded_on);
				break;
            }
            
            return result;
        }

		public int GetChildSupportCosts(DBConnection conn, int userId)
        {
            int result = 0;
            string query;
            List<Dictionary<string, object>> rs = null;
            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"SELECT cc.child_support_per_kid
FROM user_class uc
INNER JOIN class_configuration cc ON uc.class_id = cc.class_id
WHERE uc.user_id = ?";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), userId));

                    break;
                case "postgresql":
                    query =
@"SELECT cc.child_support_per_kid
FROM user_class uc
INNER JOIN class_configuration cc ON uc.class_id = cc.class_id
WHERE uc.user_id = :1";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), userId));

                    break;
                case "sqlserver":
                    query =
@"SELECT cc.child_support_per_kid
FROM user_class uc
INNER JOIN class_configuration cc ON uc.class_id = cc.class_id
WHERE uc.user_id = @1";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), userId));

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }

            foreach (Dictionary<string, object> row in rs)
            {
				object child_support_per_kid;

				row.TryGetValue("child_support_per_kid", out child_support_per_kid);

				result = Convert.ToInt32(child_support_per_kid);
                break;
            }

            return result;
        }

		public bool Post(DBConnection conn, int userId, string wantFamily, string wantKids, int childrenAppliedFor, out int actualNumberChildren, out int childSupportCosts)
        {         
			Family curVal = this.Get(conn, userId);
            string query;

			int numRows = -1;
			int familyId = -1;
            actualNumberChildren = 0;
         
			if (curVal == null)
			{
				Console.Error.WriteLine("NULL Family!");
				Console.Error.WriteLine("WantFamily: ");
				Console.Error.WriteLine(wantFamily);
				Console.Error.WriteLine("wantKids: ");
				Console.Error.WriteLine(wantKids);
				if (wantFamily == "Yes" && wantKids == "Yes")
                {
                    actualNumberChildren = new Random().Next(0, 2);
                }
			}
			else
			{
				familyId = curVal.family_id;
				actualNumberChildren = curVal.actual_number_children;

				if (curVal.wants_to_start_family == "Yes")
				{
					wantFamily = "Yes";
				}

				if (curVal.wants_children == "Yes")
                {
					wantKids = "Yes";
                }
                
                Console.Error.WriteLine("WantFamily: ");
                Console.Error.WriteLine(wantFamily);
                Console.Error.WriteLine("wantKids: ");
                Console.Error.WriteLine(wantKids);

				if(wantFamily == "Yes" && wantKids == "Yes")
                {
					actualNumberChildren += new Random().Next(0, 2);
                }
			}
			childSupportCosts = actualNumberChildren * GetChildSupportCosts(conn, userId);

			switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"INSERT INTO family(user_id, actual_number_children, child_support_costs, wants_to_start_family, wants_children, children_applied_for)
VALUES
(?, ?, ?, ?, ?, ?)";
                    numRows = conn.ExecuteUpdate(query, new ParamType(typeof(int), userId),
					                             new ParamType(typeof(int), actualNumberChildren),
					                             new ParamType(typeof(int), childSupportCosts),
					                             new ParamType(typeof(string), wantFamily),
					                             new ParamType(typeof(string), wantKids),
					                             new ParamType(typeof(int), childrenAppliedFor));
                    break;
                case "postgresql":
                    query =
@"INSERT INTO family(user_id, actual_number_children, child_support_costs, wants_to_start_family, wants_children, children_applied_for)
VALUES
(:1, :2, :3, :4, :5, :6)";
                    numRows = conn.ExecuteUpdate(query, new ParamType(typeof(int), userId),
					                             new ParamType(typeof(int), actualNumberChildren),
					                             new ParamType(typeof(int), childSupportCosts),
					                             new ParamType(typeof(string), wantFamily),
					                             new ParamType(typeof(string), wantKids),
					                             new ParamType(typeof(int), childrenAppliedFor));
                    break;
                case "sqlserver":
                    query =
@"INSERT INTO family(user_id, actual_number_children, child_support_costs, wants_to_start_family, wants_children, children_applied_for)
VALUES
(:1, :2, :3, :4, :5, :6)";
                    numRows = conn.ExecuteUpdate(query, new ParamType(typeof(int), userId),
					                             new ParamType(typeof(int), actualNumberChildren),
					                             new ParamType(typeof(int), childSupportCosts),
					                             new ParamType(typeof(string), wantFamily),
					                             new ParamType(typeof(string), wantKids),
					                             new ParamType(typeof(int), childrenAppliedFor));
                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }

			return numRows >= 0;
        }
    }
}
