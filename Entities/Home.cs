﻿//
//  Home.cs
//
//  Author:
//       Joe Merkel <merkeljoe9@gmail.com>
//
//  Copyright (c) 2018 Joe Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
namespace GameOfLife.Entities
{
    public class Home
    {
        private int _homeId;
        private int _userId;
        private string _address;
        private bool _isRental;
        private int _price;
        private int? _loanDownpayment;
        private int? _loanAmount;
        private int? _loanDuration;
        private int? _monthlyAmount;

        public Home(int homeId, int userId, string address, bool isRental, int price, int? loanDownpayment, int? loanAmount, int? loanDuration, int? monthlyAmount)
        {
            _homeId = homeId;
            _userId = userId;
            _address = address;
            _isRental = isRental;
            _price = price;
            _loanDownpayment = loanDownpayment;
            _loanAmount = loanAmount;
            _loanDuration = loanDuration;
            _monthlyAmount = monthlyAmount;
        }

        public int home_id { get { return _homeId; } }
        public int user_id { get { return _userId; } }
        public string address { get { return _address; } }
        public bool is_rental { get { return _isRental; } }
        public int price { get { return _price; } }
        public int? loan_downpayment { get { return _loanDownpayment; } }
        public int? loan_amount { get { return _loanAmount; } }
        public int? loan_duration { get { return _loanDuration; } }
        public int? monthly_amount { get { return _monthlyAmount; } }
    }
}
