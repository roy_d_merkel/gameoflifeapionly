﻿//
//  Groceries.cs
//
//  Author:
//       Joe Merkel <merkeljoe9@gmail.com>
//
//  Copyright (c) 2018 Joe Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
namespace GameOfLife.Entities
{
    public class Groceries
    {
        private int _userId;
        private int _cashOnHand;
        private Grocery[] _items;

        public Groceries(int userId, int cashOnHand, Grocery[] items)
        {
            _userId = userId;
            _cashOnHand = cashOnHand;
            _items = items;
        }

        public int user_id { get { return _userId; } }
        public int cash_on_hand { get { return _cashOnHand; } }
        public Grocery[] items { get { return _items; } }
    }
}
