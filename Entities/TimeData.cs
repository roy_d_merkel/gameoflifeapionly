﻿//
//  TimeData.cs
//
//  Author:
//       Joe Merkel <merkeljoe9@gmail.com>
//
//  Copyright (c) 2018 Joe Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
namespace GameOfLife.Entities
{
    public class TimeData
    {
        private bool _launched;
        private string _now;
        private string _nextTickTime;
        private TimeSpan? _timeToNextTick;

        public TimeData(bool launched, string now, string nextTickTime, TimeSpan? timeToNextTick)
        {
            _launched = launched;
            _now = now;
            _nextTickTime = nextTickTime;
            _timeToNextTick = timeToNextTick;
        }

        public bool launched { get { return _launched; } }
        public string server_now { get { return _now; } }
        public string server_next_tick_time { get { return _nextTickTime; } }
        public TimeSpan? time_to_next_tick { get { return _timeToNextTick; } }
    }
}
