﻿//
//  GameState.cs
//
//  Author:
//       Roy Merkel <merkel-roy@comcast.net>
//
//  Copyright (c) 2018 Roy Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
namespace GameOfLife.Entities
{
    public class GameState
    {
        private int _gameStateId;
        private int _classId;
        private int _currentGameMonthNumber;
        private DateTime? _currentGameTime;
        private DateTime? _lastGameTimeEvent;

        public GameState(int gameStateId, int classId, int currentGameMonthNumber, DateTime? currentGameTime, DateTime? lastGameTimeEvent)
        {
            _gameStateId = gameStateId;
            _classId = classId;
            _currentGameMonthNumber = currentGameMonthNumber;
            _currentGameTime = currentGameTime;
            _lastGameTimeEvent = lastGameTimeEvent;
        }

        public int game_state_id { get { return _gameStateId; } }
        public int class_id { get { return _classId; } }
        public int current_game_month_number { get { return _currentGameMonthNumber; } }
        public DateTime? current_game_time { get { return _currentGameTime; } }
        public DateTime? last_game_time_event { get { return _lastGameTimeEvent; } }
    }
}
