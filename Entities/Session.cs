﻿//
//  Session.cs
//
//  Author:
//       Roy Merkel <merkel-roy@comcast.net>
//
//  Copyright (c) 2018 Roy Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
namespace GameOfLife.Entities
{
    public class Session
    {
        private int _sessionId;
        private int _userId;
        private string _ipAddress;
        private string _browserInfo;
        private DateTime? _startTime;
        private DateTime? _endTime;
        private DateTime? _heartBeat;
        private string _phpSessionId;

        public Session(int sessionId, int userId, string ipAddress, string browserInfo, DateTime? startTime, DateTime? endTime, DateTime? heartBeat, string phpSessionId)
        {
            _sessionId = sessionId;
            _userId = userId;
            _ipAddress = ipAddress;
            _browserInfo = browserInfo;
            _startTime = startTime;
            _endTime = endTime;
            _heartBeat = heartBeat;
            _phpSessionId = phpSessionId;
        }

        public int session_id { get { return _sessionId; } }
        public int user_id { get { return _userId; } }
        public string ip_address { get { return _ipAddress; } }
        public string browser_info { get { return _browserInfo; } }
        public DateTime? start_time { get { return _startTime; } }
        public DateTime? end_time { get { return _endTime; } }
        public DateTime? heart_beat { get { return _heartBeat; } }
        public string php_session_id { get { return _phpSessionId; } }
    }
}
