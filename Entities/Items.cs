﻿//
//  Items.cs
//
//  Author:
//       Roy Merkel <merkel-roy@comcast.net>
//
//  Copyright (c) 2018 Roy Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
namespace GameOfLife.Entities
{
	public class Items
    {
		private int _userId;      
		private int _cashOnHand;
		private Item[] _items;
              
		public Items(int userId, int cashOnHand, Item[] items)
        {
			_userId = userId;
			_cashOnHand = cashOnHand;
			_items = items;
        }

		public int user_id { get { return _userId; } }
		public int cash_on_hand { get { return _cashOnHand; } }
		public Item[] items { get { return _items; } }
    }
}
