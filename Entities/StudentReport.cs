﻿//
//  StudentReport.cs
//
//  Author:
//       Joe Merkel <merkeljoe9@gmail.com>
//
//  Copyright (c) 2018 Joe Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Security.Principal;
using System.Collections.Generic;

namespace GameOfLife.Entities
{
    public class StudentReport
    {
        private int _userId;
        private int _month;
        private int _cashOnHand;
        private int _monthlyIncome;
        private int _monthlyExpense;
        private int _checking;
        private int _saving;
        private List<Expense> _expenses;

        public StudentReport(int userId, int month, int cashOnHand, int monthlyIncome, int monthlyExpense, int checking, int saving, List<Expense> expenses)
        {
            _userId = userId;
            _month = month;
            _cashOnHand = cashOnHand;
            _monthlyIncome = monthlyIncome;
            _monthlyExpense = monthlyExpense;
            _checking = checking;
            _saving = saving;
            _expenses = expenses;
        }

        public int user_id { get { return _userId; } }
        public int month { get { return _month; } }
        public int cash_on_hand { get { return _cashOnHand; } }
        public int monthly_income { get { return _monthlyIncome; } }
        public int monthly_expense { get { return _monthlyExpense; } }
        public int checking { get { return _checking; } }
        public int saving { get { return _saving; } }
        public List<Expense> expenses { get { return _expenses; } }
    }
}
