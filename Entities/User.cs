﻿//
//  Student.cs
//
//  Author:
//       Roy Merkel <merkel-roy@comcast.net>
//
//  Copyright (c) 2018 Roy Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Security.Principal;

namespace GameOfLife.Entities
{
    public class User : IAuthUser
    {
        int _userId;
        string _userType;
        string _username;
        string _password;
        string _plainTextPassword;
        string _gender;
        string _gradeSchool;
        string _school;
        bool _removed;
        DateTime? _removedDate;
        DateTime? _enrolledDate;


        public User(int userId, string userType, string username, string password, string plainTextPassword, string gender, string gradeSchool, string school, bool removed, DateTime? removedDate, DateTime? enrolledDate)
        {
            _userId = userId;
            _userType = userType;
            _username = username;
            _password = password;
            _plainTextPassword = plainTextPassword;
            _gender = gender;
            _gradeSchool = gradeSchool;
            _school = school;
            _removed = removed;
            _removedDate = removedDate;
            _enrolledDate = enrolledDate;
        }

        public int user_id { get { return _userId; } }
        public string user_type { get { return _userType; } }
        public string username { get { return _username; } }
        //public string password { get { return _password; } }
        //public string plaintextpassword { get { return _plainTextPassword; } }
        public string gender { get { return _gender; } }
        public string grade_school { get { return _gradeSchool; } }
        public string school { get { return _school; } }
        public bool removed { get { return _removed; } }
        public DateTime? removed_date { get { return _removedDate; } }
        public DateTime? enrolled_date { get { return _enrolledDate; } }
    }
}
