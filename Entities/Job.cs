﻿//
//  Job.cs
//
//  Author:
//       Roy Merkel <merkel-roy@comcast.net>
//
//  Copyright (c) 2018 Roy Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
namespace GameOfLife.Entities
{
	public class Job
    {
		private int _userId;
		private int _jobId;
		private string _title;
		private string _type;
		private int _salary;
		private int _monthlyTakeHome;
		private bool _fromCollege;
        private bool _approved;
		private DateTime? _recordedOn;
		private bool _removed;
        
		public Job(int jobId, int userId, bool fromCollege, string title, string type, int salary, int monthlyTakeHome, bool approved, DateTime? recordedOn, bool removed)
        {
			_jobId = jobId;
			_userId = userId;
			_fromCollege = fromCollege;
			_title = title;
			_type = type;
			_salary = salary;
			_monthlyTakeHome = monthlyTakeHome;
            _approved = approved;
			_recordedOn = recordedOn;
			_removed = removed;
        }
		public int job_id { get { return _jobId; } }
		public int user_id { get { return _userId; } }
		public bool from_college { get { return _fromCollege; } }
		public string title { get { return _title; } }
		public string type { get { return _type; } }
		public int salary { get { return _salary; } }
		public int monthly_take_home { get { return _monthlyTakeHome; } }
        public bool approved { get { return _approved; } }
		public DateTime? recorded_on { get { return _recordedOn; } }
		public bool removed { get { return _removed; } }
    }
}
