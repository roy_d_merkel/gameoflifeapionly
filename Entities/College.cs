﻿//
//  College.cs
//
//  Author:
//       Roy Merkel <merkel-roy@comcast.net>
//
//  Copyright (c) 2018 Roy Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
namespace GameOfLife.Entities
{
    public class College
    {
		private int _userId;
		private string _collegeName;
		private string _schoolDegree;
		private string _programName;
		private decimal _schoolDebt;
		private decimal _schoolLoanPayment;
		private decimal _salary;

		public College(int userId, string collegeName, string schoolDegree, string programName, decimal schoolDebt, decimal schoolLoanPayment, decimal salary)
        {
			_userId = userId;
			_collegeName = collegeName;
			_schoolDegree = schoolDegree;
			_programName = programName;
			_schoolDebt = schoolDebt;
			_schoolLoanPayment = schoolLoanPayment;
			_salary = salary;
        }

		public int user_id { get { return _userId; } }
		public string college_name { get { return _collegeName; } }
		public string school_degree { get { return _schoolDegree; } }
		public string program_name { get { return _programName; } }
		public decimal school_debt { get { return _schoolDebt; } }
		public decimal school_loan_payment { get { return _schoolLoanPayment; } }
		public decimal salary { get { return _salary; } }
    }
}
