﻿//
//  Grocery.cs
//
//  Author:
//       Joe Merkel <merkeljoe9@gmail.com>
//
//  Copyright (c) 2018 Joe Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
namespace GameOfLife.Entities
{
    public class Grocery
    {
        private int _id;
        private int _userId;
        private int _amount;
        private string _name;
        private int _price;

        public Grocery(int id, int userId, int amount, string name, int price)
        {
            _id = id;
            _userId = userId;
            _amount = amount;
            _name = name;
            _price = price;
        }

        public int id { get { return _id; } }
        public int user_id { get { return _userId; } }
        public int amount { get { return _amount; } }
        public string name { get { return _name; } }
        public int price { get { return _price; } }
    }
}
