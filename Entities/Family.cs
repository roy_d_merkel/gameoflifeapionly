﻿//
//  Family.cs
//
//  Author:
//       Roy Merkel <merkel-roy@comcast.net>
//
//  Copyright (c) 2018 Roy Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
namespace GameOfLife.Entities
{
	public class Family
    {
		private int _familyId;
		private int _userId;
		private int _actualNumberChildren;
		private int _childSupportCosts;
		private string _wantsToStartFamily;
		private string _wantsChildren;
		private int _childrenAppliedFor;
		private DateTime? _recordedOn;

		public Family(int familyId, int userId, int actualNumberChildren, int childSupportCosts, string wantsToStartFamily, string wantsChildren, int childrenAppliedFor, DateTime? recordedOn)
        {
			_familyId = familyId;
			_userId = userId;
			_actualNumberChildren = actualNumberChildren;
			_childSupportCosts = childSupportCosts;
			_wantsToStartFamily = wantsToStartFamily;
			_wantsChildren = wantsChildren;
			_childrenAppliedFor = childrenAppliedFor;
			_recordedOn = recordedOn;
        }

		public int family_id { get { return _familyId; } }
		public int user_id { get { return _userId; } }
		public int actual_number_children { get { return _actualNumberChildren; } }
		public int child_support_costs { get { return _childSupportCosts; } }
		public string wants_to_start_family { get { return _wantsToStartFamily; } }
		public string wants_children { get { return _wantsChildren; } }
		public int children_applied_for { get { return _childrenAppliedFor; } }
		public DateTime? recorded_on { get { return _recordedOn; } }
    }
}
