﻿//
//  Student.cs
//
//  Author:
//       Joe Merkel <merkeljoe9@gmail.com>
//
//  Copyright (c) 2018 Joe Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
namespace GameOfLife.Entities
{
    public class Student
    {
        private int _studentId;
        private string _username;
        private string _password;
        private string _gender;
        private string _grade;
        private string _schoolYear;
        private Job[] _jobs;
        private Items _items;

        public Student(int studentId, string username, string password, string gender, string grade, string schoolYear, Job[] jobs = null, Items items = null)
        {
            _studentId = studentId;
            _username = username;
            _password = password;
            _gender = gender;
            _grade = grade;
            _schoolYear = schoolYear;
            _jobs = jobs;
            _items = items;
        }

        public int student_id { get { return _studentId; } }
        public string username { get { return _username; } }
        public string password { get { return _password; } }
        public string gender { get { return _gender; } }
        public string grade { get { return _grade; } }
        public string school_year { get { return _schoolYear; } }
        public Job[] jobs { get { return _jobs; } }
        public Items items { get { return _items; } }
    }
}
