﻿//
//  StudentsController.cs
//
//  Author:
//       Jo Merkel <merkeljoe9@gmail.com>
//
//  Copyright (c) 2018 Joe Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.SessionState;
using GameOfLife.App_Start;
using GameOfLife.Entities;
using GameOfLife.Filters;
using GameOfLife.Models;
using T4R.Data;
using Newtonsoft.Json;

namespace GameOfLife.Controllers
{
    public class StudentsController : ApiController
    {
        StudentsRepository studentsRepository = null;
        UserRepository userRepository = null;

        public StudentsController()
        {
            this.studentsRepository = new StudentsRepository();
            this.userRepository = new UserRepository();
        }

        [TokenAuthFilter(true, true)]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            UserSessionIdentity identity = null;
            Session session = null;

            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity != null && Thread.CurrentPrincipal.Identity is UserSessionIdentity)
            {
                identity = (UserSessionIdentity)(Thread.CurrentPrincipal.Identity);

                if (identity.session != null)
                {
                    session = identity.session;
                }
            }

            DBConnection conn = new DBConnection("GameOfLife");
            int user_id = identity.user.user_id;

			User u = (User)((UserSessionIdentity)(Thread.CurrentPrincipal.Identity)).user;
            string userType = u.user_type;
            if(!(userType.Equals("Teacher") || userType.Equals("Admin")))
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, "Unauthorized access!");
            }

            List<Student> results = this.studentsRepository.Get(conn);
            conn.Close();

            return request.CreateResponse(HttpStatusCode.OK, results);
        }
    }
}
