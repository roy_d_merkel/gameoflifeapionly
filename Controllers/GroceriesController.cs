﻿//  Author:
//       Joe Merkel <merkeljoe9@gmail.com>
//
//  Copyright (c) 2018 Joe Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Http;
using System.Web.SessionState;
using System.Net;
using System.Net.Http;

using GameOfLife.Models;
using GameOfLife.Entities;
using GameOfLife.Filters;
using System.Threading;
using GameOfLife.App_Start;
using T4R.Data;

namespace GameOfLife.Controllers
{
    public class GroceriesController : ApiController
    {
        GroceriesRepository groceriesRepository = null;
        public GroceriesController()
        {
            this.groceriesRepository = new GroceriesRepository();
        }

        [TokenAuthFilter(true, true)]
        public HttpResponseMessage Get(HttpRequestMessage request, int? id = null)
        {
            Dictionary<string, dynamic> parms = request.GetParams();

            int? studentId = id;

            // Get our session.
            UserSessionIdentity identity = null;
            Session session = null;

            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity != null && Thread.CurrentPrincipal.Identity is UserSessionIdentity)
            {
                identity = (UserSessionIdentity)(Thread.CurrentPrincipal.Identity);

                if (identity.session != null)
                {
                    session = identity.session;
                }
            }

            // If we are not logged in, 404.
            if (session == null)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized);
            }

            // Use the user's user id, or the supplied ID if we are a teacher or admin.
            User u = (User)((UserSessionIdentity)(Thread.CurrentPrincipal.Identity)).user;
            string userType = u.user_type;
            if (!(userType.Equals("Teacher") || userType.Equals("Admin")))
            {
                studentId = session.user_id;
            }
            else if (studentId == null)
            {
                studentId = session.user_id;
            }

            DBConnection conn = new DBConnection("GameOfLife");
            Groceries result = groceriesRepository.Get(conn, studentId.Value);
            conn.Close();

            if (result == null)
            {
                return request.CreateResponse(HttpStatusCode.NoContent, new Groceries(studentId.Value, 0, new Grocery[0]));
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, result);
            }
        }

        [TokenAuthFilter(true, true)]
        public HttpResponseMessage Post(HttpRequestMessage request, int? id = null)
        {
            Dictionary<string, dynamic> parms = request.GetParams();
            bool result = false;

            int? studentId = id;

            // Get our session.
            UserSessionIdentity identity = null;
            Session session = null;

            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity != null && Thread.CurrentPrincipal.Identity is UserSessionIdentity)
            {
                identity = (UserSessionIdentity)(Thread.CurrentPrincipal.Identity);

                if (identity.session != null)
                {
                    session = identity.session;
                }
            }

            // If we are not logged in, 404.
            if (session == null)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized);
            }

            // Get our function's parameters.
            Dictionary<string, dynamic>.Enumerator enumerator = parms.GetEnumerator();
            int? student_id = studentId;
            int? amount = null;
            string title = null;
            int? price = null;

            while (enumerator.MoveNext())
            {
                KeyValuePair<string, dynamic> kv = enumerator.Current;

                switch (kv.Key)
                {
                    case "student_id":
                        try
                        {
                            student_id = Convert.ToInt32(kv.Value);
                        }
                        catch (Exception)
                        {
                        }

                        break;
                    case "amount":
                        try
                        {
                            amount = Convert.ToInt32(kv.Value);
                        }
                        catch (Exception)
                        {
                        }

                        break;
                    case "title":
                        try
                        {
                            title = (string)kv.Value;
                        }
                        catch (Exception)
                        {
                        }

                        break;
                    case "price":
                        try
                        {
                            price = Convert.ToInt32(kv.Value);
                        }
                        catch (Exception)
                        {
                        }

                        break;
                }
            }

            // Use the user's user id, or the supplied ID if we are a teacher or admin.
            User u = (User)((UserSessionIdentity)(Thread.CurrentPrincipal.Identity)).user;
            string userType = u.user_type;
            if (!(userType.Equals("Teacher") || userType.Equals("Admin")))
            {
                studentId = session.user_id;
            }
            else if (studentId != null)
            {
            }
            else if (student_id != null)
            {
                studentId = student_id.Value;
            }
            else
            {
                studentId = session.user_id;
            }

            // Create the entry.
            Grocery item = new Grocery(-1, studentId.Value, amount != null ? amount.Value : 0, title, price != null ? price.Value : 0);

            DBConnection conn = new DBConnection("GameOfLife");
            result = this.groceriesRepository.Add(conn, studentId.Value, item);
            conn.Close();

            if (result)
            {
                return request.CreateResponse(HttpStatusCode.OK, result);
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, "Failed to insert.");
            }
        }
    }
}
