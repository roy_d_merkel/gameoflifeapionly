﻿//
//  IncomeController.cs
//
//  Author:
//       Roy Merkel <merkel-roy@comcast.net>
//
//  Copyright (c) 2018 Roy Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.SessionState;
using GameOfLife.App_Start;
using GameOfLife.Entities;
using GameOfLife.Filters;
using GameOfLife.Models;
using T4R.Data;
using Newtonsoft.Json;

namespace GameOfLife.Controllers
{ 
	public class IncomeController : ApiController
    {
		IncomeRepository incomeRepository = null;
		JobsRepository jobsRepository = null;

		public IncomeController()
        {
			this.incomeRepository = new IncomeRepository();
			this.jobsRepository = new JobsRepository();
        }

		[TokenAuthFilter(true, true)]
		public HttpResponseMessage Post(HttpRequestMessage request, int id = -1)
        {
            try
            {
                // handle any other kind of processing here?
                Dictionary<string, dynamic> parms = request.GetParams();
                UserSessionIdentity identity = (UserSessionIdentity)(Thread.CurrentPrincipal.Identity);

				int user_id = -1;

				if (id > 0)
				{
					user_id = id;
				}
				else
				{
					user_id = identity.user.user_id;
				}
                
				int? number = null;
				string title = null;
				string type = null;
				int? salary = null;
				int? monthly_take_home = null;
                
                if (parms == null)
                {
                    return request.CreateResponse(HttpStatusCode.BadRequest, "No parameters provided!");
                }

                Dictionary<string, dynamic>.Enumerator enumerator = parms.GetEnumerator();
                
                while (enumerator.MoveNext())
                {
                    KeyValuePair<string, dynamic> kv = enumerator.Current;

                    switch (kv.Key)
                    {
						case "number":
                            try
                            {
								number = Convert.ToInt32(kv.Value);
                            }
                            catch (Exception)
							{
                            }

                            break;
						case "title":
							try
                            {
								title = Convert.ToString(kv.Value);
                            }
                            catch (Exception)
                            {
                            }
                            
                            break;
						case "type":
							try
                            {
								type = Convert.ToString(kv.Value);
                            }
                            catch (Exception)
                            {
                            }

                            break;
						case "salary":
                            try
                            {
								salary = Convert.ToInt32(kv.Value);
                            }
                            catch (Exception)
                            {
                            }

                            break;
						//case "monthly_take_home":
                        //    try
                        //    {
						//		monthly_take_home = Convert.ToInt32(kv.Value);
                        //    }
                        //    catch (Exception)
                        //    {
                        //    }
                        //
                        //    break;
                    }
                }
                
				if (title == null || type == null || salary == null/* || monthly_take_home == null*/)
                {
                    return request.CreateResponse(HttpStatusCode.BadRequest, "No all parameters provided!");
                }

				if (number == null)
				{
					number = -1;
				}

                if (monthly_take_home == null)
                {
                    monthly_take_home = -1;
                }

                DBConnection conn = new DBConnection("GameOfLife");
                HttpSessionState session = HttpContext.Current.Session;
                string ip = request.GetClientIpAddress();
                string userAgent = request.GetUserAgent();
                string referrer = request.GetReferrer();

                bool approved = false;
				Job job = new Job(-1, user_id, false, title, type, salary.Value, monthly_take_home.Value, approved, new DateTime(), false);
				bool success = this.jobsRepository.AddOrEditJobs(conn, user_id, -1, job);
				PostFamiliesResponse response = null;            

                if (!success)
                {
					return request.CreateResponse(HttpStatusCode.Unauthorized, response);
                }
                else
                {
					return request.CreateResponse(HttpStatusCode.OK, response);
                }
            }
            catch(Exception e)
            {
                return request.CreateResponse(HttpStatusCode.InternalServerError, e + "\n" + e.StackTrace);
            }
        }

		[TokenAuthFilter(true, true)]
        public HttpResponseMessage Put(HttpRequestMessage request, int id = -1)
        {
            try
            {
                // handle any other kind of processing here?
                Dictionary<string, dynamic> parms = request.GetParams();
                UserSessionIdentity identity = (UserSessionIdentity)(Thread.CurrentPrincipal.Identity);

                int user_id = -1;

                if (id > 0)
                {
                    user_id = id;
                }
                else
                {
                    user_id = identity.user.user_id;
                }

                int? number = null;
                string title = null;
                string type = null;
                int? salary = null;
                int? monthly_take_home = null;

                if (parms == null)
                {
                    return request.CreateResponse(HttpStatusCode.BadRequest, "No parameters provided!");
                }

                Dictionary<string, dynamic>.Enumerator enumerator = parms.GetEnumerator();

                while (enumerator.MoveNext())
                {
                    KeyValuePair<string, dynamic> kv = enumerator.Current;

                    switch (kv.Key)
                    {
                        case "number":
                            try
                            {
                                number = Convert.ToInt32(kv.Value);
                            }
                            catch (Exception)
                            {
                            }

                            break;
                        case "title":
                            try
                            {
                                title = Convert.ToString(kv.Value);
                            }
                            catch (Exception)
                            {
                            }

                            break;
                        case "type":
                            try
                            {
                                type = Convert.ToString(kv.Value);
                            }
                            catch (Exception)
                            {
                            }

                            break;
                        case "salary":
                            try
                            {
                                salary = Convert.ToInt32(kv.Value);
                            }
                            catch (Exception)
                            {
                            }

                            break;
                        //case "monthly_take_home":
                        //    try
                        //    {
                        //        monthly_take_home = Convert.ToInt32(kv.Value);
                        //    }
                        //    catch (Exception)
                        //    {
                        //    }
                        //
                        //    break;
                    }
                }

                if (title == null || type == null || salary == null/* || monthly_take_home == null*/)
                {
                    return request.CreateResponse(HttpStatusCode.BadRequest, "No all parameters provided!");
                }

				if (number == null)
				{
					number = -1;
				}

				if (monthly_take_home == null)
				{
					monthly_take_home = -1;
				}

                DBConnection conn = new DBConnection("GameOfLife");
                HttpSessionState session = HttpContext.Current.Session;
                string ip = request.GetClientIpAddress();
                string userAgent = request.GetUserAgent();
                string referrer = request.GetReferrer();

                bool approved = false;
				Job job = new Job(number.Value, user_id, false, title, type, salary.Value, monthly_take_home.Value, approved, new DateTime(), false);
				bool success = this.jobsRepository.AddOrEditJobs(conn, user_id, number.Value, job);
                PostFamiliesResponse response = null;

                if (!success)
                {
                    return request.CreateResponse(HttpStatusCode.Unauthorized, response);
                }
                else
                {
                    return request.CreateResponse(HttpStatusCode.OK, response);
                }
            }
            catch (Exception e)
            {
                return request.CreateResponse(HttpStatusCode.InternalServerError, e + "\n" + e.StackTrace);
            }
        }

        [TokenAuthFilter(true, true)]
        public HttpResponseMessage Get(HttpRequestMessage request, int id = -1)
        {
            // handle any other kind of processing here?
            Dictionary<string, dynamic> parms = request.GetParams();
            UserSessionIdentity identity = (UserSessionIdentity)(Thread.CurrentPrincipal.Identity);

			int user_id = -1;
            
            if (id > 0)
            {
                user_id = id;
            }
            else
            {
                user_id = identity.user.user_id;
            }
         
            DBConnection conn = new DBConnection("GameOfLife");

			Income result = incomeRepository.Get(conn, user_id);

            conn.Close();

            if (result == null)
            {
				return request.CreateResponse(HttpStatusCode.NoContent, result);
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, result);
            }
        }

		[TokenAuthFilter(true, true)]
        public HttpResponseMessage Delete(HttpRequestMessage request, int i1 = -1, int i2 = -1)
        {
			int userId;
			int jobId;

			if (i1 > 0 && i2 > 0)
			{
				userId = i1;
				jobId = i2;
			}
			else if (i1 > 0)
			{
				userId = -1;
				jobId = i1;
			}
			else
			{
				userId = -1;
				jobId = -1;
			}

            // handle any other kind of processing here?
            Dictionary<string, dynamic> parms = request.GetParams();
            UserSessionIdentity identity = (UserSessionIdentity)(Thread.CurrentPrincipal.Identity);

            int user_id = -1;

            if (user_id > 0)
            {
				user_id = userId;
            }
            else
            {
                user_id = identity.user.user_id;
            }

			if (jobId < 0)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, "No all parameters provided!");
            }

            DBConnection conn = new DBConnection("GameOfLife");
            
            bool result = jobsRepository.Delete(conn, user_id, jobId);

            conn.Close();

            if (!result)
            {
				return request.CreateResponse(HttpStatusCode.OK, result);
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, result);
            }
        }

        [TokenAuthFilter(true, true)]
        [IsAdminFilter(true, true)]
        [System.Web.Http.Route("income/{user_id}/jobs/{job_id}/approve")]
        [System.Web.Http.HttpPost()]
        //[System.Web.Http.HttpGet()]
        public HttpResponseMessage Approve(HttpRequestMessage request, int user_id, int job_id)
        {
            try
            {
                DBConnection conn = new DBConnection("GameOfLife");
                bool success = this.jobsRepository.Approve(conn, user_id, job_id);

                if (!success)
                {
                    return request.CreateResponse(HttpStatusCode.Unauthorized, success);
                }
                else
                {
                    return request.CreateResponse(HttpStatusCode.OK, success);
                }
            }
            catch (Exception e)
            {
                return request.CreateResponse(HttpStatusCode.InternalServerError, e + "\n" + e.StackTrace);
            }
        }

        [TokenAuthFilter(true, true)]
        [IsAdminFilter(true, true)]
        [System.Web.Http.Route("income/{user_id}/jobs/{job_id}/disapprove")]
        [System.Web.Http.HttpPost()]
        //[System.Web.Http.HttpGet()]
        public HttpResponseMessage Disapprove(HttpRequestMessage request, int user_id, int job_id)
        {
            try
            {
                DBConnection conn = new DBConnection("GameOfLife");
                bool success = this.jobsRepository.Disapprove(conn, user_id, job_id);

                if (!success)
                {
                    return request.CreateResponse(HttpStatusCode.Unauthorized, success);
                }
                else
                {
                    return request.CreateResponse(HttpStatusCode.OK, success);
                }
            }
            catch (Exception e)
            {
                return request.CreateResponse(HttpStatusCode.InternalServerError, e + "\n" + e.StackTrace);
            }
        }
    }
}
