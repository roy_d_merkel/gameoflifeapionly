﻿//  Author:
//       Joe Merkel <merkeljoe9@gmail.com>
//
//  Copyright (c) 2018 Joe Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Http;
using System.Web.SessionState;
using System.Net;
using System.Net.Http;

using GameOfLife.Models;
using GameOfLife.Entities;
using GameOfLife.Filters;
using System.Threading;
using GameOfLife.App_Start;
using T4R.Data;

namespace GameOfLife.Controllers
{
    public class BankController : ApiController
    {
        BankRepository bankRepository = null;
        public BankController()
        {
            this.bankRepository = new BankRepository();
        }

        [TokenAuthFilter(true, true)]
        public HttpResponseMessage Get(HttpRequestMessage request, int? id = null)
        {
            Dictionary<string, dynamic> parms = request.GetParams();
            Bank result;

            int? studentId = id;

            // Get our session.
            UserSessionIdentity identity = null;
            Session session = null;

			User u = (User)((UserSessionIdentity)(Thread.CurrentPrincipal.Identity)).user;
            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity != null && Thread.CurrentPrincipal.Identity is UserSessionIdentity)
            {
                identity = (UserSessionIdentity)(Thread.CurrentPrincipal.Identity);

                if (identity.session != null)
                {
                    session = identity.session;
                }
            }

            // If we are not logged in, 404.
            if (session == null)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized);
            }

            // Use the user's user id, or the supplied ID if we are a teacher or admin.
            string userType = u.user_type;
            if (!(userType.Equals("Teacher") || userType.Equals("Admin")))
            {
                studentId = session.user_id;
            }
            else if (studentId == null)
            {
                studentId = session.user_id;
            }

            // Return the banks.
            DBConnection conn = new DBConnection("GameOfLife");
            result = this.bankRepository.Get(conn, studentId.Value);
            conn.Close();

            if(result != null)
            {
                return request.CreateResponse(HttpStatusCode.OK, result);
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.NoContent, new Items(studentId.Value, 0, new Item[0]));
            }
        }

        [TokenAuthFilter(true, true)]
        public HttpResponseMessage Put(HttpRequestMessage request, int? id = null)
        {

            // handle any other kind of processing here?
            Dictionary<string, dynamic> parms = request.GetParams();

            int? studentId = id;

            int? user_id = null;
            int? amount = null;

            if (parms == null)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, "No parameters provided!");
            }

            Dictionary<string, dynamic>.Enumerator enumerator = parms.GetEnumerator();

            while (enumerator.MoveNext())
            {
                KeyValuePair<string, dynamic> kv = enumerator.Current;

                switch (kv.Key)
                {
                    case "user_id":
                        try
                        {
                            user_id = Convert.ToInt32(kv.Value);
                        }
                        catch (Exception)
                        {
                        }

                        break;
                    case "cash_on_hand":
                        try
                        {
                            amount = Convert.ToInt32(kv.Value);
                        }
                        catch (Exception)
                        {
                        }
                        break;
                }
            }

            if (amount == null)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, "No all parameters provided!");
            }

            // Get our session.
            UserSessionIdentity identity = null;
            Session session = null;

            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity != null && Thread.CurrentPrincipal.Identity is UserSessionIdentity)
            {
                identity = (UserSessionIdentity)(Thread.CurrentPrincipal.Identity);

                if (identity.session != null)
                {
                    session = identity.session;
                }
            }

            // If we are not logged in, 404.
            if (session == null)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized);
            }

            // Use the user's user id, or the supplied ID if we are a teacher or admin.
            User u = (User)((UserSessionIdentity)(Thread.CurrentPrincipal.Identity)).user;
            string userType = u.user_type;
            if (!(userType.Equals("Teacher") || userType.Equals("Admin")))
            {
                studentId = session.user_id;
            }
            else if (studentId != null)
            {

            }
            else if (user_id != null)
            {
                studentId = user_id;
            }
            else
            {
                studentId = session.user_id;
            }

            DBConnection conn = new DBConnection("GameOfLife");

            bool success = this.bankRepository.Set(conn, studentId.Value, amount ?? 0);

            if (!success)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, new ApiResult(false, "Insert failed."));
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, new ApiResult(true, ""));
            }
        }

        [TokenAuthFilter(true, true)]
        [System.Web.Http.Route("bank/checking/deposit")]
        [System.Web.Http.HttpPost()]
        public HttpResponseMessage Deposit(HttpRequestMessage request, int? id = null)
        {
            
            // handle any other kind of processing here?
            Dictionary<string, dynamic> parms = request.GetParams();

            int? studentId = id;

            int? user_id = null;
            int? amount = null;

            if (parms == null)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, "No parameters provided!");
            }

            Dictionary<string, dynamic>.Enumerator enumerator = parms.GetEnumerator();

            while (enumerator.MoveNext())
            {
                KeyValuePair<string, dynamic> kv = enumerator.Current;

                switch (kv.Key)
                {
                    case "user_id":
                        try
                        {
                            user_id = Convert.ToInt32(kv.Value);
                        }
                        catch (Exception)
                        {
                        }

                        break;
                    case "amount":
                        try
                        {
                            amount = Convert.ToInt32(kv.Value);
                        }
                        catch (Exception)
                        {
                        }
                        break;
                }
            }

            if (amount == null)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, "No all parameters provided!");
            }

            // Get our session.
            UserSessionIdentity identity = null;
            Session session = null;

            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity != null && Thread.CurrentPrincipal.Identity is UserSessionIdentity)
            {
                identity = (UserSessionIdentity)(Thread.CurrentPrincipal.Identity);

                if (identity.session != null)
                {
                    session = identity.session;
                }
            }

            // If we are not logged in, 404.
            if (session == null)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized);
            }

            // Use the user's user id, or the supplied ID if we are a teacher or admin.
            User u = (User)((UserSessionIdentity)(Thread.CurrentPrincipal.Identity)).user;
            string userType = u.user_type;
            if (!(userType.Equals("Teacher") || userType.Equals("Admin")))
            {
                studentId = session.user_id;
            }
            else if(studentId != null)
            {
                
            }
            else if(user_id != null)
            {
                studentId = user_id;
            }
            else
            {
                studentId = session.user_id;
            }

            DBConnection conn = new DBConnection("GameOfLife");

            bool success = this.bankRepository.DepositChecking(conn, studentId.Value, amount??0);

            if (!success)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, new ApiResult(false, "Insert failed."));
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, new ApiResult(true, ""));
            }
        }

        [TokenAuthFilter(true, true)]
        [System.Web.Http.Route("bank/checking/transfer/savings")]
        [System.Web.Http.HttpPost()]
        public HttpResponseMessage TransferFromCheckingToSavings(HttpRequestMessage request, int? id = null)
        {

            // handle any other kind of processing here?
            Dictionary<string, dynamic> parms = request.GetParams();

            int? studentId = id;

            int? user_id = null;
            int? amount = null;

            if (parms == null)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, "No parameters provided!");
            }

            Dictionary<string, dynamic>.Enumerator enumerator = parms.GetEnumerator();

            while (enumerator.MoveNext())
            {
                KeyValuePair<string, dynamic> kv = enumerator.Current;

                switch (kv.Key)
                {
                    case "user_id":
                        try
                        {
                            user_id = Convert.ToInt32(kv.Value);
                        }
                        catch (Exception)
                        {
                        }

                        break;
                    case "amount":
                        try
                        {
                            amount = Convert.ToInt32(kv.Value);
                        }
                        catch (Exception)
                        {
                        }
                        break;
                }
            }

            if (amount == null)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, "No all parameters provided!");
            }

            // Get our session.
            UserSessionIdentity identity = null;
            Session session = null;

            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity != null && Thread.CurrentPrincipal.Identity is UserSessionIdentity)
            {
                identity = (UserSessionIdentity)(Thread.CurrentPrincipal.Identity);

                if (identity.session != null)
                {
                    session = identity.session;
                }
            }

            // If we are not logged in, 404.
            if (session == null)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized);
            }

            // Use the user's user id, or the supplied ID if we are a teacher or admin.
            User u = (User)((UserSessionIdentity)(Thread.CurrentPrincipal.Identity)).user;
            string userType = u.user_type;
            if (!(userType.Equals("Teacher") || userType.Equals("Admin")))
            {
                studentId = session.user_id;
            }
            else if (studentId != null)
            {

            }
            else if (user_id != null)
            {
                studentId = user_id;
            }
            else
            {
                studentId = session.user_id;
            }

            DBConnection conn = new DBConnection("GameOfLife");

            bool success = this.bankRepository.CheckingToSavings(conn, studentId.Value, amount ?? 0);

            if (!success)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, new ApiResult(false, "Insert failed."));
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, new ApiResult(true, ""));
            }
        }

        [TokenAuthFilter(true, true)]
        [System.Web.Http.Route("bank/checking/withdraw")]
        [System.Web.Http.HttpPost()]
        public HttpResponseMessage WithdrawChecking(HttpRequestMessage request, int? id = null)
        {

            // handle any other kind of processing here?
            Dictionary<string, dynamic> parms = request.GetParams();

            int? studentId = id;

            int? user_id = null;
            int? amount = null;

            if (parms == null)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, "No parameters provided!");
            }

            Dictionary<string, dynamic>.Enumerator enumerator = parms.GetEnumerator();

            while (enumerator.MoveNext())
            {
                KeyValuePair<string, dynamic> kv = enumerator.Current;

                switch (kv.Key)
                {
                    case "user_id":
                        try
                        {
                            user_id = Convert.ToInt32(kv.Value);
                        }
                        catch (Exception)
                        {
                        }

                        break;
                    case "amount":
                        try
                        {
                            amount = Convert.ToInt32(kv.Value);
                        }
                        catch (Exception)
                        {
                        }
                        break;
                }
            }

            if (amount == null)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, "No all parameters provided!");
            }

            // Get our session.
            UserSessionIdentity identity = null;
            Session session = null;

            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity != null && Thread.CurrentPrincipal.Identity is UserSessionIdentity)
            {
                identity = (UserSessionIdentity)(Thread.CurrentPrincipal.Identity);

                if (identity.session != null)
                {
                    session = identity.session;
                }
            }

            // If we are not logged in, 404.
            if (session == null)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized);
            }

            // Use the user's user id, or the supplied ID if we are a teacher or admin.
            User u = (User)((UserSessionIdentity)(Thread.CurrentPrincipal.Identity)).user;
            string userType = u.user_type;
            if (!(userType.Equals("Teacher") || userType.Equals("Admin")))
            {
                studentId = session.user_id;
            }
            else if (studentId != null)
            {

            }
            else if (user_id != null)
            {
                studentId = user_id;
            }
            else
            {
                studentId = session.user_id;
            }

            DBConnection conn = new DBConnection("GameOfLife");

            bool success = this.bankRepository.WithdrawChecking(conn, studentId.Value, amount ?? 0);

            if (!success)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, new ApiResult(false, "Insert failed."));
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, new ApiResult(true, ""));
            }
        }

        [TokenAuthFilter(true, true)]
        [System.Web.Http.Route("bank/savings/deposit")]
        [System.Web.Http.HttpPost()]
        public HttpResponseMessage DepositSavings(HttpRequestMessage request, int? id = null)
        {

            // handle any other kind of processing here?
            Dictionary<string, dynamic> parms = request.GetParams();

            int? studentId = id;

            int? user_id = null;
            int? amount = null;

            if (parms == null)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, "No parameters provided!");
            }

            Dictionary<string, dynamic>.Enumerator enumerator = parms.GetEnumerator();

            while (enumerator.MoveNext())
            {
                KeyValuePair<string, dynamic> kv = enumerator.Current;

                switch (kv.Key)
                {
                    case "user_id":
                        try
                        {
                            user_id = Convert.ToInt32(kv.Value);
                        }
                        catch (Exception)
                        {
                        }

                        break;
                    case "amount":
                        try
                        {
                            amount = Convert.ToInt32(kv.Value);
                        }
                        catch (Exception)
                        {
                        }
                        break;
                }
            }

            if (amount == null)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, "No all parameters provided!");
            }

            // Get our session.
            UserSessionIdentity identity = null;
            Session session = null;

            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity != null && Thread.CurrentPrincipal.Identity is UserSessionIdentity)
            {
                identity = (UserSessionIdentity)(Thread.CurrentPrincipal.Identity);

                if (identity.session != null)
                {
                    session = identity.session;
                }
            }

            // If we are not logged in, 404.
            if (session == null)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized);
            }

            // Use the user's user id, or the supplied ID if we are a teacher or admin.
            User u = (User)((UserSessionIdentity)(Thread.CurrentPrincipal.Identity)).user;
            string userType = u.user_type;
            if (!(userType.Equals("Teacher") || userType.Equals("Admin")))
            {
                studentId = session.user_id;
            }
            else if (studentId != null)
            {

            }
            else if (user_id != null)
            {
                studentId = user_id;
            }
            else
            {
                studentId = session.user_id;
            }

            DBConnection conn = new DBConnection("GameOfLife");

            bool success = this.bankRepository.DepositSavings(conn, studentId.Value, amount ?? 0);

            if (!success)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, new ApiResult(false, "Insert failed."));
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, new ApiResult(true, ""));
            }
        }

        [TokenAuthFilter(true, true)]
        [System.Web.Http.Route("bank/savings/transfer/checking")]
        [System.Web.Http.HttpPost()]
        public HttpResponseMessage TransferFromSavingsToChecking(HttpRequestMessage request, int? id = null)
        {

            // handle any other kind of processing here?
            Dictionary<string, dynamic> parms = request.GetParams();

            int? studentId = id;

            int? user_id = null;
            int? amount = null;

            if (parms == null)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, "No parameters provided!");
            }

            Dictionary<string, dynamic>.Enumerator enumerator = parms.GetEnumerator();

            while (enumerator.MoveNext())
            {
                KeyValuePair<string, dynamic> kv = enumerator.Current;

                switch (kv.Key)
                {
                    case "user_id":
                        try
                        {
                            user_id = Convert.ToInt32(kv.Value);
                        }
                        catch (Exception)
                        {
                        }

                        break;
                    case "amount":
                        try
                        {
                            amount = Convert.ToInt32(kv.Value);
                        }
                        catch (Exception)
                        {
                        }
                        break;
                }
            }

            if (amount == null)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, "No all parameters provided!");
            }

            // Get our session.
            UserSessionIdentity identity = null;
            Session session = null;

            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity != null && Thread.CurrentPrincipal.Identity is UserSessionIdentity)
            {
                identity = (UserSessionIdentity)(Thread.CurrentPrincipal.Identity);

                if (identity.session != null)
                {
                    session = identity.session;
                }
            }

            // If we are not logged in, 404.
            if (session == null)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized);
            }

            // Use the user's user id, or the supplied ID if we are a teacher or admin.
            User u = (User)((UserSessionIdentity)(Thread.CurrentPrincipal.Identity)).user;
            string userType = u.user_type;
            if (!(userType.Equals("Teacher") || userType.Equals("Admin")))
            {
                studentId = session.user_id;
            }
            else if (studentId != null)
            {

            }
            else if (user_id != null)
            {
                studentId = user_id;
            }
            else
            {
                studentId = session.user_id;
            }

            DBConnection conn = new DBConnection("GameOfLife");

            bool success = this.bankRepository.SavingsToChecking(conn, studentId.Value, amount ?? 0);

            if (!success)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, new ApiResult(false, "Insert failed."));
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, new ApiResult(true, ""));
            }
        }

        [TokenAuthFilter(true, true)]
        [System.Web.Http.Route("bank/savings/withdraw")]
        [System.Web.Http.HttpPost()]
        public HttpResponseMessage WithdrawSavings(HttpRequestMessage request, int? id = null)
        {

            // handle any other kind of processing here?
            Dictionary<string, dynamic> parms = request.GetParams();

            int? studentId = id;

            int? user_id = null;
            int? amount = null;

            if (parms == null)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, "No parameters provided!");
            }

            Dictionary<string, dynamic>.Enumerator enumerator = parms.GetEnumerator();

            while (enumerator.MoveNext())
            {
                KeyValuePair<string, dynamic> kv = enumerator.Current;

                switch (kv.Key)
                {
                    case "user_id":
                        try
                        {
                            user_id = Convert.ToInt32(kv.Value);
                        }
                        catch (Exception)
                        {
                        }

                        break;
                    case "amount":
                        try
                        {
                            amount = Convert.ToInt32(kv.Value);
                        }
                        catch (Exception)
                        {
                        }
                        break;
                }
            }

            if (amount == null)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, "No all parameters provided!");
            }

            // Get our session.
            UserSessionIdentity identity = null;
            Session session = null;

            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity != null && Thread.CurrentPrincipal.Identity is UserSessionIdentity)
            {
                identity = (UserSessionIdentity)(Thread.CurrentPrincipal.Identity);

                if (identity.session != null)
                {
                    session = identity.session;
                }
            }

            // If we are not logged in, 404.
            if (session == null)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized);
            }

            // Use the user's user id, or the supplied ID if we are a teacher or admin.
            User u = (User)((UserSessionIdentity)(Thread.CurrentPrincipal.Identity)).user;
            string userType = u.user_type;
            if (!(userType.Equals("Teacher") || userType.Equals("Admin")))
            {
                studentId = session.user_id;
            }
            else if (studentId != null)
            {

            }
            else if (user_id != null)
            {
                studentId = user_id;
            }
            else
            {
                studentId = session.user_id;
            }

            DBConnection conn = new DBConnection("GameOfLife");

            bool success = this.bankRepository.WithdrawSavings(conn, studentId.Value, amount ?? 0);

            if (!success)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, new ApiResult(false, "Insert failed."));
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, new ApiResult(true, ""));
            }
        }
    }
}
