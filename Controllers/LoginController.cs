﻿//
//  LoginController.cs
//
//  Author:
//       Roy Merkel <merkel-roy@comcast.net>
//
//  Copyright (c) 2018 Roy Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.SessionState;
using GameOfLife.App_Start;
using GameOfLife.Entities;
using GameOfLife.Filters;
using GameOfLife.Models;
using T4R.Data;
using Newtonsoft.Json;

namespace GameOfLife.Controllers
{
	public class IsLoggedInResponse
    {
        private bool _isLoggedIn;

        public IsLoggedInResponse(bool isLoggedIn)
        {
            _isLoggedIn = isLoggedIn;
        }

        public bool logged_in { get { return _isLoggedIn; } }
    }
    
    public class LoginResponse
    {
        private string _token;
        private IAuthUser _user;
		private bool _isFirstLogin;
		private bool _launched;
        private string _now;
        private string _nextTickTime;
        private TimeSpan? _timeToNextTick;

		public LoginResponse(string token, IAuthUser user, bool isFirstLogin, bool launched, string now, string nextTickTime, TimeSpan? timeToNextTick)
        {
            _token = token;
            _user = user;
			_isFirstLogin = isFirstLogin;
			_launched = launched;
            _now = now;
            _nextTickTime = nextTickTime;
            _timeToNextTick = timeToNextTick;
        }

        public string token { get { return _token; } }
        public IAuthUser user {  get { return _user; } }
		public bool is_first_login { get { return _isFirstLogin; } }
		public bool launched { get { return _launched; } }
        public string server_now { get { return _now; } }
        public string server_next_tick_time { get { return _nextTickTime; } }
        public TimeSpan? time_to_next_tick { get { return _timeToNextTick; } }
    }

    public class LoginController : ApiController
    {
        SessionRepository sessionRepository = null;
		GameStateRepository gameStateRepository = null;

        public LoginController()
        {
            this.sessionRepository = new SessionRepository();
			this.gameStateRepository = new GameStateRepository();
        }

		[TokenAuthFilter(true, false)]
        [System.Web.Http.Route("is_logged_in")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            UserSessionIdentity identity = null;
            Session session = null;

            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity != null && Thread.CurrentPrincipal.Identity is UserSessionIdentity)
            {
                identity = (UserSessionIdentity)(Thread.CurrentPrincipal.Identity);

                if (identity.session != null)
                {
                    session = identity.session;
                }
            }

            return request.CreateResponse(HttpStatusCode.OK, new IsLoggedInResponse(identity != null && session != null && session.session_id > 0));
        }

        public HttpResponseMessage Post(HttpRequestMessage request, string Id = null)
        {
            // handle any other kind of processing here?
            Dictionary<string, dynamic> parms = request.GetParams();

            string user_name = Id;
            string password = null;

            if (parms == null)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, "No parameters provided!");
            }

            Dictionary<string, dynamic>.Enumerator enumerator = parms.GetEnumerator();

            while (enumerator.MoveNext())
            {
                KeyValuePair<string, dynamic> kv = enumerator.Current;

                switch (kv.Key)
                {
                    case "password":
                        try
                        {
                            password = (string)kv.Value;
                        }
                        catch (Exception)
                        {
                        }

                        break;
                }
            }

            if (password == null || user_name == null)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, "No all parameters provided!");
            }

			DBConnection conn = new DBConnection("GameOfLife");
            HttpSessionState session = HttpContext.Current.Session;
            string ip = request.GetClientIpAddress();
            string userAgent = request.GetUserAgent();
            string referrer = request.GetReferrer();

            IAuthUser user = null;
            string token = this.sessionRepository.DoLogin(conn, session, user_name, password, ip, userAgent, DateTime.Now, out user);

			if (token != null && user != null)
			{
				bool isFirstLogin = this.sessionRepository.IsFirstLogin(conn, user.user_id);

				bool launched;
                DateTime? now;
                DateTime? nextDateTime;
                TimeSpan? timeToNextTick;

				timeToNextTick = gameStateRepository.GetNextTick(conn, user.user_id, out launched, out now, out nextDateTime);
            
				return request.CreateResponse(HttpStatusCode.OK, new LoginResponse(token, user, isFirstLogin, launched, ((now == null || !now.HasValue) ? null : now.Value.ToString("yyyy/MM/dd HH:mm:ss")), ((nextDateTime == null || !nextDateTime.HasValue) ? null : nextDateTime.Value.ToString("yyyy/MM/dd HH:mm:ss")), timeToNextTick));
			}
			else
			{
				return request.CreateResponse(HttpStatusCode.Unauthorized, new LoginResponse(null, null, false, false, null, null, null));
			}
        }
    }
}
