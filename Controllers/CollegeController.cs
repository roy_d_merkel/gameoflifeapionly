﻿//
//  CollegeController.cs
//
//  Author:
//       Roy Merkel <merkel-roy@comcast.net>
//
//  Copyright (c) 2018 Roy Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.SessionState;
using GameOfLife.App_Start;
using GameOfLife.Entities;
using GameOfLife.Filters;
using GameOfLife.Models;
using T4R.Data;
using Newtonsoft.Json;

namespace GameOfLife.Controllers
{ 
	public class CollegeController : ApiController
    {
        CollegeRepository collegeRepository = null;
        LoanRepository loanRepository = null;
        JobsRepository jobsRepository = null;

		public CollegeController()
        {
            this.collegeRepository = new CollegeRepository();
            this.loanRepository = new LoanRepository();
            this.jobsRepository = new JobsRepository();
        }

		[TokenAuthFilter(true)]
        public HttpResponseMessage Post(HttpRequestMessage request)
        {
            // handle any other kind of processing here?
            Dictionary<string, dynamic> parms = request.GetParams();
            UserSessionIdentity identity = (UserSessionIdentity)(Thread.CurrentPrincipal.Identity);

            int user_id = identity.user.user_id;
            string college_name = null;
            string school_degree = null;
            string program_name = null;
            decimal? school_debt = null;
            decimal? school_loan_payment = null;
            decimal? salary = null;

            if (parms == null)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, "No parameters provided!");
            }

            Dictionary<string, dynamic>.Enumerator enumerator = parms.GetEnumerator();

            while (enumerator.MoveNext())
            {
                KeyValuePair<string, dynamic> kv = enumerator.Current;

                switch (kv.Key)
                {
                    case "college_name":
                        try
                        {
                            college_name = (string)kv.Value;
                        }
                        catch (Exception)
                        {
                        }

                        break;
                    case "school_degree":
                        try
                        {
                            school_degree = (string)kv.Value;
                        }
                        catch (Exception)
                        {
                        }

                        break;
                    case "program_name":
                        try
                        {
                            program_name = (string)kv.Value;
                        }
                        catch (Exception)
                        {
                        }

                        break;
                    case "school_debt":
                        try
                        {
                            school_debt = Convert.ToDecimal(kv.Value);
                        }
                        catch (Exception)
                        {
                        }

                        break;
                    case "school_loan_payment":
                        try
                        {
                            school_loan_payment = Convert.ToDecimal(kv.Value);
                        }
                        catch (Exception)
                        {
                        }

                        break;
                    case "salary":
                        try
                        {
                            salary = Convert.ToDecimal(kv.Value);
                        }
                        catch (Exception)
                        {
                        }

                        break;
                }
            }

            if (college_name == null && school_degree == null && program_name == null && school_debt == null && school_loan_payment == null && salary == null)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, "No all parameters provided!");
            }

            DBConnection conn = new DBConnection("GameOfLife");
            HttpSessionState session = HttpContext.Current.Session;
            string ip = request.GetClientIpAddress();
            string userAgent = request.GetUserAgent();
            string referrer = request.GetReferrer();

            bool success = this.collegeRepository.Set(conn, user_id, new College(user_id, college_name, school_degree, program_name, ((school_debt != null) ? school_debt.Value : new decimal(0.0)), ((school_loan_payment != null) ? school_loan_payment.Value : new decimal(0.0)), ((salary != null) ? salary.Value : new decimal(0.0))));
            if(success)
            {
                // Add Loan for College.
                success = this.loanRepository.AddCollegeLoan(conn, user_id, Convert.ToInt32((school_loan_payment != null) ? school_loan_payment.Value : new decimal(0.0)), Convert.ToInt32((school_debt != null) ? school_debt.Value : new decimal(0.0)));

            }
            // Add/Set job for the college settings.
            if(success)
            {
                success = this.jobsRepository.AddEditCollegeJobs(conn, user_id, program_name, Convert.ToInt32((salary != null) ? salary.Value : new decimal(0.0)));
            }

            if (!success)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, new ApiResult(false, "Insert failed."));
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, new ApiResult(true, ""));
            }
        }

        [TokenAuthFilter(true)]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            // handle any other kind of processing here?
            Dictionary<string, dynamic> parms = request.GetParams();
            UserSessionIdentity identity = (UserSessionIdentity)(Thread.CurrentPrincipal.Identity);

            int user_id = identity.user.user_id;

            DBConnection conn = new DBConnection("GameOfLife");

            College result = collegeRepository.Get(conn, user_id);

            conn.Close();

            if (result == null)
            {
                return request.CreateResponse(HttpStatusCode.NoContent, new College(user_id, "", "", "", new decimal(0.0), new decimal(0.0), new decimal(0.0)));
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, result);
            }
        }
    }
}
