﻿//
//  OverviewController.cs
//
//  Author:
//       Roy Merkel <merkel-roy@comcast.net>
//
//  Copyright (c) 2018 Roy Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Http;
using System.Web.SessionState;
using System.Net;
using System.Net.Http;
using T4R.Data;

using GameOfLife.Models;
using GameOfLife.Entities;
using GameOfLife.Filters;
using System.Threading;
using GameOfLife.App_Start;

namespace GameOfLife.Controllers
{
	public class OverView
	{
		private int _cashOnHand;
		private int _monthlyIncome;
		private int _monthlyExpenses;
		private int _monthlyBalance;
		private Expense[] _expenses;
		private bool _launched;
		private string _now;
		private string _nextTickTime;
		private TimeSpan? _timeToNextTick;
        private int _currentGameMonth;

        public OverView( int cashOnHand, int monthlyIncome, int monthlyExpenses, int monthlyBalance, Expense[] expenses, bool launched, string now, string nextTickTime, TimeSpan? timeToNextTick, int currentGameMonth)
		{
			_cashOnHand = cashOnHand;
			_monthlyIncome = monthlyIncome;
			_monthlyExpenses = monthlyExpenses;
			_monthlyBalance = monthlyBalance;
			_expenses = expenses;
			_launched = launched;
			_now = now;
			_nextTickTime = nextTickTime;
			_timeToNextTick = timeToNextTick;
            _currentGameMonth = currentGameMonth;
		}

		public int cash_on_hand { get { return _cashOnHand;  } }
		public int monthly_income { get { return _monthlyIncome; } }
		public int monthly_expenses { get { return _monthlyExpenses; } }
		public int monthly_balance { get { return _monthlyBalance; } }
		public Expense[] expenses { get { return _expenses; } }
		public bool launched { get { return _launched; } }
		public string server_now { get { return _now; } }
		public string server_next_tick_time { get { return _nextTickTime; } }
        public TimeSpan? time_to_next_tick { get { return _timeToNextTick; } }
        public int current_game_month { get { return _currentGameMonth; } }
	}

	public class OverviewController : ApiController
    {
		BankRepository bankRepository = null;
		IncomeRepository incomeRepository = null;
		ExpenseRepository expenseRepository = null;
		GameStateRepository gameStateRepository = null;
		public OverviewController()
        {
            this.bankRepository = new BankRepository();
			this.incomeRepository = new IncomeRepository();
			this.expenseRepository = new ExpenseRepository();
			this.gameStateRepository = new GameStateRepository();
        }

        [TokenAuthFilter(true, true)]
        public HttpResponseMessage Get(HttpRequestMessage request, int id = -1)
        {
			OverView results = null;
			DBConnection conn = new DBConnection("GameOfLife");
			Dictionary<string, dynamic> parms = request.GetParams();
            UserSessionIdentity identity = (UserSessionIdentity)(Thread.CurrentPrincipal.Identity);

			int studentId = -1;

			if (id > 0)
            {
				studentId = id;
            }
            else
            {
				studentId = identity.user.user_id;
            }
            
			Bank bank = this.bankRepository.Get(conn, studentId);
			Income income = this.incomeRepository.Get(conn, studentId);
			GameState gameState = this.gameStateRepository.Get(conn);
			List<Expense> expenses = this.expenseRepository.expenses(conn, studentId, gameState.current_game_month_number);

			decimal monthlyExpenses = 0;

			foreach (Expense expense in expenses)
			{
				monthlyExpenses += Convert.ToDecimal(expense.amount);
			}

			int imonthlyExpenses = Convert.ToInt32(Math.Floor(monthlyExpenses));

			int monthlyBalance = income.monthly_income - imonthlyExpenses;

			bool launched;
			DateTime? now;
			DateTime? nextDateTime;
			TimeSpan? timeToNextTick;

			timeToNextTick = gameStateRepository.GetNextTick(conn, studentId, out launched, out now, out nextDateTime);

            results = new OverView((bank == null) ? 0 : bank.cash_on_hand, income.monthly_income, imonthlyExpenses, monthlyBalance, expenses.ToArray(), launched, ((now == null || !now.HasValue) ? null : now.Value.ToString("yyyy/MM/dd HH:mm:ss")), ((nextDateTime == null || !nextDateTime.HasValue) ? null : nextDateTime.Value.ToString("yyyy/MM/dd HH:mm:ss")), timeToNextTick, gameState.current_game_month_number);
			return request.CreateResponse(HttpStatusCode.OK, results);
        }
    }
}
