﻿//  Author:
//       Joe Merkel <merkeljoe9@gmail.com>
//
//  Copyright (c) 2018 Joe Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Http;
using System.Web.SessionState;
using System.Net;
using System.Net.Http;

using GameOfLife.Models;
using GameOfLife.Entities;
using GameOfLife.Filters;
using System.Threading;
using GameOfLife.App_Start;
using T4R.Data;

namespace GameOfLife.Controllers
{
    public class JobsController : ApiController
    {
        JobsRepository jobsRepository = null;
        public JobsController()
        {
            this.jobsRepository = new JobsRepository();
        }

        [TokenAuthFilter(true, true)]
        public HttpResponseMessage Get(HttpRequestMessage request, int? id = null)
        {
            Dictionary<string, dynamic> parms = request.GetParams();
            Job[] result = null;

            int? studentId = id;

            // Get our session.
            UserSessionIdentity identity = null;
            Session session = null;

            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity != null && Thread.CurrentPrincipal.Identity is UserSessionIdentity)
            {
                identity = (UserSessionIdentity)(Thread.CurrentPrincipal.Identity);

                if (identity.session != null)
                {
                    session = identity.session;
                }
            }

            // If we are not logged in, 404.
            if (session == null)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized);
            }

            // Use the user's user id, or the supplied ID if we are a teacher or admin.
            User u = (User)((UserSessionIdentity)(Thread.CurrentPrincipal.Identity)).user;
            string userType = u.user_type;
            // Use the user's user id, or the supplied ID if we are a teacher or admin.
            if (!(userType.Equals("Teacher") || userType.Equals("Admin")))
            {
                studentId = session.user_id;
            }
            else if (studentId == null)
            {
                studentId = session.user_id;
            }

            // Return the carss.
            DBConnection conn = new DBConnection("GameOfLife");
            result = this.jobsRepository.Get(conn, studentId.Value);

            conn.Close();

            if (result != null)
            {

                return request.CreateResponse(HttpStatusCode.OK, result);
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, new JobsController[0]);
            }
        }
    }
}
