﻿//  Author:
//       Joe Merkel <merkeljoe9@gmail.com>
//
//  Copyright (c) 2018 Joe Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Http;
using System.Web.SessionState;
using System.Net;
using System.Net.Http;

using GameOfLife.Models;
using GameOfLife.Entities;
using GameOfLife.Filters;
using System.Threading;
using GameOfLife.App_Start;
using T4R.Data;

namespace GameOfLife.Controllers
{
    public class HomesController : ApiController
    {
        HomesRepository homesRepository = null;
        public HomesController()
        {
            this.homesRepository = new HomesRepository();
        }

        [TokenAuthFilter(true, true)]
        public HttpResponseMessage Get(HttpRequestMessage request, int? id = null)
        {
            Dictionary<string, dynamic> parms = request.GetParams();
            List<Home> result = null;

            int? studentId = id;

            // Get our session.
            UserSessionIdentity identity = null;
            Session session = null;

            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity != null && Thread.CurrentPrincipal.Identity is UserSessionIdentity)
            {
                identity = (UserSessionIdentity)(Thread.CurrentPrincipal.Identity);

                if (identity.session != null)
                {
                    session = identity.session;
                }
            }

            // If we are not logged in, 404.
            if (session == null)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized);
            }

            // Use the user's user id, or the supplied ID if we are a teacher or admin.
            User u = (User)((UserSessionIdentity)(Thread.CurrentPrincipal.Identity)).user;
            string userType = u.user_type;
            if (!(userType.Equals("Teacher") || userType.Equals("Admin")))
            {
                studentId = session.user_id;
            }
            else if (studentId == null)
            {
                studentId = session.user_id;
            }

            // Return the homess.
            DBConnection conn = new DBConnection("GameOfLife");
            result = this.homesRepository.Get(conn, studentId.Value);

            BankRepository bank = new BankRepository();
            int cashOnHand = (bank.Get(conn, studentId.Value) ?? new Bank(studentId.Value, 0, 0, 0)).cash_on_hand;

            conn.Close();

            if (result != null)
            {

                return request.CreateResponse(HttpStatusCode.OK, new Homes(cashOnHand, result.ToArray()));
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, new Homes(cashOnHand, new Home[0]));
            }
        }

        [TokenAuthFilter(true, true)]
        [System.Web.Http.Route("homes/buy")]
        [System.Web.Http.HttpPost()]
        public HttpResponseMessage Buy(HttpRequestMessage request, int? id = null)
        {

            // handle any other kind of processing here?
            Dictionary<string, dynamic> parms = request.GetParams();

            int? studentId = id;

            int? user_id = null;
            string address = null;
            int? price = null;

            if (parms == null)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, "No parameters provided!");
            }

            Dictionary<string, dynamic>.Enumerator enumerator = parms.GetEnumerator();

            while (enumerator.MoveNext())
            {
                KeyValuePair<string, dynamic> kv = enumerator.Current;

                switch (kv.Key)
                {
                    case "user_id":
                        try
                        {
                            user_id = Convert.ToInt32(kv.Value);
                        }
                        catch (Exception)
                        {
                        }

                        break;
                    case "address":
                        try
                        {
                            address = (string)kv.Value;
                        }
                        catch (Exception)
                        {
                        }
                        break;
                    case "price":
                        try
                        {
                            price = Convert.ToInt32(kv.Value);
                        }
                        catch (Exception)
                        {
                        }
                        break;
                }
            }

            if (price == null || address == null)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, "No all parameters provided!");
            }

            // Get our session.
            UserSessionIdentity identity = null;
            Session session = null;

            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity != null && Thread.CurrentPrincipal.Identity is UserSessionIdentity)
            {
                identity = (UserSessionIdentity)(Thread.CurrentPrincipal.Identity);

                if (identity.session != null)
                {
                    session = identity.session;
                }
            }

            // If we are not logged in, 404.
            if (session == null)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized);
            }

            // Use the user's user id, or the supplied ID if we are a teacher or admin.
            User u = (User)((UserSessionIdentity)(Thread.CurrentPrincipal.Identity)).user;
            string userType = u.user_type;
            if (!(userType.Equals("Teacher") || userType.Equals("Admin")))
            {
                studentId = session.user_id;
            }
            else if (studentId != null)
            {

            }
            else if (user_id != null)
            {
                studentId = user_id;
            }
            else
            {
                studentId = session.user_id;
            }

            DBConnection conn = new DBConnection("GameOfLife");

            bool success = this.homesRepository.Buy(conn, studentId.Value, address, price.Value);

            if (!success)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, new ApiResult(false, "Insert failed."));
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, new ApiResult(true, ""));
            }
        }

        [TokenAuthFilter(true, true)]
        [System.Web.Http.Route("homes/loan")]
        [System.Web.Http.HttpPost()]
        public HttpResponseMessage Loan(HttpRequestMessage request, int? id = null)
        {

            // handle any other kind of processing here?
            Dictionary<string, dynamic> parms = request.GetParams();

            int? studentId = id;

            int? user_id = null;
            string address = null;
            int? price = null;
            int? loan_downpayment = null;
            int? loan_amount = null;
            int? loan_duration = null;

            if (parms == null)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, "No parameters provided!");
            }

            Dictionary<string, dynamic>.Enumerator enumerator = parms.GetEnumerator();

            while (enumerator.MoveNext())
            {
                KeyValuePair<string, dynamic> kv = enumerator.Current;

                switch (kv.Key)
                {
                    case "user_id":
                        try
                        {
                            user_id = Convert.ToInt32(kv.Value);
                        }
                        catch (Exception)
                        {
                        }

                        break;
                    case "address":
                        try
                        {
                            address = (string)kv.Value;
                        }
                        catch (Exception)
                        {
                        }
                        break;
                    case "price":
                        try
                        {
                            price = Convert.ToInt32(kv.Value);
                        }
                        catch (Exception)
                        {
                        }
                        break;
                    case "loan_downpayment":
                        try
                        {
                            loan_downpayment = Convert.ToInt32(kv.Value);
                        }
                        catch (Exception)
                        {
                        }
                        break;
                    case "loan_amount":
                        try
                        {
                            loan_amount = Convert.ToInt32(kv.Value);
                        }
                        catch (Exception)
                        {
                        }
                        break;
                    case "loan_duration":
                        try
                        {
                            loan_duration = Convert.ToInt32(kv.Value);
                        }
                        catch (Exception)
                        {
                        }
                        break;
                }
            }

            if (price == null || address == null || loan_downpayment == null || loan_amount == null || loan_duration == null)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, "No all parameters provided!");
            }

            // Get our session.
            UserSessionIdentity identity = null;
            Session session = null;

            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity != null && Thread.CurrentPrincipal.Identity is UserSessionIdentity)
            {
                identity = (UserSessionIdentity)(Thread.CurrentPrincipal.Identity);

                if (identity.session != null)
                {
                    session = identity.session;
                }
            }

            // If we are not logged in, 404.
            if (session == null)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized);
            }

            // Use the user's user id, or the supplied ID if we are a teacher or admin.
            User u = (User)((UserSessionIdentity)(Thread.CurrentPrincipal.Identity)).user;
            string userType = u.user_type;
            if (!(userType.Equals("Teacher") || userType.Equals("Admin")))
            {
                studentId = session.user_id;
            }
            else if (studentId != null)
            {

            }
            else if (user_id != null)
            {
                studentId = user_id;
            }
            else
            {
                studentId = session.user_id;
            }

            DBConnection conn = new DBConnection("GameOfLife");

            bool success = this.homesRepository.Loan(conn, studentId.Value, address, price.Value, loan_downpayment.Value, loan_amount.Value, loan_duration.Value);

            if (!success)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, new ApiResult(false, "Insert failed."));
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, new ApiResult(true, ""));
            }
        }

        [TokenAuthFilter(true, true)]
        [System.Web.Http.Route("homes/rent")]
        [System.Web.Http.HttpPost()]
        public HttpResponseMessage Rent(HttpRequestMessage request, int? id = null)
        {

            // handle any other kind of processing here?
            Dictionary<string, dynamic> parms = request.GetParams();

            int? studentId = id;

            int? user_id = null;
            string address = null;
            int? price = null;

            if (parms == null)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, "No parameters provided!");
            }

            Dictionary<string, dynamic>.Enumerator enumerator = parms.GetEnumerator();

            while (enumerator.MoveNext())
            {
                KeyValuePair<string, dynamic> kv = enumerator.Current;

                switch (kv.Key)
                {
                    case "user_id":
                        try
                        {
                            user_id = Convert.ToInt32(kv.Value);
                        }
                        catch (Exception)
                        {
                        }

                        break;
                    case "address":
                        try
                        {
                            address = (string)kv.Value;
                        }
                        catch (Exception)
                        {
                        }
                        break;
                    case "price":
                        try
                        {
                            price = Convert.ToInt32(kv.Value);
                        }
                        catch (Exception)
                        {
                        }
                        break;
                }
            }

            if (price == null || address == null)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, "No all parameters provided!");
            }

            // Get our session.
            UserSessionIdentity identity = null;
            Session session = null;

            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity != null && Thread.CurrentPrincipal.Identity is UserSessionIdentity)
            {
                identity = (UserSessionIdentity)(Thread.CurrentPrincipal.Identity);

                if (identity.session != null)
                {
                    session = identity.session;
                }
            }

            // If we are not logged in, 404.
            if (session == null)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized);
            }

            // Use the user's user id, or the supplied ID if we are a teacher or admin.
            User u = (User)((UserSessionIdentity)(Thread.CurrentPrincipal.Identity)).user;
            string userType = u.user_type;
            if (!(userType.Equals("Teacher") || userType.Equals("Admin")))
            {
                studentId = session.user_id;
            }
            else if (studentId != null)
            {

            }
            else if (user_id != null)
            {
                studentId = user_id;
            }
            else
            {
                studentId = session.user_id;
            }

            DBConnection conn = new DBConnection("GameOfLife");

            bool success = this.homesRepository.Rent(conn, studentId.Value, address, price.Value);

            if (!success)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, new ApiResult(false, "Insert failed."));
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, new ApiResult(true, ""));
            }
        }
    }
}
