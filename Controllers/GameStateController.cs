﻿//
//  GameStateController.cs
//
//  Author:
//       Roy Merkel <merkel-roy@comcast.net>
//
//  Copyright (c) 2018 Roy Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Http;
using System.Web.SessionState;
using System.Net;
using System.Net.Http;
using T4R.Data;

using GameOfLife.Models;
using GameOfLife.Entities;
using GameOfLife.Filters;
using System.Threading;
using GameOfLife.App_Start;

namespace GameOfLife.Controllers
{
	public class GameStateController : ApiController
    {
		GameStateRepository gameStateRepository = null;
		public GameStateController()
        {
			this.gameStateRepository = new GameStateRepository();
        }

		[TokenAuthFilter(true, true)]
		[System.Web.Http.Route("start_session")]
		public HttpResponseMessage Start(HttpRequestMessage request)
		{
			bool results = false;
			DBConnection conn = new DBConnection("GameOfLife");

            results = gameStateRepository.StartSession(conn);

			conn.Close();

			if (results)
			{
				return request.CreateResponse(HttpStatusCode.OK, results);
			}
			else
			{
				return request.CreateResponse(HttpStatusCode.InternalServerError, "Update failed!");
			}
        }

		[TokenAuthFilter(true, true)]
		[System.Web.Http.Route("stop_session")]
        public HttpResponseMessage Stop(HttpRequestMessage request)
        {
            bool results = false;
            DBConnection conn = new DBConnection("GameOfLife");

            results = gameStateRepository.StopSession(conn);

            conn.Close();

            if (results)
            {
                return request.CreateResponse(HttpStatusCode.OK, results);
            }
            else
            {
				return request.CreateResponse(HttpStatusCode.InternalServerError, "Update failed!");
            }
        }
    }
}
