﻿//
//  FamilyController.cs
//
//  Author:
//       Roy Merkel <merkel-roy@comcast.net>
//
//  Copyright (c) 2018 Roy Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.SessionState;
using GameOfLife.App_Start;
using GameOfLife.Entities;
using GameOfLife.Filters;
using GameOfLife.Models;
using T4R.Data;
using Newtonsoft.Json;

namespace GameOfLife.Controllers
{ 
	public class PostFamiliesResponse
    {
		private int _actualNumberChildren;
		private int _childSupportCosts;

		public PostFamiliesResponse(int actualNumberChildren, int childSupportCosts)
		{
			_actualNumberChildren = actualNumberChildren;
			_childSupportCosts = childSupportCosts;
		}
        
		public int actual_number_children { get { return _actualNumberChildren; } }
		public int child_support_costs { get { return _childSupportCosts; } }
    }
    
	public class FamilyController : ApiController
    {
		FamilyRepository familyRepository = null;

		public FamilyController()
        {
			this.familyRepository = new FamilyRepository();
        }

		[TokenAuthFilter(true, true)]
		public HttpResponseMessage Post(HttpRequestMessage request, int id = -1)
        {
            try
            {
                // handle any other kind of processing here?
                Dictionary<string, dynamic> parms = request.GetParams();
                UserSessionIdentity identity = (UserSessionIdentity)(Thread.CurrentPrincipal.Identity);

				int user_id = -1;

				if (id > 0)
				{
					user_id = id;
				}
				else
				{
					user_id = identity.user.user_id;
				}

				string want_family = null;
				string want_kids = null;
				int? children_applied_for = null;
                
                if (parms == null)
                {
                    return request.CreateResponse(HttpStatusCode.BadRequest, "No parameters provided!");
                }

                Dictionary<string, dynamic>.Enumerator enumerator = parms.GetEnumerator();
                
                while (enumerator.MoveNext())
                {
                    KeyValuePair<string, dynamic> kv = enumerator.Current;

                    switch (kv.Key)
                    {
						case "want_family":
                            try
                            {
								want_family = (string)kv.Value;
                            }
                            catch (Exception)
							{
                            }

                            break;
						case "want_kids":
							try
                            {
								want_kids = (string)kv.Value;
                            }
                            catch (Exception)
                            {
                            }
                            
                            break;
						case "children_applied_for":
							try
                            {
								children_applied_for = Convert.ToInt32(kv.Value);
                            }
                            catch (Exception)
                            {
                            }

                            break;
                    }
                }

				if (want_family == null || want_kids == null || children_applied_for == null)
                {
                    return request.CreateResponse(HttpStatusCode.BadRequest, "No all parameters provided!");
                }

                DBConnection conn = new DBConnection("GameOfLife");
                HttpSessionState session = HttpContext.Current.Session;
                string ip = request.GetClientIpAddress();
                string userAgent = request.GetUserAgent();
                string referrer = request.GetReferrer();
                
				int actualNumberChildren;
				int childSupportCosts;
                
				bool success = this.familyRepository.Post(conn, user_id, want_family, want_kids, children_applied_for.Value, out actualNumberChildren, out childSupportCosts);
				PostFamiliesResponse response = null;            

                if (!success)
                {
					return request.CreateResponse(HttpStatusCode.Unauthorized, response);
                }
                else
                {
					response = new PostFamiliesResponse(actualNumberChildren, childSupportCosts);
					return request.CreateResponse(HttpStatusCode.OK, response);
                }
            }
            catch(Exception e)
            {
                return request.CreateResponse(HttpStatusCode.InternalServerError, e + "\n" + e.StackTrace);
            }
        }

        [TokenAuthFilter(true, true)]
        public HttpResponseMessage Get(HttpRequestMessage request, int id = -1)
        {
            // handle any other kind of processing here?
            Dictionary<string, dynamic> parms = request.GetParams();
            UserSessionIdentity identity = (UserSessionIdentity)(Thread.CurrentPrincipal.Identity);

			int user_id = -1;
            
            if (id > 0)
            {
                user_id = id;
            }
            else
            {
                user_id = identity.user.user_id;
            }
         
            DBConnection conn = new DBConnection("GameOfLife");

            Family result = familyRepository.Get(conn, user_id);

            conn.Close();

            if (result == null)
            {
				return request.CreateResponse(HttpStatusCode.NoContent, result);
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, result);
            }
        }
    }
}
