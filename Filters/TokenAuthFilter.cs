﻿//
//  TokenAuthFilter.cs
//
//  Author:
//       Roy Merkel <merkel-roy@comcast.net>
//
//  Copyright (c) 2018 Roy Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Web.Http.Results;
using GameOfLife.Entities;
using GameOfLife.Models;
using T4R.Data;

namespace GameOfLife.Filters
{
	public class TokenAuthFilter : Attribute, IAuthenticationFilter
    {
        bool isActive;
        bool isMandatory;
        SessionRepository sessionRepository;
        UserRepository userRepository;

        public TokenAuthFilter() : base()
        {
            isActive = true;
            isMandatory = true;
            sessionRepository = new SessionRepository();
            userRepository = new UserRepository();
        }

        public TokenAuthFilter(bool isActive) : base()
        {
            this.isActive = isActive;
            isMandatory = true;
            sessionRepository = new SessionRepository();
            userRepository = new UserRepository();
        }

        public TokenAuthFilter(bool isActive, bool isMandatory) : base()
        {
            this.isActive = isActive;
            this.isMandatory = isMandatory;
            sessionRepository = new SessionRepository();
            userRepository = new UserRepository();
        }

		public bool AllowMultiple => false;

		public Task AuthenticateAsync(HttpAuthenticationContext context, CancellationToken cancellationToken)
		{         
			if (!isActive) return Task.FromResult(0);

            string token = GetToken(context);

            if (token == null)
            {
                if (isMandatory)
                {
					context.ErrorResult = new UnauthorizedResult(new AuthenticationHeaderValue[0], context.Request);
                }
				return Task.FromResult(0);
            }

            // check for the session.
            DBConnection conn = new DBConnection("GameOfLife");

            Session s = sessionRepository.GetSession(conn, token);

            if (s == null)
            {
                if (isMandatory)
                {
					context.ErrorResult = new UnauthorizedResult(new AuthenticationHeaderValue[0], context.Request);
                }
                conn.Close();
				return Task.FromResult(0);
            }

            // check for session expiration.
            if (s.start_time == null || s.end_time != null || DateTime.Compare(s.start_time.Value, DateTime.Now.Subtract(TimeSpan.FromMinutes(120))) < 0)
            {
                if (isMandatory)
                {
					context.ErrorResult = new UnauthorizedResult(new AuthenticationHeaderValue[0], context.Request);
                }
                conn.Close();
				return Task.FromResult(0);
            }

            //
            GenericIdentity identity = null;
            if (s.user_id > 0)
            {
                identity = new UserSessionIdentity(userRepository.GetUser(conn, s.user_id), s);
				context.Principal = new GenericPrincipal(identity, null);
            }
            else
            {
                if (isMandatory)
                {
					context.ErrorResult = new UnauthorizedResult(new AuthenticationHeaderValue[0], context.Request);
                }
                conn.Close();
				return Task.FromResult(0);
            }

            conn.Close();
			return Task.FromResult(0);
		}

		public Task ChallengeAsync(HttpAuthenticationChallengeContext context, CancellationToken cancellationToken)
		{
			return Task.FromResult(0);
		}

		public string GetToken(HttpActionContext actionContext)
        {
            // get the token from the header
            string token = null;

            IEnumerable<string> values = null;

            if (actionContext.Request.Headers.TryGetValues("Token", out values))
            {
                IEnumerator<string> enumerator = values.GetEnumerator();

                while (enumerator.MoveNext())
                {
                    token = enumerator.Current;
                }
            }
            else if (actionContext.Request.Headers.TryGetValues("token", out values))
            {
                IEnumerator<string> enumerator = values.GetEnumerator();

                while (enumerator.MoveNext())
                {
                    token = enumerator.Current;
                }
            }
            // if that failes try the get parameters.
            else
            {
                IEnumerable<KeyValuePair<string, string>> enumerable = actionContext.Request.GetQueryNameValuePairs();
                IEnumerator<KeyValuePair<string, string>> enumerator = enumerable.GetEnumerator();

                while (enumerator.MoveNext())
                {
                    KeyValuePair<string, string> kv = enumerator.Current;

                    if (kv.Key.Equals("Token") || kv.Key.Equals("token"))
                    {
                        token = kv.Value;
                    }
                }
            }

            return token;
        }
        
		public string GetToken(HttpAuthenticationContext authenticationContext)
        {
            // get the token from the header
            string token = null;

            IEnumerable<string> values = null;

			if (authenticationContext.Request.Headers.TryGetValues("Token", out values))
            {
                IEnumerator<string> enumerator = values.GetEnumerator();

                while (enumerator.MoveNext())
                {
                    token = enumerator.Current;
                }
            }
			else if (authenticationContext.Request.Headers.TryGetValues("token", out values))
            {
                IEnumerator<string> enumerator = values.GetEnumerator();

                while (enumerator.MoveNext())
                {
                    token = enumerator.Current;
                }
            }
            // if that failes try the get parameters.
            else
            {
				IEnumerable<KeyValuePair<string, string>> enumerable = authenticationContext.Request.GetQueryNameValuePairs();
                IEnumerator<KeyValuePair<string, string>> enumerator = enumerable.GetEnumerator();

                while (enumerator.MoveNext())
                {
                    KeyValuePair<string, string> kv = enumerator.Current;

                    if (kv.Key.Equals("Token") || kv.Key.Equals("token"))
                    {
                        token = kv.Value;
                    }
                }
            }

            return token;
        }
	}
}
