﻿//
//  IsAdminFilter.cs
//
//  Author:
//       Roy Merkel <merkel-roy@comcast.net>
//
//  Copyright (c) 2018 Roy Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Web.Http.Routing;
using GameOfLife.Entities;

namespace GameOfLife.Filters
{
	public class IsAdminFilter : AuthorizationFilterAttribute
	{
		bool isActive;
		bool isMandatory;
		string mandatoryIfSetRouteParamName;

		public IsAdminFilter() : base()
		{
			isActive = true;
			isMandatory = true;
		}

		public IsAdminFilter(bool isActive) : base()
		{
			this.isActive = isActive;
			this.isMandatory = true;
			this.mandatoryIfSetRouteParamName = null;
		}

		public IsAdminFilter(bool isActive, bool isMandatory) : base()
		{
			this.isActive = isActive;
			this.isMandatory = isMandatory;
			this.mandatoryIfSetRouteParamName = null;
		}

		public IsAdminFilter(bool isActive, string mandatoryIfSetRouteParamName) : base()
        {
            this.isActive = isActive;
            this.isMandatory = false;
			this.mandatoryIfSetRouteParamName = mandatoryIfSetRouteParamName;
        }

		public override Task OnAuthorizationAsync(HttpActionContext actionContext, CancellationToken cancellationToken)
		{
			IHttpRouteData route = actionContext.Request.GetRouteData();
			object pathParamVal = null;
			bool isMandatory = this.isMandatory;

			if (mandatoryIfSetRouteParamName != null && !mandatoryIfSetRouteParamName.Equals(""))
			{
				if (route.Values.ContainsKey(mandatoryIfSetRouteParamName) && route.Values.TryGetValue(mandatoryIfSetRouteParamName, out pathParamVal))
				{
					if (pathParamVal != null)
					{
						isMandatory = true;
					}
				}
			}

			if (!this.isActive)
			{
				return base.OnAuthorizationAsync(actionContext, cancellationToken);
			}

			if (actionContext.RequestContext.Principal == null)
			{
				if (isMandatory)
				{
					actionContext.Response = new HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);
				}

				return base.OnAuthorizationAsync(actionContext, cancellationToken);
			}
			else if (actionContext.RequestContext.Principal.Identity == null)
			{
				if (isMandatory)
				{
					actionContext.Response = new HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);
				}

				return base.OnAuthorizationAsync(actionContext, cancellationToken);
			}
			else if (!(actionContext.RequestContext.Principal.Identity is UserSessionIdentity))
			{
				if (isMandatory)
				{
					actionContext.Response = new HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);
				}

				return base.OnAuthorizationAsync(actionContext, cancellationToken);
			}
			else if (((UserSessionIdentity)(Thread.CurrentPrincipal.Identity)).user == null)
			{
				if (isMandatory)
				{
					actionContext.Response = new HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);
				}

				return base.OnAuthorizationAsync(actionContext, cancellationToken);
			}
			else
			{
				User u = (User)((UserSessionIdentity)(Thread.CurrentPrincipal.Identity)).user;
				string userType = u.user_type;

				if (!(userType.Equals("Teacher") || userType.Equals("Admin")))
				{
					if (isMandatory)
                    {
                        actionContext.Response = new HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);
                    }
				}

				return base.OnAuthorizationAsync(actionContext, cancellationToken);
			}
		}
	}
}
