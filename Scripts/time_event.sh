#!/bin/bash
export path=$(/usr/bin/dirname `/usr/bin/realpath $0`)

pushd ${path}

export NODE_PATH=/usr/lib/node_modules
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin
/usr/bin/nodejs time_event.js

popd
