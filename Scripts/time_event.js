var fs = require('fs');
var os = require('os');
var path = require('path');
var process = require('process');
var isrunning = require('is-running');
var deasync = require('deasync');
var ps = require('ps-node');
var DBConnection = require('t4r-dbconnection');

var name = __filename;
var pid = process.pid;

name = path.basename(name);

var pidfilename = os.tmpdir() + path.sep + name + ".pid";

function canRun(cb)
{
	var name = __filename;
	var pid = process.pid;

	name = path.basename(name);

	var pidfilename = os.tmpdir() + path.sep + name + ".pid";

	function pidfileexists(callback)
	{
		fs.access(pidfilename, fs.constants.F_OK | fs.constants.R_OK | fs.constants.W_OK, (err) => {
			if(err && err.code != 'ENOENT')
			{
				return callback(err);
			}
			else if(err)
			{
				return callback(null, false);
			}
			else
			{
				return callback(null, true);
			}
		});
	}

	function readpidfile(callback)
	{
		fs.readFile(pidfilename, 'utf8', (err, data) => {
			if(err)
			{
				return callback(err);
			}
			else
			{
				return callback(null, data);
			}
		});
	}

	function pidrunning(pid, callback)
	{
		var isRunning = isrunning(pid);

		return callback(null, isRunning);
	}

	function pidisthis(pid, callback)
	{
		ps.lookup({ 'pid': pid }, function(err, resultList) {
			if(err)
			{
				callback(err);
			}
			else
			{
				var res = false;
				for(var i = 0; i < resultList.length; i++)
				{
					var result = resultList[i];

					if(path.basename(result.command) == name)
					{
						res = true;
						break;
					}
					else if(result.arguments !== undefined && result.arguments !== null && result.arguments.length > 0 && result.arguments[0] == name)
					{
						res = true;
						break;
					}
				}

				return callback(null, res);
			}
		});
	}

	pidfileexists((err, exists) => {
		if(err)
		{
			console.log(err);
			cb(err, false);
		}
		else
		{
			readpidfile((err, data) => {
				if(err)
				{
					console.log(err);
					cb(err, false);
				}

				if(data != pid)
				{
					pidrunning(data, (err, isRunning) => {
						if(err)
						{
							console.log(err);
							cb(err, false);
						}

						if(!isRunning)
						{
							cb(null, true);
						}
						else
						{
							pidisthis(data, (err, isThis) => {
								if(err)
								{
									console.log(err);
									cb(err, false);
								}

								cb(null, !isThis);
							});
						}
					});
				}
				else
				{
					cb(null, true);
				}
			});
		}
	});
}

console.log("started!");

var canAccess = undefined;

canRun((err, canDo) => {
	canAccess = canDo;
});

deasync.loopWhile(function(){return canAccess === undefined;});

var wrotePid = undefined;
fs.writeFile(pidfilename, pid, function(err) {
	if(err) 
	{
		console.log(err);
		canAccess = false;
		wrotePid = false;
	}

	canAccess = true;
	wrotePid = true;
});

deasync.loopWhile(function(){return wrotePid === undefined;});

if(canAccess && wrotePid)
{
	console.log("running DB operation!");
	var doneConnecting = false;
	var doneDisconnecting = false;

	var conn = new DBConnection('GameOfLife', (err, obj) => {
		doneConnecting = true;
	});

	deasync.loopWhile(function(){return !doneConnecting;});

	var rs;
	conn.ExecuteUpdate("CALL time_event();", [], (err, resultSet) => {
		if(err)
		{
			console.log(err);
			rs = [];
		}

		rs = resultSet;
	});
	deasync.loopWhile(function(){return rs === undefined;});

	conn.close((err, obj) => {
		doneDisconnecting = true;
	});

	deasync.loopWhile(function(){return !doneDisconnecting;});

}

console.log("finished!");
