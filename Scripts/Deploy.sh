#!/bin/bash
pushd .. > /dev/null
export projfolder=$(basename `pwd`)
pushd .. > /dev/null

echo tar cvzf ${projfolder}.tar.gz ${projfolder}
tar cvzf ${projfolder}.tar.gz ${projfolder} > /dev/null

if test $# -eq "0"; then
	echo "Uploading!"
	echo scp -i ~/.ssh/shasta.pem ${projfolder}.tar.gz ubuntu@shastaaws.intervisionmedia.com:/home/ubuntu/${projfolder}.tar.gz
	scp -i ~/.ssh/shasta.pem ${projfolder}.tar.gz ubuntu@shastaaws.intervisionmedia.com:/home/ubuntu/${projfolder}.tar.gz

	if test $# -eq "0"; then
		echo "uploaded!"

		echo ssh -i ~/.ssh/shasta.pem ubuntu@shastaaws.intervisionmedia.com "\"bash -c 'cd /var/www/html; sudo tar xvzf /home/ubuntu/"${projfolder}".tar.gz > /dev/null; echo \$#'\""
		export untarRes=`ssh -i ~/.ssh/shasta.pem ubuntu@shastaaws.intervisionmedia.com "bash -c 'cd /var/www/html; sudo tar xvzf /home/ubuntu/${projfolder}.tar.gz > /dev/null; echo \$#'"`
		if test $# -eq "0" -a ${untarRes} -eq "0"; then
			echo "untarred!"
			echo "ssh -i ~/.ssh/shasta.pem ubuntu@shastaaws.intervisionmedia.com \"bash -c 'cd /var/www/html/"${projfolder}"; find . -type \"f\" -iname \"*.csproj\" -exec rm -rf \"{}\" \\;'\""
			ssh -i ~/.ssh/shasta.pem ubuntu@shastaaws.intervisionmedia.com "bash -c 'cd /var/www/html/${projfolder}; find . -type \"f\" -iname \"*.csproj\" -exec rm -rf \"{}\" \\;'"
			echo "ssh -i ~/.ssh/shasta.pem ubuntu@shastaaws.intervisionmedia.com \"bash -c 'cd /var/www/html/"${projfolder}"; find . -type \"f\" -iname \"*.sln\" -exec rm -rf \"{}\" \\;'\""
			ssh -i ~/.ssh/shasta.pem ubuntu@shastaaws.intervisionmedia.com "bash -c 'cd /var/www/html/${projfolder}; find . -type \"f\" -iname \"*.sln\" -exec rm -rf \"{}\" \\;'"
			echo "ssh -i ~/.ssh/shasta.pem ubuntu@shastaaws.intervisionmedia.com \"bash -c 'cd /var/www/html/"${projfolder}"; find . -type \"f\" -iname \"*.cs\" -exec rm -rf \"{}\" \\;'\""
			ssh -i ~/.ssh/shasta.pem ubuntu@shastaaws.intervisionmedia.com "bash -c 'cd /var/www/html/${projfolder}; find . -type \"f\" -iname \"*.cs\" -exec rm -rf \"{}\" \\;'"
			echo "ssh -i ~/.ssh/shasta.pem ubuntu@shastaaws.intervisionmedia.com \"bash -c 'cd /var/www/html/"${projfolder}"; rm -rf \"obj\";'\""
			ssh -i ~/.ssh/shasta.pem ubuntu@shastaaws.intervisionmedia.com "bash -c 'cd /var/www/html/${projfolder}; rm -rf \"obj\"'"
			echo "ssh -i ~/.ssh/shasta.pem ubuntu@shastaaws.intervisionmedia.com \"bash -c 'cd /var/www/html/"${projfolder}"; rm -rf \"Views\"'\""
			ssh -i ~/.ssh/shasta.pem ubuntu@shastaaws.intervisionmedia.com "bash -c 'cd /var/www/html/${projfolder}; rm -rf \"Views\"'"
			echo "ssh -i ~/.ssh/shasta.pem ubuntu@shastaaws.intervisionmedia.com \"bash -c 'cd /var/www/html/"${projfolder}"; rmdir \"Models\"'\""
			ssh -i ~/.ssh/shasta.pem ubuntu@shastaaws.intervisionmedia.com "bash -c 'cd /var/www/html/${projfolder}; rmdir \"Models\"'"
			echo "ssh -i ~/.ssh/shasta.pem ubuntu@shastaaws.intervisionmedia.com \"bash -c 'cd /var/www/html/"${projfolder}"; rmdir \"Handlers\"'\""
			ssh -i ~/.ssh/shasta.pem ubuntu@shastaaws.intervisionmedia.com "bash -c 'cd /var/www/html/${projfolder}; rmdir \"Handlers\"'"
			echo "ssh -i ~/.ssh/shasta.pem ubuntu@shastaaws.intervisionmedia.com \"bash -c 'cd /var/www/html/"${projfolder}"; rmdir \"Filters\"'\""
			ssh -i ~/.ssh/shasta.pem ubuntu@shastaaws.intervisionmedia.com "bash -c 'cd /var/www/html/${projfolder}; rmdir \"Filters\"'"
			echo "ssh -i ~/.ssh/shasta.pem ubuntu@shastaaws.intervisionmedia.com \"bash -c 'cd /var/www/html/"${projfolder}"; rmdir \"Entities\"'\""
			ssh -i ~/.ssh/shasta.pem ubuntu@shastaaws.intervisionmedia.com "bash -c 'cd /var/www/html/${projfolder}; rmdir \"Entities\"'"
			echo "ssh -i ~/.ssh/shasta.pem ubuntu@shastaaws.intervisionmedia.com \"bash -c 'cd /var/www/html/"${projfolder}"; rmdir \"Controllers\"'\""
			ssh -i ~/.ssh/shasta.pem ubuntu@shastaaws.intervisionmedia.com "bash -c 'cd /var/www/html/${projfolder}; rmdir \"Controllers\"'"
			echo "ssh -i ~/.ssh/shasta.pem ubuntu@shastaaws.intervisionmedia.com \"bash -c 'cd /var/www/html/"${projfolder}"; rmdir \"App_Start\"'\""
			ssh -i ~/.ssh/shasta.pem ubuntu@shastaaws.intervisionmedia.com "bash -c 'cd /var/www/html/${projfolder}; rmdir \"App_Start\"'"
			echo "ssh -i ~/.ssh/shasta.pem ubuntu@shastaaws.intervisionmedia.com \"bash -c 'cd /var/www/html/"${projfolder}"; rm -rf \".git\"'\""
			ssh -i ~/.ssh/shasta.pem ubuntu@shastaaws.intervisionmedia.com "bash -c 'cd /var/www/html/${projfolder}; rm -rf \".git\"'"
			echo "ssh -i ~/.ssh/shasta.pem ubuntu@shastaaws.intervisionmedia.com \"bash -c 'cd /var/www/html/"${projfolder}"; rm \".gitignore\"'\""
			ssh -i ~/.ssh/shasta.pem ubuntu@shastaaws.intervisionmedia.com "bash -c 'cd /var/www/html/${projfolder}; rm \".gitignore\"'"
			echo "ssh -i ~/.ssh/shasta.pem ubuntu@shastaaws.intervisionmedia.com \"bash -c 'cd /var/www/html/"${projfolder}"; rm \".gitmodules\"'\""
			ssh -i ~/.ssh/shasta.pem ubuntu@shastaaws.intervisionmedia.com "bash -c 'cd /var/www/html/${projfolder}; rm \".gitmodules\"'"
			echo "ssh -i ~/.ssh/shasta.pem ubuntu@shastaaws.intervisionmedia.com \"bash -c 'cd /var/www/html/"${projfolder}"; rm -rf \".vs\"'\""
			ssh -i ~/.ssh/shasta.pem ubuntu@shastaaws.intervisionmedia.com "bash -c 'cd /var/www/html/${projfolder}; rm -rf \".vs\"'"

			echo "ssh -i ~/.ssh/shasta.pem ubuntu@shastaaws.intervisionmedia.com \"bash -c 'cd /var/www/html; sudo rm -rf gameoflife.api'\""
			ssh -i ~/.ssh/shasta.pem ubuntu@shastaaws.intervisionmedia.com "bash -c 'cd /var/www/html; sudo rm -rf gameoflife.api'"
			echo "ssh -i ~/.ssh/shasta.pem ubuntu@shastaaws.intervisionmedia.com \"bash -c 'cd /var/www/html; sudo mv ${projfolder} gameoflife.api'\""
			ssh -i ~/.ssh/shasta.pem ubuntu@shastaaws.intervisionmedia.com "bash -c 'cd /var/www/html; sudo mv ${projfolder} gameoflife.api'"
			echo "ssh -i ~/.ssh/shasta.pem ubuntu@shastaaws.intervisionmedia.com \"bash -c 'cd /var/www/html; sudo chown -R ubuntu:www-data gameoflife.api'\""
			ssh -i ~/.ssh/shasta.pem ubuntu@shastaaws.intervisionmedia.com "bash -c 'cd /var/www/html; sudo chown -R ubuntu:www-data gameoflife.api'"
			echo "ssh -i ~/.ssh/shasta.pem ubuntu@shastaaws.intervisionmedia.com \"bash -c 'cd /var/www/html; sudo chmod -R g+w gameoflife.api'\""
			ssh -i ~/.ssh/shasta.pem ubuntu@shastaaws.intervisionmedia.com "bash -c 'cd /var/www/html; sudo chmod -R g+w gameoflife.api'"

			echo "ssh -i ~/.ssh/shasta.pem ubuntu@shastaaws.intervisionmedia.com \"bash -c 'sudo service apache2 reload'\""
			ssh -i ~/.ssh/shasta.pem ubuntu@shastaaws.intervisionmedia.com "bash -c 'sudo service apache2 reload'"
		fi
	fi
fi

popd > /dev/null
popd > /dev/null
